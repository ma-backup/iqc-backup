﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace IQCAdmin
{
    class DBHelper
    {
       
        public static SqlConnection connection = new SqlConnection("Data Source=192.168.2.14;Initial Catalog=IQCDatabase;Persist Security Info=True;User ID=software;Password=specialist");
        public static List<string> ComboBoxItems(string tablename, string columnnames, int columnindex)
        {
            List<string> list = new List<string>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT DISTINCT " + columnnames + " FROM " + tablename + "", connection);
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    string hldr = dataReader.GetValue(columnindex).ToString();
                    list.Add(hldr);
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
            return list;
        }
        public static void updateTable (string tblName, string columns, string id)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE "+tblName+" SET "+columns+" WHERE id = '"+id+"'",connection);
                command.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }
        public static DataTable exportData(string tblname)
        {
            try
            {
                DataTable output = new DataTable();

                SqlDataAdapter dataAdapter = new SqlDataAdapter("SELECT * FROM "+tblname+"", connection);
                dataAdapter.Fill(output);

                return output;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }
        public static void deleteEntireData(string tablename, string invoice, string partno)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM "+tablename+" WHERE invoice_no = '"+invoice+ "' AND MaterialCodeBoxSeqID = '" + partno+"'", connection);
                command.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }
        public static void overallJudgement(string invoice, string materialseq, string judgement)
        {
            try
            {
                SqlCommand command1 = new SqlCommand("SELECT TOP 1 id FROM tblOverall_Judgement WHERE invoice_no = '" + invoice + "' AND MaterialCodeBoxSeqID = '" + materialseq + "' ORDER BY id DESC", connection);
                if (Convert.ToString(command1.ExecuteScalar()) == "")
                {
                    SqlCommand command2 = new SqlCommand("INSERT INTO tblOverall_Judgement (invoice_no, MaterialCodeBoxSeqID, overall_judgement) VALUES ('" + invoice + "', '" + materialseq + "', '" + judgement + "')", connection);
                    command2.ExecuteNonQuery();
                }
                else
                {
                    SqlCommand command3 = new SqlCommand("UPDATE tblOverall_Judgement SET overall_judgement = '"+judgement+"' WHERE id = '"+ Convert.ToString(command1.ExecuteScalar()) + "'", connection);
                    command3.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }

        //This Code is for Saving Activity Log
        public static void saveActivityToDatabase(string action, string defaultAction)
        {
            try
            {
                //if the action string is not equal to the defaultAction variable, meaning there are unequal values, also means that the user updates something
                if (action != defaultAction)
                   {
                    frmEdit_Data newEditForm = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                    SqlCommand insertCommand = new SqlCommand("SP_Insert_Activity", DBHelper.connection);
                    insertCommand.CommandType = CommandType.StoredProcedure;
                    insertCommand.Parameters.AddWithValue("@EmployeeNumber", newEditForm.employeeNumberTextBox.Text);
                    insertCommand.Parameters.AddWithValue("@EmployeeName", newEditForm.employeeNameTextBox.Text);
                    insertCommand.Parameters.AddWithValue("@Action", action);
                    insertCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }
    }
}
