﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class ActivityLogForm : Form
    {
        public ActivityLogForm()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ActivityLogForm_Load(object sender, EventArgs e)
        {
            activityLogTimeFrameComboBox.SelectedIndex = 0;
            try
            {
                activityLogDataGridView.Rows.Clear();
                SqlCommand command = new SqlCommand("SP_Get_Activity_Today", DBHelper.connection);
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    activityLogDataGridView.Rows.Add(reader["Employee_Number"], reader["Employee_Name"], reader["Action"], reader["DateTime"]);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void activityLogTimeFrameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            activityLogDataGridView.Rows.Clear();
            SqlCommand command = new SqlCommand("SP_Get_Activity_Today", DBHelper.connection);
            switch (activityLogTimeFrameComboBox.SelectedIndex){
                case 0:
                    command = new SqlCommand("SP_Get_Activity_Today", DBHelper.connection);
                    break;
                case 1:
                    command = new SqlCommand("SP_Get_Activity_Last7Days", DBHelper.connection);
                    break;
                case 2:
                    command = new SqlCommand("SP_Get_Activity_Last30Days", DBHelper.connection);
                    break;
                case 3:
                    command = new SqlCommand("SP_Get_Activity_All", DBHelper.connection);
                    break;
            }
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                activityLogDataGridView.Rows.Add(reader["Employee_Number"], reader["Employee_Name"], reader["Action"], reader["DateTime"]);
            }
            reader.Close();
        }
    }
}
