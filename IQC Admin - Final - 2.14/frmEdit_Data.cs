﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class frmEdit_Data : Form
    {
        public string employeeNumber, employeeName;
        public frmEdit_Data()
        {
            InitializeComponent();
        }       
        string query;        
        private void frmEdit_Data_Activated(object sender, EventArgs e)
        {            
            comboBox2_SelectedIndexChanged(sender, e);
        }
        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            comboBox1_SelectedIndexChanged(sender, e);
        }
        private void comboBox2_TextChanged(object sender, EventArgs e)
        {
            comboBox2_SelectedIndexChanged(sender, e);
        }

        private void inspectionReportDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(inspectionReportDataGridView.RowCount > 0)
            {
                UpdateIncomingInspectionForm modalUIIF = new UpdateIncomingInspectionForm();

                modalUIIF.id = inspectionReportDataGridView.CurrentRow.Cells[0].Value.ToString();
                modalUIIF.invoiceNo = inspectionReportDataGridView.CurrentRow.Cells[1].Value.ToString();
                modalUIIF.approved = inspectionReportDataGridView.CurrentRow.Cells[2].Value.ToString();
                modalUIIF.checked1 = inspectionReportDataGridView.CurrentRow.Cells[3].Value.ToString();
                modalUIIF.prepared = inspectionReportDataGridView.CurrentRow.Cells[4].Value.ToString();
                modalUIIF.approvedDate = inspectionReportDataGridView.CurrentRow.Cells[5].Value.ToString();
                modalUIIF.checkedDate = inspectionReportDataGridView.CurrentRow.Cells[6].Value.ToString();
                modalUIIF.preparedDate = inspectionReportDataGridView.CurrentRow.Cells[7].Value.ToString();
                modalUIIF.temperature = inspectionReportDataGridView.CurrentRow.Cells[8].Value.ToString();
                modalUIIF.assemblyLine = inspectionReportDataGridView.CurrentRow.Cells[9].Value.ToString();
                modalUIIF.partNo = inspectionReportDataGridView.CurrentRow.Cells[10].Value.ToString();
                modalUIIF.rohsCompliance = inspectionReportDataGridView.CurrentRow.Cells[11].Value.ToString();
                modalUIIF.humidity = inspectionReportDataGridView.CurrentRow.Cells[12].Value.ToString();
                modalUIIF.inspectedDate = inspectionReportDataGridView.CurrentRow.Cells[13].Value.ToString();
                modalUIIF.receivedDate = inspectionReportDataGridView.CurrentRow.Cells[14].Value.ToString();
                modalUIIF.supplier = inspectionReportDataGridView.CurrentRow.Cells[15].Value.ToString();
                modalUIIF.maker = inspectionReportDataGridView.CurrentRow.Cells[16].Value.ToString();
                modalUIIF.inspector = inspectionReportDataGridView.CurrentRow.Cells[17].Value.ToString();
                modalUIIF.materialType = inspectionReportDataGridView.CurrentRow.Cells[18].Value.ToString();
                modalUIIF.productionType = inspectionReportDataGridView.CurrentRow.Cells[19].Value.ToString();
                modalUIIF.inspectionType = inspectionReportDataGridView.CurrentRow.Cells[20].Value.ToString();
                modalUIIF.oir = inspectionReportDataGridView.CurrentRow.Cells[21].Value.ToString();
                modalUIIF.testReport = inspectionReportDataGridView.CurrentRow.Cells[22].Value.ToString();
                modalUIIF.sampleSize = inspectionReportDataGridView.CurrentRow.Cells[23].Value.ToString();
                modalUIIF.ulMarking = inspectionReportDataGridView.CurrentRow.Cells[24].Value.ToString();
                modalUIIF.coc = inspectionReportDataGridView.CurrentRow.Cells[25].Value.ToString();
                modalUIIF.partname = inspectionReportDataGridView.CurrentRow.Cells[26].Value.ToString();
                modalUIIF.invoiceQty = inspectionReportDataGridView.CurrentRow.Cells[27].Value.ToString();
                modalUIIF.goodsCode = inspectionReportDataGridView.CurrentRow.Cells[28].Value.ToString();
                modalUIIF.materialCodeBoxSeqID = inspectionReportDataGridView.CurrentRow.Cells[29].Value.ToString();
                modalUIIF.date1 = inspectionReportDataGridView.CurrentRow.Cells[30].Value.ToString();

                modalUIIF.ShowDialog();
            }
                    
        }        

        private void appearanceInspectionDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(appearanceInspectionDataGridView.RowCount > 0)
            {
                var appearanceInspectionFormUpdateModal = new UpdateAppearanceInspectionForm();
                appearanceInspectionFormUpdateModal.id = appearanceInspectionDataGridView.CurrentRow.Cells[0].Value.ToString();
                appearanceInspectionFormUpdateModal.invoice_no = appearanceInspectionDataGridView.CurrentRow.Cells[1].Value.ToString();
                appearanceInspectionFormUpdateModal.ig_checkpoints = appearanceInspectionDataGridView.CurrentRow.Cells[2].Value.ToString();
                appearanceInspectionFormUpdateModal.instrument_used = appearanceInspectionDataGridView.CurrentRow.Cells[3].Value.ToString();
                appearanceInspectionFormUpdateModal.result = appearanceInspectionDataGridView.CurrentRow.Cells[4].Value.ToString();
                appearanceInspectionFormUpdateModal.judgement = appearanceInspectionDataGridView.CurrentRow.Cells[5].Value.ToString();
                appearanceInspectionFormUpdateModal.remarks = appearanceInspectionDataGridView.CurrentRow.Cells[6].Value.ToString();
                appearanceInspectionFormUpdateModal.goodsCode = appearanceInspectionDataGridView.CurrentRow.Cells[7].Value.ToString();
                appearanceInspectionFormUpdateModal.defectQTY = appearanceInspectionDataGridView.CurrentRow.Cells[8].Value.ToString();
                appearanceInspectionFormUpdateModal.defect_enc = appearanceInspectionDataGridView.CurrentRow.Cells[9].Value.ToString();
                appearanceInspectionFormUpdateModal.MaterialCodeBoxSeqID = appearanceInspectionDataGridView.CurrentRow.Cells[10].Value.ToString();
                appearanceInspectionFormUpdateModal.ShowDialog();
            }             
        }

        private void dimensionalCheckDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dimensionalCheckDataGridView.RowCount > 0)
            {
                var DimensionalCheckUpdateModal = new dimensionalCheckUpdateForm();
                DimensionalCheckUpdateModal.id = dimensionalCheckDataGridView.CurrentRow.Cells[0].Value.ToString();
                DimensionalCheckUpdateModal.invoiceNO = dimensionalCheckDataGridView.CurrentRow.Cells[1].Value.ToString();
                DimensionalCheckUpdateModal.checkpoints = dimensionalCheckDataGridView.CurrentRow.Cells[2].Value.ToString();
                DimensionalCheckUpdateModal.instrumentUsed = dimensionalCheckDataGridView.CurrentRow.Cells[3].Value.ToString();
                DimensionalCheckUpdateModal.sampleUnit = dimensionalCheckDataGridView.CurrentRow.Cells[4].Value.ToString();
                DimensionalCheckUpdateModal.sample1 = dimensionalCheckDataGridView.CurrentRow.Cells[5].Value.ToString();
                DimensionalCheckUpdateModal.sample2 = dimensionalCheckDataGridView.CurrentRow.Cells[6].Value.ToString();
                DimensionalCheckUpdateModal.sample3 = dimensionalCheckDataGridView.CurrentRow.Cells[7].Value.ToString();
                DimensionalCheckUpdateModal.sample4 = dimensionalCheckDataGridView.CurrentRow.Cells[8].Value.ToString();
                DimensionalCheckUpdateModal.sample5 = dimensionalCheckDataGridView.CurrentRow.Cells[9].Value.ToString();
                DimensionalCheckUpdateModal.sample6 = dimensionalCheckDataGridView.CurrentRow.Cells[10].Value.ToString();
                DimensionalCheckUpdateModal.sample7 = dimensionalCheckDataGridView.CurrentRow.Cells[11].Value.ToString();
                DimensionalCheckUpdateModal.sample8 = dimensionalCheckDataGridView.CurrentRow.Cells[12].Value.ToString();
                DimensionalCheckUpdateModal.sample9 = dimensionalCheckDataGridView.CurrentRow.Cells[13].Value.ToString();
                DimensionalCheckUpdateModal.sample10 = dimensionalCheckDataGridView.CurrentRow.Cells[14].Value.ToString();
                DimensionalCheckUpdateModal.minimum = dimensionalCheckDataGridView.CurrentRow.Cells[15].Value.ToString();
                DimensionalCheckUpdateModal.average = dimensionalCheckDataGridView.CurrentRow.Cells[16].Value.ToString();
                DimensionalCheckUpdateModal.maximum = dimensionalCheckDataGridView.CurrentRow.Cells[17].Value.ToString();
                DimensionalCheckUpdateModal.lowerSpecLimit = dimensionalCheckDataGridView.CurrentRow.Cells[18].Value.ToString();
                DimensionalCheckUpdateModal.upperSpecLimit = dimensionalCheckDataGridView.CurrentRow.Cells[19].Value.ToString();
                DimensionalCheckUpdateModal.judgement = dimensionalCheckDataGridView.CurrentRow.Cells[20].Value.ToString();
                DimensionalCheckUpdateModal.remarks = dimensionalCheckDataGridView.CurrentRow.Cells[21].Value.ToString();
                DimensionalCheckUpdateModal.goodsCode = dimensionalCheckDataGridView.CurrentRow.Cells[22].Value.ToString();
                DimensionalCheckUpdateModal.defectQty = dimensionalCheckDataGridView.CurrentRow.Cells[23].Value.ToString();
                DimensionalCheckUpdateModal.defectEnc = dimensionalCheckDataGridView.CurrentRow.Cells[24].Value.ToString();
                DimensionalCheckUpdateModal.materialCodeBox = dimensionalCheckDataGridView.CurrentRow.Cells[25].Value.ToString();
                DimensionalCheckUpdateModal.date1 = dimensionalCheckDataGridView.CurrentRow.Cells[26].Value.ToString();
                DimensionalCheckUpdateModal.remarksCheckPoint = dimensionalCheckDataGridView.CurrentRow.Cells[27].Value.ToString();
                DimensionalCheckUpdateModal.ShowDialog();
            }           
        }      

        

        private void editIncomingInspToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            var eventArgs = new DataGridViewCellEventArgs(0, 0);
            inspectionReportDataGridView.CurrentRow.Selected = true;
            inspectionReportDataGridView_CellDoubleClick(sender, eventArgs);
        }

        private void editAppearanceInspectionToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            var eventArgs = new DataGridViewCellEventArgs(0, 0);
            appearanceInspectionDataGridView.CurrentRow.Selected = true;
            appearanceInspectionDataGridView_CellDoubleClick(sender, eventArgs);
        }

        private void editDimensionalCheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var eventArgs = new DataGridViewCellEventArgs(0, 0);
            dimensionalCheckDataGridView.CurrentRow.Selected = true;
            dimensionalCheckDataGridView_CellDoubleClick(sender, eventArgs);
        }
        private void editFunctionalCheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var eventArgs = new DataGridViewCellEventArgs(0, 0);
            functionalCheckDataGridView.CurrentRow.Selected = true;
            functionalCheckDataGridView_CellDoubleClick(sender, eventArgs);
        }

        private void lotInformationDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (lotInformationDataGridView.RowCount > 0){
                editToolStripMenuItem_Click(sender, e);
            }
        }
        private void lotInformationDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (lotInformationDataGridView.RowCount > 0){
                if (lotInformationDataGridView.CurrentRow.Index > -1){
                    editToolStripMenuItem.Enabled = true;
                    return;
                }
                editToolStripMenuItem.Enabled = false;
            }
        }
        private void inspectionReportDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (inspectionReportDataGridView.RowCount > 0){
                if (inspectionReportDataGridView.CurrentRow.Index > -1){
                    editIncomingInspToolStripMenuItem2.Enabled = true;
                    return;
                }
                editIncomingInspToolStripMenuItem2.Enabled = false;
            }
        }
        private void appearanceInspectionDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(appearanceInspectionDataGridView.RowCount > 0){
                if(appearanceInspectionDataGridView.CurrentRow.Index > -1){
                    editAppearanceInspectionToolStripMenuItem3.Enabled = true;
                    return;
                }
                editAppearanceInspectionToolStripMenuItem3.Enabled = true;                
            }
        }
        private void dimensionalCheckDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dimensionalCheckDataGridView.RowCount > 0){
                if (dimensionalCheckDataGridView.CurrentRow.Index > -1){
                    editDimensionalCheckToolStripMenuItem.Enabled = true;
                    return;
                }
                editDimensionalCheckToolStripMenuItem.Enabled = false;
            }
        }
        private void functionalCheckDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(functionalCheckDataGridView.RowCount > 0){
                if(functionalCheckDataGridView.CurrentRow.Index > -1){
                    editFunctionalCheckToolStripMenuItem.Enabled = true;
                    return;
                }
                editFunctionalCheckToolStripMenuItem.Enabled = false;
            }
        }
        private void functionalCheckDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(functionalCheckDataGridView.RowCount > 0)
            {
                var functionalCheckUpdateModal = new FunctionalCheckUpdateForm();
                functionalCheckUpdateModal.id = functionalCheckDataGridView.CurrentRow.Cells[0].Value.ToString();
                functionalCheckUpdateModal.invoiceNO = functionalCheckDataGridView.CurrentRow.Cells[1].Value.ToString();
                functionalCheckUpdateModal.checkpoints = functionalCheckDataGridView.CurrentRow.Cells[2].Value.ToString();
                functionalCheckUpdateModal.instrumentUsed = functionalCheckDataGridView.CurrentRow.Cells[3].Value.ToString();
                functionalCheckUpdateModal.sampleUnit = functionalCheckDataGridView.CurrentRow.Cells[4].Value.ToString();
                functionalCheckUpdateModal.sample1 = functionalCheckDataGridView.CurrentRow.Cells[5].Value.ToString();
                functionalCheckUpdateModal.sample2 = functionalCheckDataGridView.CurrentRow.Cells[6].Value.ToString();
                functionalCheckUpdateModal.sample3 = functionalCheckDataGridView.CurrentRow.Cells[7].Value.ToString();
                functionalCheckUpdateModal.sample4 = functionalCheckDataGridView.CurrentRow.Cells[8].Value.ToString();
                functionalCheckUpdateModal.sample5 = functionalCheckDataGridView.CurrentRow.Cells[9].Value.ToString();
                functionalCheckUpdateModal.sample6 = functionalCheckDataGridView.CurrentRow.Cells[10].Value.ToString();
                functionalCheckUpdateModal.sample7 = functionalCheckDataGridView.CurrentRow.Cells[11].Value.ToString();
                functionalCheckUpdateModal.sample8 = functionalCheckDataGridView.CurrentRow.Cells[12].Value.ToString();
                functionalCheckUpdateModal.sample9 = functionalCheckDataGridView.CurrentRow.Cells[13].Value.ToString();
                functionalCheckUpdateModal.sample10 = functionalCheckDataGridView.CurrentRow.Cells[14].Value.ToString();
                functionalCheckUpdateModal.minimum = functionalCheckDataGridView.CurrentRow.Cells[15].Value.ToString();
                functionalCheckUpdateModal.average = functionalCheckDataGridView.CurrentRow.Cells[16].Value.ToString();
                functionalCheckUpdateModal.maximum = functionalCheckDataGridView.CurrentRow.Cells[17].Value.ToString();
                functionalCheckUpdateModal.lowerSpecLimit = functionalCheckDataGridView.CurrentRow.Cells[18].Value.ToString();
                functionalCheckUpdateModal.upperSpecLimit = functionalCheckDataGridView.CurrentRow.Cells[19].Value.ToString();
                functionalCheckUpdateModal.judgement = functionalCheckDataGridView.CurrentRow.Cells[20].Value.ToString();
                functionalCheckUpdateModal.remarks = functionalCheckDataGridView.CurrentRow.Cells[21].Value.ToString();
                functionalCheckUpdateModal.goodsCode = functionalCheckDataGridView.CurrentRow.Cells[22].Value.ToString();
                functionalCheckUpdateModal.defectQty = functionalCheckDataGridView.CurrentRow.Cells[23].Value.ToString();
                functionalCheckUpdateModal.defectEnc = functionalCheckDataGridView.CurrentRow.Cells[24].Value.ToString();
                functionalCheckUpdateModal.materialCodeBox = functionalCheckDataGridView.CurrentRow.Cells[25].Value.ToString();
                functionalCheckUpdateModal.date1 = functionalCheckDataGridView.CurrentRow.Cells[26].Value.ToString();
                functionalCheckUpdateModal.remarksCheckPoint = functionalCheckDataGridView.CurrentRow.Cells[27].Value.ToString();
                functionalCheckUpdateModal.ShowDialog();
            }
        }           

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var modalUpdateForm = new lotInformationUpdateForm();
                modalUpdateForm.lotInfoId = lotInformationDataGridView.CurrentRow.Cells[0].Value.ToString();
                modalUpdateForm.invoiceNo = lotInformationDataGridView.CurrentRow.Cells[1].Value.ToString();
                modalUpdateForm.partNo = lotInformationDataGridView.CurrentRow.Cells[2].Value.ToString();
                modalUpdateForm.partName = lotInformationDataGridView.CurrentRow.Cells[3].Value.ToString();
                modalUpdateForm.totalQty = lotInformationDataGridView.CurrentRow.Cells[4].Value.ToString();
                modalUpdateForm.quantityReceived = lotInformationDataGridView.CurrentRow.Cells[5].Value.ToString();
                modalUpdateForm.lotNo = lotInformationDataGridView.CurrentRow.Cells[6].Value.ToString();
                modalUpdateForm.lotQty = lotInformationDataGridView.CurrentRow.Cells[7].Value.ToString();
                modalUpdateForm.boxNumber = lotInformationDataGridView.CurrentRow.Cells[8].Value.ToString();
                modalUpdateForm.reject = lotInformationDataGridView.CurrentRow.Cells[9].Value.ToString();
                modalUpdateForm.sampleSize = lotInformationDataGridView.CurrentRow.Cells[10].Value.ToString();
                modalUpdateForm.goodsCode = lotInformationDataGridView.CurrentRow.Cells[11].Value.ToString();
                modalUpdateForm.materialCodeNo = lotInformationDataGridView.CurrentRow.Cells[12].Value.ToString();
                modalUpdateForm.remarks = lotInformationDataGridView.CurrentRow.Cells[13].Value.ToString();
                modalUpdateForm.date1 = lotInformationDataGridView.CurrentRow.Cells[14].Value.ToString();
                modalUpdateForm.kenId = lotInformationDataGridView.CurrentRow.Cells[15].Value.ToString();
                modalUpdateForm.diff = lotInformationDataGridView.CurrentRow.Cells[16].Value.ToString();
                modalUpdateForm.totalGood = lotInformationDataGridView.CurrentRow.Cells[17].Value.ToString();

                modalUpdateForm.ShowDialog();
            }
            catch (Exception)
            {
            }           
        }     
        private void frmEdit_Data_Load(object sender, EventArgs e)
        {
            //this.reportViewer1.RefreshReport();
            comboBox1.Items.Clear();
            comboBox1.AutoCompleteCustomSource.Clear();
            comboBox1.Items.AddRange(DBHelper.ComboBoxItems("LotNumber", "invoice_no", 0).ToArray());
            comboBox1.AutoCompleteCustomSource.AddRange(DBHelper.ComboBoxItems("LotNumber", "invoice_no", 0).ToArray());

            SqlCommand command = new SqlCommand("SP_Latest_Log", DBHelper.connection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                employeeNumberTextBox.Text = reader["Employee_Number"].ToString();
                employeeNameTextBox.Text = reader["Employee_Name"].ToString();
            }
            reader.Close();

            MessageBox.Show("NOTE:\n\n" +
                "WHEN EDITING:\n" +
                "- Do not change the id, invoice_no, MaterialCodeBoxSeqID.\n\n" +
                "WHEN DELETING:\n" +
                "- Make sure that the selected invoice and part no. is correct.\n" +
                "- System can't undo changes",
                "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            comboBox2.AutoCompleteCustomSource.Clear();
            comboBox2.Text = "";
            comboBox2.Items.AddRange(DBHelper.ComboBoxItems("LotNumber WHERE invoice_no = '" + comboBox1.Text + "'", "MaterialCodeBoxSeqID", 0).ToArray());
            comboBox2.AutoCompleteCustomSource.AddRange(DBHelper.ComboBoxItems("LotNumber WHERE invoice_no = '" + comboBox1.Text + "'", "MaterialCodeBoxSeqID", 0).ToArray());
        }       

        public void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
       {
            string errorMessage = "";
            try
            {
                errorMessage = "Error on Lot Information.";
                lotInformationDataGridView.Rows.Clear();
                query = "SELECT * FROM LotNumber WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'";
                SqlCommand command2 = new SqlCommand(query, DBHelper.connection);
                var reader2 = command2.ExecuteReader();
                while (reader2.Read())
                {
                    var a = reader2["id"];
                    var b = reader2["invoice_no"];
                    var c = reader2["part_no"];
                    var d = reader2["part_name"];
                    var f = reader2["total_quantity"];
                    var g = reader2["quantity_recieved"];
                    var h = reader2["lot_no"];
                    var i = reader2["lot_quantity"];                   
                    var j = reader2["box_number"];
                    var k = reader2["reject"];
                    var l = reader2["sample_size"];
                    var m = reader2["goodsCode"];
                    var n = reader2["MaterialCodeBoxSeqID"];
                    var o = reader2["remarks"];
                    var p = reader2.IsDBNull(14) ? reader2["Date"] : reader2.GetDateTime(14);
                    var q = reader2["KEN_ID"];
                    var r = reader2["DIFF"];
                    var s = reader2["Total_good"];
                    
                    lotInformationDataGridView.Rows.Add(a, b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s);
                }
                reader2.Close();


                errorMessage = "Error on Inspection Report.";
                inspectionReportDataGridView.Rows.Clear();
                query = "SELECT * FROM inspectiondata WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'";
                SqlCommand command3 = new SqlCommand(query, DBHelper.connection);
                var reader3 = command3.ExecuteReader();
                while (reader3.Read())
                {
                    var a = reader3["id"];
                    var b = reader3["invoice_no"];
                    var c = reader3["approved"];
                    var d = reader3["checked"];
                    var f = reader3["prepared"];
                    var g = reader3.IsDBNull(5) ? reader3["approved_date"] :  reader3.GetDateTime(5).ToString("yyyy-MM-dd");
                    var h = reader3.IsDBNull(6) ? reader3["checked_date"] : reader3.GetDateTime(6).ToString("yyyy-MM-dd");
                    var i = reader3.IsDBNull(7) ? reader3["prepared_date"] : reader3.GetDateTime(7).ToString("yyyy-MM-dd");
                    var j = reader3["temperature"];
                    var k = reader3["assembly_line"];
                    var l = reader3["part_number"];
                    var m = reader3["rohs_compliance"];
                    var n = reader3["humidity"];
                    var o = reader3.IsDBNull(13) ? reader3["inspected_date"] : reader3.GetDateTime(13).ToString("yyyy-MM-dd");
                    var p = reader3.IsDBNull(14) ? reader3["recieved_date"] : reader3.GetDateTime(14).ToString("yyyy-MM-dd");
                    var q = reader3["supplier"];
                    var r = reader3["maker"];
                    var s = reader3["inspector"];
                    var t = reader3["material_type"];
                    var u = reader3["production_type"];
                    var v = reader3["inspection_type"];
                    var w = reader3["oir"];
                    var x = reader3["test_report"];
                    var y = reader3["sample_size"];
                    var z = reader3["ul_marking"];
                    var z1 = reader3["coc"];
                    var z2 = reader3["partname"];
                    var z3 = reader3["invoicequant"];
                    var z4 = reader3["goodsCode"];
                    var z5 = reader3["MaterialCodeBoxSeqID"];
                    var z6 = reader3.IsDBNull(30) ? reader3["Date"] : reader3.GetDateTime(30);
                    inspectionReportDataGridView.Rows.Add(a, b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, z1, z2, z3, z4, z5, z6);
                }
                reader3.Close();                

                errorMessage = "Error on appearance inspection.";
                appearanceInspectionDataGridView.Rows.Clear();
                query = "SELECT * FROM Appearance_Inspection WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'";
                SqlCommand command = new SqlCommand(query, DBHelper.connection);
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var a = reader["id"];
                    var b = reader["invoice_no"];
                    var c = reader["ig_checkpoints"];
                    var d = reader["instrument_used"];
                    var f = reader["result"];
                    var g = reader["judgement"];
                    var h = reader["remarks"];
                    var i = reader["goodsCOde"];
                    var j = reader["defectqty"];
                    var k = reader["defect_enc"];
                    var l = reader["MaterialCodeBoxSeqID"];
                    appearanceInspectionDataGridView.Rows.Add(a, b, c, d, f, g, h, i, j, k, l);
                }
                reader.Close();

                errorMessage = "Error on Dimensional check.";
                dimensionalCheckDataGridView.Rows.Clear();
                query = "SELECT * FROM DimensionalCheck WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'";
                SqlCommand command4 = new SqlCommand(query, DBHelper.connection);
                var reader4 = command4.ExecuteReader();
                while (reader4.Read())
                {
                    var a = reader4["id"];
                    var b = reader4["invoice_no"];
                    var c = reader4["checkpoints"];
                    var d = reader4["instrument_used"];
                    var f = reader4["sample_unit"];
                    var g = reader4["sample1"];
                    var h = reader4["sample2"];
                    var i = reader4["sample3"];
                    var j = reader4["sample4"];
                    var k = reader4["sample5"];
                    var l = reader4["sample6"];
                    var m = reader4["sample7"];
                    var n = reader4["sample8"];
                    var o = reader4["sample9"];
                    var p = reader4["sample10"];
                    var q = reader4["minimum"];
                    var r = reader4["average"];
                    var s = reader4["maximum"];
                    var t = reader4["lower_spec_limit"];
                    var u = reader4["upper_spec_limit"];
                    var v = reader4["judgement"];
                    var w = reader4["remarks"];
                    var x = reader4["goodsCode"];
                    var y = reader4["defectqty"];
                    var z = reader4["defect_enc"];
                    var z1 = reader4["MaterialCodeBoxSeqID"];
                    var z2 = reader4["Date"];
                    var z3 = reader4["Remarks_chckpoint"];
                    dimensionalCheckDataGridView.Rows.Add(a, b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, z1, z2, z3);
                }
                reader4.Close();

                errorMessage = "Error on functional check.";
                functionalCheckDataGridView.Rows.Clear();
                query = "SELECT * FROM FunctionalCheck WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'";
                SqlCommand command5 = new SqlCommand(query, DBHelper.connection);
                var reader5 = command5.ExecuteReader();
                while (reader5.Read())
                {
                    var a = reader5["id"];
                    var b = reader5["invoice_no"];
                    var c = reader5["checkpoints"];
                    var d = reader5["instrument_used"];
                    var f = reader5["sample_unit"];
                    var g = reader5["sample1"];
                    var h = reader5["sample2"];
                    var i = reader5["sample3"];
                    var j = reader5["sample4"];
                    var k = reader5["sample5"];
                    var l = reader5["sample6"];
                    var m = reader5["sample7"];
                    var n = reader5["sample8"];
                    var o = reader5["sample9"];
                    var p = reader5["sample10"];
                    var q = reader5["minimum"];
                    var r = reader5["average"];
                    var s = reader5["maximum"];
                    var t = reader5["lower_spec_limit"];
                    var u = reader5["upper_spec_limit"];
                    var v = reader5["judgement"];
                    var w = reader5["remarks"];
                    var x = reader5["goodsCode"];
                    var y = reader5["defectqty"];
                    var z = reader5["defect_enc"];                    
                    var z1 = reader5["MaterialCodeBoxSeqID"];
                    var z2 = reader5["Date"];
                    var z3 = reader5["[Remarks_chckpoint"];
                    functionalCheckDataGridView.Rows.Add(a, b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, z1, z2, z3);
                }
                reader5.Close();
            }
            catch (Exception)
            {
                MessageBox.Show(errorMessage);
            }
            if(lotInformationDataGridView.RowCount > 0 ||
                inspectionReportDataGridView.RowCount > 0 ||
                appearanceInspectionDataGridView.RowCount > 0 ||
                dimensionalCheckDataGridView.RowCount > 0 || 
                functionalCheckDataGridView.RowCount > 0)
            {
                deleteToolStripMenuItem.Enabled = true;
                deleteToolStripMenuItem1.Enabled = true;
                deleteToolStripMenuItem2.Enabled = true;
                deleteToolStripMenuItem3.Enabled = true;
                deleteToolStripMenuItem4.Enabled = true;
            }
            else
            {
                deleteToolStripMenuItem.Enabled = false;
                deleteToolStripMenuItem1.Enabled = false;
                deleteToolStripMenuItem2.Enabled = false;
                deleteToolStripMenuItem3.Enabled = false;
                deleteToolStripMenuItem4.Enabled = false;
            }
        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();        
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "" && comboBox2.Text != "")
            {
                try
                {
                    var option = MessageBox.Show("ARE YOU SURE TO DELETE THE ENTIRE DATA (LOT INFORMATION, INCOMING INSPECTION, APPEARANCE INSPECTION, DIMENSION INSPECTION, AND FUNCTIONAL INSPECTION) OF " + comboBox2.Text + "?", "IQC System - Admin", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (option == DialogResult.OK)
                    {
                        SqlCommand command = new SqlCommand("DELETE FROM LotNumber WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
                        command.ExecuteNonQuery();
                        SqlCommand command1 = new SqlCommand("DELETE FROM inspectiondata WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
                        command.ExecuteNonQuery();
                        SqlCommand command2 = new SqlCommand("DELETE FROM Appearance_Inspection WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
                        command.ExecuteNonQuery();
                        SqlCommand command3 = new SqlCommand("DELETE FROM DimensionalCheck WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
                        command.ExecuteNonQuery();
                        SqlCommand command4 = new SqlCommand("DELETE FROM FunctionalCheck WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
                        command.ExecuteNonQuery();
                        comboBox2_SelectedIndexChanged(sender, e);
                        MessageBox.Show("Deletion Successful!", "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //THESE CODES SAVES THE DELETION OF DATA TO ACTIVITY LOG
                        try
                        {
                            var action = "DELETED THE ENTIRETY OF DATA WITH \x0A" +
                                         "INVOICE NUMBER  :  ' " + comboBox1.Text +
                                         " '\x0APART NUMBER  :  ' " + comboBox2.Text + " '";
                            SqlCommand insertCommand = new SqlCommand("SP_Insert_Activity", DBHelper.connection);
                            insertCommand.CommandType = CommandType.StoredProcedure;
                            insertCommand.Parameters.AddWithValue("@EmployeeNumber", employeeNumberTextBox.Text);
                            insertCommand.Parameters.AddWithValue("@EmployeeName", employeeNameTextBox.Text);
                            insertCommand.Parameters.AddWithValue("@Action", action);
                            insertCommand.ExecuteNonQuery();
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }                
            }
            else
            {
                MessageBox.Show("NO DATA TO DELETE, GENERATE DATA FIRST!", "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            deleteToolStripMenuItem_Click(sender, e);
        }

        private void deleteToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            deleteToolStripMenuItem_Click(sender, e);
        }

        private void deleteToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            deleteToolStripMenuItem_Click(sender, e);
        }

        private void deleteToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            deleteToolStripMenuItem_Click(sender, e);
        }

        private void frmEdit_Data_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                SqlCommand insertCommand = new SqlCommand("SP_Insert_Activity", DBHelper.connection);
                insertCommand.CommandType = CommandType.StoredProcedure;
                insertCommand.Parameters.AddWithValue("@EmployeeNumber", employeeNumberTextBox.Text);
                insertCommand.Parameters.AddWithValue("@EmployeeName", employeeNameTextBox.Text);
                insertCommand.Parameters.AddWithValue("@Action", "Closes the Edit Form.");
                insertCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void frmEdit_Data_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode.ToString() == "D")
            {
                deleteToolStripMenuItem_Click(sender, e);
            }
        }
    }
}