﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using ClosedXML.Excel;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(DBHelper.connection.State == ConnectionState.Closed){
                DBHelper.connection.Open();
            }
        }
        #region DesigningForm
        private int tolerance = 12;
        private const int WM_NCHITTEST = 132;
        private const int HTBOTTOMRIGHT = 17;
        private Rectangle sizeGripRectangle;
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    base.WndProc(ref m);
                    var hitPoint = this.PointToClient(new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16));
                    if (sizeGripRectangle.Contains(hitPoint))
                        m.Result = new IntPtr(HTBOTTOMRIGHT);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        //Main panel sizing
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            var region = new Region(new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));
            sizeGripRectangle = new Rectangle(this.ClientRectangle.Width - tolerance, this.ClientRectangle.Height - tolerance, tolerance, tolerance);
            region.Exclude(sizeGripRectangle);
            this.bgPanel.Region = region;
            this.Invalidate();
        }
        //SizingGrip
        protected override void OnPaint(PaintEventArgs e)
        {
            SolidBrush blueBrush = new SolidBrush(Color.FromArgb(244, 244, 244));
            e.Graphics.FillRectangle(blueBrush, sizeGripRectangle);
            base.OnPaint(e);
            ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, sizeGripRectangle);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        int lx, ly;
        int sw, sh;
        private void btn_maximize_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;

            btn_maximize.Visible = false;
            btn_normal.Visible = true;

            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }
        private void btn_normal_Click(object sender, EventArgs e)
        {
            btn_maximize.Visible = true;
            btn_normal.Visible = false;
            this.Size = new Size(sw, sh);
            this.Location = new Point(lx, ly);
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        private void btn_minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panelTitle_MouseMove(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        #endregion

        //for Main panel
        private void CallingForms<formname>() where formname : Form, new()
        {
            Form forms;
            forms = panelMain.Controls.OfType<formname>().FirstOrDefault();

            if (forms == null)
            {
                forms = new formname();
                forms.TopLevel = false;
                forms.FormBorderStyle = FormBorderStyle.None;//pede icomment for different approach
                forms.Dock = DockStyle.Fill;//pede icomment for different approach
                forms.WindowState = FormWindowState.Maximized;
                panelMain.Controls.Add(forms);
                panelMain.Tag = forms;
                forms.Show();
                forms.BringToFront();
                forms.FormClosed += new FormClosedEventHandler(CloseForms);
            }
            else
            {
                forms.BringToFront();
            }
        }
        private void first_button_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(60, 83, 147);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);
            activityLogButton.BackColor = Color.FromArgb(204, 205, 207);

            if (Application.OpenForms.OfType<frmEdit_Data>().Any()){
                Application.OpenForms["frmEdit_Data"].Close();
            }
            if (Application.OpenForms.OfType<SecondForm>().Any()){
                Application.OpenForms["SecondForm"].Close();
            }
            if (Application.OpenForms.OfType<ActivityLogForm>().Any()){
                Application.OpenForms["ActivityLogForm"].Close();
            }
            adminCredentialsGroup.Visible = false;
            CallingForms<FirstForm>();
        }
        private void second_button_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(60, 83, 147);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);
            activityLogButton.BackColor = Color.FromArgb(204, 205, 207);

            if (Application.OpenForms.OfType<frmEdit_Data>().Any()){
                Application.OpenForms["frmEdit_Data"].Close();
            }
            if (Application.OpenForms.OfType<FirstForm>().Any()){
                Application.OpenForms["FirstForm"].Close();
            }
            if (Application.OpenForms.OfType<ActivityLogForm>().Any()){
                Application.OpenForms["ActivityLogForm"].Close();
            }
            adminCredentialsGroup.Visible = false;
            CallingForms<SecondForm>();           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);            
            editButton.BackColor = Color.FromArgb(60, 83, 147);
            activityLogButton.BackColor = Color.FromArgb(204, 205, 207);

            if (Application.OpenForms.OfType<FirstForm>().Any()){
                Application.OpenForms["FirstForm"].Close();
            }
            if (Application.OpenForms.OfType<SecondForm>().Any()){
                Application.OpenForms["SecondForm"].Close();
            }
            if (Application.OpenForms.OfType<ActivityLogForm>().Any()){
                Application.OpenForms["ActivityLogForm"].Close();
            }
            adminCredentialsGroup.Visible = true;
            empNumber_textBox.Focus();            
        }
        private void enterButton_Click(object sender, EventArgs e)
        {            
            KeyPressEventArgs arg = new KeyPressEventArgs(Convert.ToChar(Keys.Enter));
            password_textBox_KeyPress(sender, arg);
        }
        private void empNumber_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            password_textBox_KeyPress(sender, e);
        }

        public string employeeNumber = "";
        public string employeeName = "";
       
        private void password_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            frmEdit_Data EditForm = new frmEdit_Data();
            if (e.KeyChar == (char)Keys.Enter){
                int result;
                if (int.TryParse(empNumber_textBox.Text, out result)){
                    if (password_textBox.Text == ""){
                        MessageBox.Show("Password is required!", "IQC Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        password_textBox.Clear();
                        password_textBox.Focus();
                        return;
                    }
                }else{
                    MessageBox.Show("Employee number must be an integer!", "IQC Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    empNumber_textBox.Clear();
                    password_textBox.Clear();
                    empNumber_textBox.Focus();
                    return;
                }
                SqlConnection GlobalAccessConnection = new SqlConnection("" +
                "Data Source=192.168.2.14;" +
                "Initial Catalog=GlobalAccess;" +
                "Persist Security Info=True;" +
                "User ID=software;Password=specialist");
                GlobalAccessConnection.Open();
                try{
                    var userFound = 0;
                    var query = "SELECT * FROM tblEnrolled_List " +
                        "WHERE empNum = " + empNumber_textBox.Text +
                        " AND password = '" + password_textBox.Text +
                        "' AND Admin = 1 " +
                        "AND Software_Name = 'IQC_ADMIN'";                    
                    SqlCommand command = new SqlCommand(query, GlobalAccessConnection);
                    var reader = command.ExecuteReader();
                    while (reader.Read()){
                        userFound += 1;
                        employeeNumber = reader["empNum"].ToString();
                        employeeName = reader["Full_Name"].ToString();
                    }
                    reader.Close();
                    GlobalAccessConnection.Close();
                    if (userFound > 0){
                        empNumber_textBox.Clear();
                        password_textBox.Text = "";
                        adminCredentialsGroup.Visible = false;

                        //SAVE USERS INFO UPON ENTERING THE EDIT FORM                        
                        SqlCommand insertCommand = new SqlCommand("SP_Insert_Activity", DBHelper.connection);
                        insertCommand.CommandType = CommandType.StoredProcedure;
                        insertCommand.Parameters.AddWithValue("@EmployeeNumber", employeeNumber);
                        insertCommand.Parameters.AddWithValue("@EmployeeName", employeeName);
                        insertCommand.Parameters.AddWithValue("@Action", "Opens the Edit Form.");
                        insertCommand.ExecuteNonQuery();
                        //ENDS HERE
                        
                        CallingForms<frmEdit_Data>();
                    }else{
                        var opt = MessageBox.Show("Invalid Credentials!", "IQC Admin",
                                MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                        if (opt == DialogResult.Retry){
                            empNumber_textBox.Text = "";
                            password_textBox.Text = "";
                            empNumber_textBox.Focus();
                        }else{
                            empNumber_textBox.Clear();
                            password_textBox.Text = "";
                            adminCredentialsGroup.Visible = false;
                        }
                    }
                }catch (Exception ex){
                    MessageBox.Show(ex.Message);
                }                
            }            
        }

        private void powerBi_Click(object sender, EventArgs e)
        {
            adminCredentialsGroup.Visible = false;
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(60, 83, 147);
            editButton.BackColor = Color.FromArgb(204, 205, 207);
            activityLogButton.BackColor = Color.FromArgb(204, 205, 207);

            ProcessStartInfo sInfo1 = new ProcessStartInfo("https://app.powerbi.com/view?r=eyJrIjoiOGUxNzExN2UtM2FmMS00NjFlLTgwZTItZjZkM2I0OTBjOGE3IiwidCI6IjUxMTFmMDI3LTVkNzYtNDcxNy1hY2ZmLTVlNzgyOTViODgzMSIsImMiOjEwfQ%3D%3D&pageName=ReportSection");
            Process.Start(sInfo1);            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if ((DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() +":" + DateTime.Now.Second.ToString()) == "13:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "15:1:0" ||
            //   (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "17:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString()) == "10:1:0" || 
            //   (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "22:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "1:1:0" ||
            //   (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "3:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString()) == "5:1:0")
            //{
            //    ProcessStartInfo sInfo2 = new ProcessStartInfo("https://app.powerbi.com/view?r=eyJrIjoiOGUxNzExN2UtM2FmMS00NjFlLTgwZTItZjZkM2I0OTBjOGE3IiwidCI6IjUxMTFmMDI3LTVkNzYtNDcxNy1hY2ZmLTVlNzgyOTViODgzMSIsImMiOjEwfQ%3D%3D&pageName=ReportSection");
            //    Process.Start(sInfo2);
            //}
        }

        private void panelMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void password_Enter(object sender, EventArgs e)
        {

        }

        private void password_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void panelMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelTitle_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bgPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelMain_Paint_1(object sender, PaintEventArgs e)
        {

        }       

        private void empNumber_textBox_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void xLabel_Click(object sender, EventArgs e)
        {
            empNumber_textBox.Clear();
            password_textBox.Clear();
            adminCredentialsGroup.Visible = false;
        }

        private void xLabel_MouseHover(object sender, EventArgs e)
        {
            xLabel.BackColor = Color.Red;
        }

        private void xLabel_MouseLeave(object sender, EventArgs e)
        {
            xLabel.BackColor = Color.FromArgb(60, 83, 147);
        }       

        private void cancelButton_Click(object sender, EventArgs e)
        {
            xLabel_Click(sender, e);
        }

        private void activityLogButton_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);
            activityLogButton.BackColor = Color.FromArgb(60, 83, 147);

            if (Application.OpenForms.OfType<FirstForm>().Any()){
                Application.OpenForms["FirstForm"].Close();
            }
            if (Application.OpenForms.OfType<SecondForm>().Any()){
                Application.OpenForms["SecondForm"].Close();
            }
            if (Application.OpenForms.OfType<frmEdit_Data>().Any()){
                Application.OpenForms["frmEdit_Data"].Close();
            }
            adminCredentialsGroup.Visible = false;
            CallingForms<ActivityLogForm>();
        }

        private void third_button_Click(object sender, EventArgs e)
        {
            adminCredentialsGroup.Visible = false;
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(60, 83, 147);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);
            activityLogButton.BackColor = Color.FromArgb(204, 205, 207);

            //Exporting to Excel
            string folderPath = @"\\192.168.1.11\Published Installers\IQC - Windows\IQC Admin (Resources)\References\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(DBHelper.exportData("inspectiondata"), "InspectionData");
                wb.Worksheets.Add(DBHelper.exportData("DimensionalCheck"), "DimensionalCheck");
                wb.Worksheets.Add(DBHelper.exportData("Appearance_Inspection"), "Appearance_Inspection");
                wb.Worksheets.Add(DBHelper.exportData("FunctionalCheck"), "FunctionalCheck");
                wb.Worksheets.Add(DBHelper.exportData("tblOverall_Judgement"), "OverallJudgement");
                wb.SaveAs(folderPath + "Data.xlsx");
            }
            ProcessStartInfo sInfo2 = new ProcessStartInfo("file://192.168.1.11/Published%20Installers/IQC%20-%20Windows/IQC%20Admin%20(Resources)/DailyReport_v3.0.xlsx");
            Process.Start(sInfo2);            
        }
        private void CloseForms(object sender, FormClosedEventArgs e)
        {
            //if(Application.OpenForms["FirstForm"] == null)
            //{
            //    first_button.BackColor = Color.FromArgb(4, 41, 68);
            //}
            //if (Application.OpenForms["SecondForm"] == null)
            //{
            //    second_button.BackColor = Color.FromArgb(4, 41, 68);
            //}
            //if (Application.OpenForms["ThirdForm"] == null)
            //{
            //    button1.BackColor = Color.FromArgb(4, 41, 68);
            //}
        }
    }
}
