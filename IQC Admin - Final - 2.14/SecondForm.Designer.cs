﻿
namespace IQCAdmin
{
    partial class SecondForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource5 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource6 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecondForm));
            this.SP_SummaryReport_InspectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iQCDatabaseDataSet = new IQCAdmin.IQCDatabaseDataSet1();
            this.sPSummaryReportLotNo2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.SP_SummaryReport_LotNoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sPSummaryReportAppearanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sPSummaryReportInspectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SP_SummaryReport_AppearanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SP_SummaryReport_DimensionalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SP_SummaryReport_FunctionalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sP_SummaryReport_AppearanceTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_AppearanceTableAdapter();
            this.sP_SummaryReport_InspectionTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_InspectionTableAdapter();
            this.SP_SummaryReport_FunctionalTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_FunctionalTableAdapter();
            this.sP_SummaryReport_LotNo2TableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_LotNo2TableAdapter();
            this.sP_SummaryReport_DimensionalTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_DimensionalTableAdapter();
            this.sP_SummaryReport_LotNoTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_LotNoTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_InspectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iQCDatabaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sPSummaryReportLotNo2BindingSource)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_LotNoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sPSummaryReportAppearanceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sPSummaryReportInspectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_AppearanceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_DimensionalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_FunctionalBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // SP_SummaryReport_InspectionBindingSource
            // 
            this.SP_SummaryReport_InspectionBindingSource.DataMember = "SP_SummaryReport_Inspection";
            this.SP_SummaryReport_InspectionBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // iQCDatabaseDataSet
            // 
            this.iQCDatabaseDataSet.DataSetName = "IQCDatabaseDataSet";
            this.iQCDatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sPSummaryReportLotNo2BindingSource
            // 
            this.sPSummaryReportLotNo2BindingSource.DataMember = "SP_SummaryReport_LotNo2";
            this.sPSummaryReportLotNo2BindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(0, 95);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1280, 51);
            this.panel3.TabIndex = 4;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(554, 5);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(349, 32);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gainsboro;
            this.label2.Location = new System.Drawing.Point(412, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Invoice Number:";
            // 
            // reportViewer1
            // 
            this.reportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            reportDataSource5.Name = "InspectionDS";
            reportDataSource5.Value = this.SP_SummaryReport_InspectionBindingSource;
            reportDataSource6.Name = "LotNoDS";
            reportDataSource6.Value = this.sPSummaryReportLotNo2BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource5);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource6);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "IQCAdmin.LotNumber.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 150);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(2);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(1280, 780);
            this.reportViewer1.TabIndex = 9;
            this.reportViewer1.ZoomPercent = 150;
            this.reportViewer1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.reportViewer1_KeyDown);
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(1251, 3);
            this.btn_close.Margin = new System.Windows.Forms.Padding(2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 26);
            this.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_close.TabIndex = 7;
            this.btn_close.TabStop = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Location = new System.Drawing.Point(0, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 94);
            this.panel2.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(354, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(670, 44);
            this.label1.TabIndex = 11;
            this.label1.Text = "[ GENERATE LOT FORM REPORT ]";
            // 
            // SP_SummaryReport_LotNoBindingSource
            // 
            this.SP_SummaryReport_LotNoBindingSource.DataMember = "SP_SummaryReport_LotNo";
            this.SP_SummaryReport_LotNoBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // sPSummaryReportAppearanceBindingSource
            // 
            this.sPSummaryReportAppearanceBindingSource.DataMember = "SP_SummaryReport_Appearance";
            this.sPSummaryReportAppearanceBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // sPSummaryReportInspectionBindingSource
            // 
            this.sPSummaryReportInspectionBindingSource.DataMember = "SP_SummaryReport_Inspection";
            this.sPSummaryReportInspectionBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // SP_SummaryReport_AppearanceBindingSource
            // 
            this.SP_SummaryReport_AppearanceBindingSource.DataMember = "SP_SummaryReport_Appearance";
            this.SP_SummaryReport_AppearanceBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // SP_SummaryReport_DimensionalBindingSource
            // 
            this.SP_SummaryReport_DimensionalBindingSource.DataMember = "SP_SummaryReport_Dimensional";
            this.SP_SummaryReport_DimensionalBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // SP_SummaryReport_FunctionalBindingSource
            // 
            this.SP_SummaryReport_FunctionalBindingSource.DataMember = "SP_SummaryReport_Functional";
            this.SP_SummaryReport_FunctionalBindingSource.DataSource = this.iQCDatabaseDataSet;
            // 
            // sP_SummaryReport_AppearanceTableAdapter
            // 
            this.sP_SummaryReport_AppearanceTableAdapter.ClearBeforeFill = true;
            // 
            // sP_SummaryReport_InspectionTableAdapter
            // 
            this.sP_SummaryReport_InspectionTableAdapter.ClearBeforeFill = true;
            // 
            // SP_SummaryReport_FunctionalTableAdapter
            // 
            this.SP_SummaryReport_FunctionalTableAdapter.ClearBeforeFill = true;
            // 
            // sP_SummaryReport_LotNo2TableAdapter
            // 
            this.sP_SummaryReport_LotNo2TableAdapter.ClearBeforeFill = true;
            // 
            // sP_SummaryReport_DimensionalTableAdapter
            // 
            this.sP_SummaryReport_DimensionalTableAdapter.ClearBeforeFill = true;
            // 
            // sP_SummaryReport_LotNoTableAdapter
            // 
            this.sP_SummaryReport_LotNoTableAdapter.ClearBeforeFill = true;
            // 
            // SecondForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 941);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SecondForm";
            this.Text = "SecondForm";
            this.Load += new System.EventHandler(this.SecondForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SecondForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_InspectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iQCDatabaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sPSummaryReportLotNo2BindingSource)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_LotNoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sPSummaryReportAppearanceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sPSummaryReportInspectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_AppearanceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_DimensionalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_FunctionalBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_AppearanceTableAdapter sP_SummaryReport_AppearanceTableAdapter;
        private System.Windows.Forms.BindingSource sPSummaryReportAppearanceBindingSource;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_InspectionTableAdapter sP_SummaryReport_InspectionTableAdapter;
        private System.Windows.Forms.BindingSource sPSummaryReportInspectionBindingSource;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.BindingSource SP_SummaryReport_LotNoBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_InspectionBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_AppearanceBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_DimensionalBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_FunctionalBindingSource;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.Panel panel2;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_FunctionalTableAdapter SP_SummaryReport_FunctionalTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource sPSummaryReportLotNo2BindingSource;
        private IQCDatabaseDataSet1 iQCDatabaseDataSet;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_LotNo2TableAdapter sP_SummaryReport_LotNo2TableAdapter;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_DimensionalTableAdapter sP_SummaryReport_DimensionalTableAdapter;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_LotNoTableAdapter sP_SummaryReport_LotNoTableAdapter;
    }
}