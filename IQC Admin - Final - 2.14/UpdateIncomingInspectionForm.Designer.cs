﻿
namespace IQCAdmin
{
    partial class UpdateIncomingInspectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.closeLabel = new System.Windows.Forms.Label();
            this.incomingInspectionFormlabel30 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.incInspecIdTextBox = new System.Windows.Forms.TextBox();
            this.invoiceNotextBox = new System.Windows.Forms.TextBox();
            this.approvedtextBox = new System.Windows.Forms.TextBox();
            this.checkedTextBox = new System.Windows.Forms.TextBox();
            this.preparedTextBox = new System.Windows.Forms.TextBox();
            this.temperatureTextBox = new System.Windows.Forms.TextBox();
            this.assemblyLineTextBox = new System.Windows.Forms.TextBox();
            this.partNoTextBox = new System.Windows.Forms.TextBox();
            this.oirTextBox11 = new System.Windows.Forms.TextBox();
            this.inspectionTypetextBox12 = new System.Windows.Forms.TextBox();
            this.productionTypetextBox13 = new System.Windows.Forms.TextBox();
            this.materialTypetextBox14 = new System.Windows.Forms.TextBox();
            this.inspectortextBox15 = new System.Windows.Forms.TextBox();
            this.makertextBox16 = new System.Windows.Forms.TextBox();
            this.suppliertextBox17 = new System.Windows.Forms.TextBox();
            this.humidityTextBox = new System.Windows.Forms.TextBox();
            this.rohsComplianceTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.materialBoxTextBox25 = new System.Windows.Forms.TextBox();
            this.goodsCodetextBox26 = new System.Windows.Forms.TextBox();
            this.invoiceQTYtextBox27 = new System.Windows.Forms.TextBox();
            this.partNametextBox28 = new System.Windows.Forms.TextBox();
            this.coctextBox29 = new System.Windows.Forms.TextBox();
            this.ulMarkingtextBox30 = new System.Windows.Forms.TextBox();
            this.sampleSizetextBox31 = new System.Windows.Forms.TextBox();
            this.testReporttextBox32 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.IncInspecDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.approveDatedateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.checkedDatedateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.preparedDatedateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.inspectedDatedateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.receivedDatedateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.closeLabel);
            this.panel2.Controls.Add(this.incomingInspectionFormlabel30);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1039, 57);
            this.panel2.TabIndex = 62;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // closeLabel
            // 
            this.closeLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.closeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeLabel.ForeColor = System.Drawing.Color.White;
            this.closeLabel.Location = new System.Drawing.Point(999, -1);
            this.closeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.closeLabel.Name = "closeLabel";
            this.closeLabel.Size = new System.Drawing.Size(38, 57);
            this.closeLabel.TabIndex = 1;
            this.closeLabel.Text = "X";
            this.closeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.closeLabel.Click += new System.EventHandler(this.closeLabel_Click);
            // 
            // incomingInspectionFormlabel30
            // 
            this.incomingInspectionFormlabel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.incomingInspectionFormlabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incomingInspectionFormlabel30.ForeColor = System.Drawing.Color.White;
            this.incomingInspectionFormlabel30.Location = new System.Drawing.Point(0, 0);
            this.incomingInspectionFormlabel30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.incomingInspectionFormlabel30.Name = "incomingInspectionFormlabel30";
            this.incomingInspectionFormlabel30.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.incomingInspectionFormlabel30.Size = new System.Drawing.Size(370, 55);
            this.incomingInspectionFormlabel30.TabIndex = 59;
            this.incomingInspectionFormlabel30.Text = "INCOMING INSPECTION UPDATE";
            this.incomingInspectionFormlabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(205)))), ((int)(((byte)(207)))));
            this.panel1.Controls.Add(this.saveButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.addButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 474);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1039, 66);
            this.panel1.TabIndex = 63;
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(172)))), ((int)(((byte)(60)))));
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSeaGreen;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(787, 11);
            this.saveButton.Margin = new System.Windows.Forms.Padding(2);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(104, 43);
            this.saveButton.TabIndex = 40;
            this.saveButton.Text = "&SAVE";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(902, 11);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(104, 43);
            this.cancelButton.TabIndex = 39;
            this.cancelButton.Text = "&CANCEL";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.Blue;
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(172)))), ((int)(((byte)(60)))));
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.ForeColor = System.Drawing.Color.White;
            this.addButton.Location = new System.Drawing.Point(787, 11);
            this.addButton.Margin = new System.Windows.Forms.Padding(2);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(104, 43);
            this.addButton.TabIndex = 41;
            this.addButton.Text = "&ADD";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 91);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 24);
            this.label1.TabIndex = 64;
            this.label1.Text = "ID";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 123);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 26);
            this.label2.TabIndex = 65;
            this.label2.Text = "INVOICE NO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 158);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 24);
            this.label3.TabIndex = 66;
            this.label3.Text = "APPROVED";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 192);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 24);
            this.label4.TabIndex = 67;
            this.label4.Text = "CHECKED";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 225);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 24);
            this.label5.TabIndex = 68;
            this.label5.Text = "PREPARED";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 257);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 24);
            this.label6.TabIndex = 69;
            this.label6.Text = "APPROVED DATE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 289);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 24);
            this.label7.TabIndex = 70;
            this.label7.Text = "CHECKED DATE";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(13, 324);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 24);
            this.label8.TabIndex = 71;
            this.label8.Text = "PREPARED DATE";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 357);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 24);
            this.label9.TabIndex = 72;
            this.label9.Text = "TEMPERATURE";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 391);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 24);
            this.label10.TabIndex = 73;
            this.label10.Text = "ASSEMBLY LINE";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(13, 422);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 24);
            this.label11.TabIndex = 74;
            this.label11.Text = "PART NO";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // incInspecIdTextBox
            // 
            this.incInspecIdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incInspecIdTextBox.Location = new System.Drawing.Point(171, 90);
            this.incInspecIdTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.incInspecIdTextBox.Name = "incInspecIdTextBox";
            this.incInspecIdTextBox.ReadOnly = true;
            this.incInspecIdTextBox.Size = new System.Drawing.Size(168, 26);
            this.incInspecIdTextBox.TabIndex = 76;
            // 
            // invoiceNotextBox
            // 
            this.invoiceNotextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceNotextBox.Location = new System.Drawing.Point(171, 123);
            this.invoiceNotextBox.Margin = new System.Windows.Forms.Padding(2);
            this.invoiceNotextBox.Name = "invoiceNotextBox";
            this.invoiceNotextBox.ReadOnly = true;
            this.invoiceNotextBox.Size = new System.Drawing.Size(168, 26);
            this.invoiceNotextBox.TabIndex = 77;
            // 
            // approvedtextBox
            // 
            this.approvedtextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.approvedtextBox.Location = new System.Drawing.Point(171, 158);
            this.approvedtextBox.Margin = new System.Windows.Forms.Padding(2);
            this.approvedtextBox.Name = "approvedtextBox";
            this.approvedtextBox.Size = new System.Drawing.Size(168, 26);
            this.approvedtextBox.TabIndex = 0;
            // 
            // checkedTextBox
            // 
            this.checkedTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedTextBox.Location = new System.Drawing.Point(171, 191);
            this.checkedTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.checkedTextBox.Name = "checkedTextBox";
            this.checkedTextBox.Size = new System.Drawing.Size(168, 26);
            this.checkedTextBox.TabIndex = 1;
            // 
            // preparedTextBox
            // 
            this.preparedTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preparedTextBox.Location = new System.Drawing.Point(171, 224);
            this.preparedTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.preparedTextBox.Name = "preparedTextBox";
            this.preparedTextBox.Size = new System.Drawing.Size(168, 26);
            this.preparedTextBox.TabIndex = 2;
            // 
            // temperatureTextBox
            // 
            this.temperatureTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureTextBox.Location = new System.Drawing.Point(171, 356);
            this.temperatureTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.temperatureTextBox.Name = "temperatureTextBox";
            this.temperatureTextBox.Size = new System.Drawing.Size(168, 26);
            this.temperatureTextBox.TabIndex = 6;
            this.temperatureTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.temperatureTextBox_KeyPress);
            // 
            // assemblyLineTextBox
            // 
            this.assemblyLineTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assemblyLineTextBox.Location = new System.Drawing.Point(171, 390);
            this.assemblyLineTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.assemblyLineTextBox.Name = "assemblyLineTextBox";
            this.assemblyLineTextBox.Size = new System.Drawing.Size(168, 26);
            this.assemblyLineTextBox.TabIndex = 7;
            // 
            // partNoTextBox
            // 
            this.partNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partNoTextBox.Location = new System.Drawing.Point(171, 422);
            this.partNoTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.partNoTextBox.Name = "partNoTextBox";
            this.partNoTextBox.Size = new System.Drawing.Size(168, 26);
            this.partNoTextBox.TabIndex = 8;
            // 
            // oirTextBox11
            // 
            this.oirTextBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oirTextBox11.Location = new System.Drawing.Point(525, 422);
            this.oirTextBox11.Margin = new System.Windows.Forms.Padding(2);
            this.oirTextBox11.Name = "oirTextBox11";
            this.oirTextBox11.Size = new System.Drawing.Size(168, 26);
            this.oirTextBox11.TabIndex = 19;
            // 
            // inspectionTypetextBox12
            // 
            this.inspectionTypetextBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inspectionTypetextBox12.Location = new System.Drawing.Point(525, 390);
            this.inspectionTypetextBox12.Margin = new System.Windows.Forms.Padding(2);
            this.inspectionTypetextBox12.Name = "inspectionTypetextBox12";
            this.inspectionTypetextBox12.Size = new System.Drawing.Size(168, 26);
            this.inspectionTypetextBox12.TabIndex = 18;
            // 
            // productionTypetextBox13
            // 
            this.productionTypetextBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productionTypetextBox13.Location = new System.Drawing.Point(525, 356);
            this.productionTypetextBox13.Margin = new System.Windows.Forms.Padding(2);
            this.productionTypetextBox13.Name = "productionTypetextBox13";
            this.productionTypetextBox13.Size = new System.Drawing.Size(168, 26);
            this.productionTypetextBox13.TabIndex = 17;
            // 
            // materialTypetextBox14
            // 
            this.materialTypetextBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialTypetextBox14.Location = new System.Drawing.Point(525, 323);
            this.materialTypetextBox14.Margin = new System.Windows.Forms.Padding(2);
            this.materialTypetextBox14.Name = "materialTypetextBox14";
            this.materialTypetextBox14.Size = new System.Drawing.Size(168, 26);
            this.materialTypetextBox14.TabIndex = 16;
            // 
            // inspectortextBox15
            // 
            this.inspectortextBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inspectortextBox15.Location = new System.Drawing.Point(525, 288);
            this.inspectortextBox15.Margin = new System.Windows.Forms.Padding(2);
            this.inspectortextBox15.Name = "inspectortextBox15";
            this.inspectortextBox15.Size = new System.Drawing.Size(168, 26);
            this.inspectortextBox15.TabIndex = 15;
            // 
            // makertextBox16
            // 
            this.makertextBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.makertextBox16.Location = new System.Drawing.Point(525, 257);
            this.makertextBox16.Margin = new System.Windows.Forms.Padding(2);
            this.makertextBox16.Name = "makertextBox16";
            this.makertextBox16.Size = new System.Drawing.Size(168, 26);
            this.makertextBox16.TabIndex = 14;
            // 
            // suppliertextBox17
            // 
            this.suppliertextBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.suppliertextBox17.Location = new System.Drawing.Point(525, 224);
            this.suppliertextBox17.Margin = new System.Windows.Forms.Padding(2);
            this.suppliertextBox17.Name = "suppliertextBox17";
            this.suppliertextBox17.Size = new System.Drawing.Size(168, 26);
            this.suppliertextBox17.TabIndex = 13;
            // 
            // humidityTextBox
            // 
            this.humidityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityTextBox.Location = new System.Drawing.Point(525, 123);
            this.humidityTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.humidityTextBox.Name = "humidityTextBox";
            this.humidityTextBox.Size = new System.Drawing.Size(168, 26);
            this.humidityTextBox.TabIndex = 10;
            this.humidityTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.humidityTextBox_KeyPress);
            // 
            // rohsComplianceTextBox
            // 
            this.rohsComplianceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rohsComplianceTextBox.Location = new System.Drawing.Point(525, 90);
            this.rohsComplianceTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.rohsComplianceTextBox.Name = "rohsComplianceTextBox";
            this.rohsComplianceTextBox.Size = new System.Drawing.Size(168, 26);
            this.rohsComplianceTextBox.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(355, 422);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 24);
            this.label12.TabIndex = 97;
            this.label12.Text = "OIR";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(355, 391);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(166, 24);
            this.label13.TabIndex = 96;
            this.label13.Text = "INSPECTION TYPE";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(355, 357);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(166, 24);
            this.label14.TabIndex = 95;
            this.label14.Text = "PRODUCTION TYPE";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(355, 324);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(166, 24);
            this.label15.TabIndex = 94;
            this.label15.Text = "MATERIAL TYPE";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(355, 289);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(166, 24);
            this.label16.TabIndex = 93;
            this.label16.Text = "INSPECTOR";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(355, 257);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(166, 24);
            this.label17.TabIndex = 92;
            this.label17.Text = "MAKER";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(355, 225);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(166, 24);
            this.label18.TabIndex = 91;
            this.label18.Text = "SUPPLIER";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(355, 192);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(166, 24);
            this.label19.TabIndex = 90;
            this.label19.Text = "RECEIVED DATE";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(355, 158);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(166, 24);
            this.label20.TabIndex = 89;
            this.label20.Text = "INSPECTED DATE";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(355, 123);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(166, 26);
            this.label21.TabIndex = 88;
            this.label21.Text = "HUMIDITY";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(355, 91);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(166, 24);
            this.label22.TabIndex = 87;
            this.label22.Text = "ROHS COMPLIANCE";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialBoxTextBox25
            // 
            this.materialBoxTextBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialBoxTextBox25.Location = new System.Drawing.Point(843, 320);
            this.materialBoxTextBox25.Margin = new System.Windows.Forms.Padding(2);
            this.materialBoxTextBox25.Name = "materialBoxTextBox25";
            this.materialBoxTextBox25.ReadOnly = true;
            this.materialBoxTextBox25.Size = new System.Drawing.Size(168, 26);
            this.materialBoxTextBox25.TabIndex = 127;
            // 
            // goodsCodetextBox26
            // 
            this.goodsCodetextBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goodsCodetextBox26.Location = new System.Drawing.Point(843, 288);
            this.goodsCodetextBox26.Margin = new System.Windows.Forms.Padding(2);
            this.goodsCodetextBox26.Name = "goodsCodetextBox26";
            this.goodsCodetextBox26.ReadOnly = true;
            this.goodsCodetextBox26.Size = new System.Drawing.Size(168, 26);
            this.goodsCodetextBox26.TabIndex = 26;
            this.goodsCodetextBox26.TabStop = false;
            // 
            // invoiceQTYtextBox27
            // 
            this.invoiceQTYtextBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceQTYtextBox27.Location = new System.Drawing.Point(843, 257);
            this.invoiceQTYtextBox27.Margin = new System.Windows.Forms.Padding(2);
            this.invoiceQTYtextBox27.Name = "invoiceQTYtextBox27";
            this.invoiceQTYtextBox27.Size = new System.Drawing.Size(168, 26);
            this.invoiceQTYtextBox27.TabIndex = 25;
            this.invoiceQTYtextBox27.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.invoiceQTYtextBox27_KeyPress);
            // 
            // partNametextBox28
            // 
            this.partNametextBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partNametextBox28.Location = new System.Drawing.Point(843, 224);
            this.partNametextBox28.Margin = new System.Windows.Forms.Padding(2);
            this.partNametextBox28.Name = "partNametextBox28";
            this.partNametextBox28.Size = new System.Drawing.Size(168, 26);
            this.partNametextBox28.TabIndex = 24;
            // 
            // coctextBox29
            // 
            this.coctextBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coctextBox29.Location = new System.Drawing.Point(843, 191);
            this.coctextBox29.Margin = new System.Windows.Forms.Padding(2);
            this.coctextBox29.Name = "coctextBox29";
            this.coctextBox29.Size = new System.Drawing.Size(168, 26);
            this.coctextBox29.TabIndex = 23;
            // 
            // ulMarkingtextBox30
            // 
            this.ulMarkingtextBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ulMarkingtextBox30.Location = new System.Drawing.Point(843, 158);
            this.ulMarkingtextBox30.Margin = new System.Windows.Forms.Padding(2);
            this.ulMarkingtextBox30.Name = "ulMarkingtextBox30";
            this.ulMarkingtextBox30.Size = new System.Drawing.Size(168, 26);
            this.ulMarkingtextBox30.TabIndex = 22;
            // 
            // sampleSizetextBox31
            // 
            this.sampleSizetextBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sampleSizetextBox31.Location = new System.Drawing.Point(843, 123);
            this.sampleSizetextBox31.Margin = new System.Windows.Forms.Padding(2);
            this.sampleSizetextBox31.Name = "sampleSizetextBox31";
            this.sampleSizetextBox31.Size = new System.Drawing.Size(168, 26);
            this.sampleSizetextBox31.TabIndex = 21;
            this.sampleSizetextBox31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sampleSizetextBox31_KeyPress);
            // 
            // testReporttextBox32
            // 
            this.testReporttextBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testReporttextBox32.Location = new System.Drawing.Point(843, 90);
            this.testReporttextBox32.Margin = new System.Windows.Forms.Padding(2);
            this.testReporttextBox32.Name = "testReporttextBox32";
            this.testReporttextBox32.Size = new System.Drawing.Size(168, 26);
            this.testReporttextBox32.TabIndex = 20;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(699, 353);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(122, 24);
            this.label25.TabIndex = 117;
            this.label25.Text = "DATE";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(699, 321);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 24);
            this.label26.TabIndex = 116;
            this.label26.Text = "MATERIAL BOX";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(699, 289);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 24);
            this.label27.TabIndex = 115;
            this.label27.Text = "GOODS CODE";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(699, 257);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(140, 24);
            this.label28.TabIndex = 114;
            this.label28.Text = "INVOICE QTY";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(699, 225);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(140, 24);
            this.label29.TabIndex = 113;
            this.label29.Text = "PART NAME";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(699, 192);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(140, 24);
            this.label31.TabIndex = 112;
            this.label31.Text = "COC";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(699, 158);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 24);
            this.label32.TabIndex = 111;
            this.label32.Text = "UL MARKING";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(699, 123);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(140, 26);
            this.label33.TabIndex = 110;
            this.label33.Text = "SAMPLE SIZE";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(699, 91);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(140, 24);
            this.label34.TabIndex = 109;
            this.label34.Text = "TEST REPORT";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // IncInspecDateTimePicker
            // 
            this.IncInspecDateTimePicker.CustomFormat = "yyyy-MM-dd hh:mm:ss";
            this.IncInspecDateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IncInspecDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.IncInspecDateTimePicker.Location = new System.Drawing.Point(825, 352);
            this.IncInspecDateTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.IncInspecDateTimePicker.Name = "IncInspecDateTimePicker";
            this.IncInspecDateTimePicker.Size = new System.Drawing.Size(186, 26);
            this.IncInspecDateTimePicker.TabIndex = 27;
            // 
            // approveDatedateTimePicker1
            // 
            this.approveDatedateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.approveDatedateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.approveDatedateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.approveDatedateTimePicker1.Location = new System.Drawing.Point(171, 257);
            this.approveDatedateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.approveDatedateTimePicker1.Name = "approveDatedateTimePicker1";
            this.approveDatedateTimePicker1.Size = new System.Drawing.Size(168, 26);
            this.approveDatedateTimePicker1.TabIndex = 3;
            // 
            // checkedDatedateTimePicker2
            // 
            this.checkedDatedateTimePicker2.CustomFormat = "yyyy-MM-dd";
            this.checkedDatedateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedDatedateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.checkedDatedateTimePicker2.Location = new System.Drawing.Point(171, 288);
            this.checkedDatedateTimePicker2.Margin = new System.Windows.Forms.Padding(2);
            this.checkedDatedateTimePicker2.Name = "checkedDatedateTimePicker2";
            this.checkedDatedateTimePicker2.Size = new System.Drawing.Size(168, 26);
            this.checkedDatedateTimePicker2.TabIndex = 4;
            // 
            // preparedDatedateTimePicker3
            // 
            this.preparedDatedateTimePicker3.CustomFormat = "yyyy-MM-dd";
            this.preparedDatedateTimePicker3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preparedDatedateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.preparedDatedateTimePicker3.Location = new System.Drawing.Point(171, 322);
            this.preparedDatedateTimePicker3.Margin = new System.Windows.Forms.Padding(2);
            this.preparedDatedateTimePicker3.Name = "preparedDatedateTimePicker3";
            this.preparedDatedateTimePicker3.Size = new System.Drawing.Size(168, 26);
            this.preparedDatedateTimePicker3.TabIndex = 5;
            // 
            // inspectedDatedateTimePicker4
            // 
            this.inspectedDatedateTimePicker4.CustomFormat = "yyyy-MM-dd";
            this.inspectedDatedateTimePicker4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inspectedDatedateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.inspectedDatedateTimePicker4.Location = new System.Drawing.Point(525, 156);
            this.inspectedDatedateTimePicker4.Margin = new System.Windows.Forms.Padding(2);
            this.inspectedDatedateTimePicker4.Name = "inspectedDatedateTimePicker4";
            this.inspectedDatedateTimePicker4.Size = new System.Drawing.Size(168, 26);
            this.inspectedDatedateTimePicker4.TabIndex = 11;
            // 
            // receivedDatedateTimePicker5
            // 
            this.receivedDatedateTimePicker5.CustomFormat = "yyyy-MM-dd";
            this.receivedDatedateTimePicker5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivedDatedateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.receivedDatedateTimePicker5.Location = new System.Drawing.Point(525, 189);
            this.receivedDatedateTimePicker5.Margin = new System.Windows.Forms.Padding(2);
            this.receivedDatedateTimePicker5.Name = "receivedDatedateTimePicker5";
            this.receivedDatedateTimePicker5.Size = new System.Drawing.Size(168, 26);
            this.receivedDatedateTimePicker5.TabIndex = 12;
            // 
            // UpdateIncomingInspectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(1039, 540);
            this.Controls.Add(this.receivedDatedateTimePicker5);
            this.Controls.Add(this.inspectedDatedateTimePicker4);
            this.Controls.Add(this.preparedDatedateTimePicker3);
            this.Controls.Add(this.checkedDatedateTimePicker2);
            this.Controls.Add(this.approveDatedateTimePicker1);
            this.Controls.Add(this.IncInspecDateTimePicker);
            this.Controls.Add(this.materialBoxTextBox25);
            this.Controls.Add(this.goodsCodetextBox26);
            this.Controls.Add(this.invoiceQTYtextBox27);
            this.Controls.Add(this.partNametextBox28);
            this.Controls.Add(this.coctextBox29);
            this.Controls.Add(this.ulMarkingtextBox30);
            this.Controls.Add(this.sampleSizetextBox31);
            this.Controls.Add(this.testReporttextBox32);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.oirTextBox11);
            this.Controls.Add(this.inspectionTypetextBox12);
            this.Controls.Add(this.productionTypetextBox13);
            this.Controls.Add(this.materialTypetextBox14);
            this.Controls.Add(this.inspectortextBox15);
            this.Controls.Add(this.makertextBox16);
            this.Controls.Add(this.suppliertextBox17);
            this.Controls.Add(this.humidityTextBox);
            this.Controls.Add(this.rohsComplianceTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.partNoTextBox);
            this.Controls.Add(this.assemblyLineTextBox);
            this.Controls.Add(this.temperatureTextBox);
            this.Controls.Add(this.preparedTextBox);
            this.Controls.Add(this.checkedTextBox);
            this.Controls.Add(this.approvedtextBox);
            this.Controls.Add(this.invoiceNotextBox);
            this.Controls.Add(this.incInspecIdTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UpdateIncomingInspectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update";
            this.Load += new System.EventHandler(this.UpdateIncomingInspectionForm_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label closeLabel;
        private System.Windows.Forms.Label incomingInspectionFormlabel30;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox incInspecIdTextBox;
        private System.Windows.Forms.TextBox invoiceNotextBox;
        private System.Windows.Forms.TextBox approvedtextBox;
        private System.Windows.Forms.TextBox checkedTextBox;
        private System.Windows.Forms.TextBox preparedTextBox;
        private System.Windows.Forms.TextBox temperatureTextBox;
        private System.Windows.Forms.TextBox assemblyLineTextBox;
        private System.Windows.Forms.TextBox partNoTextBox;
        private System.Windows.Forms.TextBox oirTextBox11;
        private System.Windows.Forms.TextBox inspectionTypetextBox12;
        private System.Windows.Forms.TextBox productionTypetextBox13;
        private System.Windows.Forms.TextBox materialTypetextBox14;
        private System.Windows.Forms.TextBox inspectortextBox15;
        private System.Windows.Forms.TextBox makertextBox16;
        private System.Windows.Forms.TextBox suppliertextBox17;
        private System.Windows.Forms.TextBox humidityTextBox;
        private System.Windows.Forms.TextBox rohsComplianceTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox materialBoxTextBox25;
        private System.Windows.Forms.TextBox goodsCodetextBox26;
        private System.Windows.Forms.TextBox invoiceQTYtextBox27;
        private System.Windows.Forms.TextBox partNametextBox28;
        private System.Windows.Forms.TextBox coctextBox29;
        private System.Windows.Forms.TextBox ulMarkingtextBox30;
        private System.Windows.Forms.TextBox sampleSizetextBox31;
        private System.Windows.Forms.TextBox testReporttextBox32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DateTimePicker IncInspecDateTimePicker;
        private System.Windows.Forms.DateTimePicker approveDatedateTimePicker1;
        private System.Windows.Forms.DateTimePicker checkedDatedateTimePicker2;
        private System.Windows.Forms.DateTimePicker preparedDatedateTimePicker3;
        private System.Windows.Forms.DateTimePicker inspectedDatedateTimePicker4;
        private System.Windows.Forms.DateTimePicker receivedDatedateTimePicker5;
        private System.Windows.Forms.Button addButton;
    }
}