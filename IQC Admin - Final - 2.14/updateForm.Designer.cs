﻿
namespace IQCAdmin
{
    partial class lotInformationUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeLabel = new System.Windows.Forms.Label();
            this.lotInfoIdTextBox = new System.Windows.Forms.TextBox();
            this.invoiceNoTextBox = new System.Windows.Forms.TextBox();
            this.partNoTextBox = new System.Windows.Forms.TextBox();
            this.partNameTextBox = new System.Windows.Forms.TextBox();
            this.totalQTYTextBox = new System.Windows.Forms.TextBox();
            this.quantityReceivedTextBox = new System.Windows.Forms.TextBox();
            this.lotNoTextBox = new System.Windows.Forms.TextBox();
            this.lotQuantityTextBox = new System.Windows.Forms.TextBox();
            this.boxNoTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.totalGoodTextBox = new System.Windows.Forms.TextBox();
            this.diffTextBox = new System.Windows.Forms.TextBox();
            this.kenIDTextBox = new System.Windows.Forms.TextBox();
            this.remarksTextBox = new System.Windows.Forms.TextBox();
            this.materialBoxNoTextBox = new System.Windows.Forms.TextBox();
            this.goodsCodeTextBox = new System.Windows.Forms.TextBox();
            this.sampleSizeTextBox = new System.Windows.Forms.TextBox();
            this.rejectTextBox = new System.Windows.Forms.TextBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lotInfoFormLabel30 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.addButton = new System.Windows.Forms.Button();
            this.lotNumberDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // closeLabel
            // 
            this.closeLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.closeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeLabel.ForeColor = System.Drawing.Color.White;
            this.closeLabel.Location = new System.Drawing.Point(859, 0);
            this.closeLabel.Name = "closeLabel";
            this.closeLabel.Size = new System.Drawing.Size(40, 57);
            this.closeLabel.TabIndex = 1;
            this.closeLabel.Text = "X";
            this.closeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.closeLabel.Click += new System.EventHandler(this.closeLabel_Click);
            // 
            // lotInfoIdTextBox
            // 
            this.lotInfoIdTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotInfoIdTextBox.Location = new System.Drawing.Point(208, 81);
            this.lotInfoIdTextBox.Name = "lotInfoIdTextBox";
            this.lotInfoIdTextBox.ReadOnly = true;
            this.lotInfoIdTextBox.Size = new System.Drawing.Size(223, 26);
            this.lotInfoIdTextBox.TabIndex = 2;
            this.lotInfoIdTextBox.TabStop = false;
            // 
            // invoiceNoTextBox
            // 
            this.invoiceNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceNoTextBox.Location = new System.Drawing.Point(208, 119);
            this.invoiceNoTextBox.Name = "invoiceNoTextBox";
            this.invoiceNoTextBox.ReadOnly = true;
            this.invoiceNoTextBox.Size = new System.Drawing.Size(223, 26);
            this.invoiceNoTextBox.TabIndex = 3;
            this.invoiceNoTextBox.TabStop = false;
            // 
            // partNoTextBox
            // 
            this.partNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partNoTextBox.Location = new System.Drawing.Point(208, 157);
            this.partNoTextBox.Name = "partNoTextBox";
            this.partNoTextBox.Size = new System.Drawing.Size(223, 26);
            this.partNoTextBox.TabIndex = 4;
            // 
            // partNameTextBox
            // 
            this.partNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partNameTextBox.Location = new System.Drawing.Point(208, 195);
            this.partNameTextBox.Name = "partNameTextBox";
            this.partNameTextBox.Size = new System.Drawing.Size(223, 26);
            this.partNameTextBox.TabIndex = 5;
            // 
            // totalQTYTextBox
            // 
            this.totalQTYTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalQTYTextBox.Location = new System.Drawing.Point(208, 233);
            this.totalQTYTextBox.Name = "totalQTYTextBox";
            this.totalQTYTextBox.Size = new System.Drawing.Size(223, 26);
            this.totalQTYTextBox.TabIndex = 6;
            this.totalQTYTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.totalQTYTextBox_KeyPress);
            // 
            // quantityReceivedTextBox
            // 
            this.quantityReceivedTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quantityReceivedTextBox.Location = new System.Drawing.Point(208, 271);
            this.quantityReceivedTextBox.Name = "quantityReceivedTextBox";
            this.quantityReceivedTextBox.Size = new System.Drawing.Size(223, 26);
            this.quantityReceivedTextBox.TabIndex = 7;
            this.quantityReceivedTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantityReceivedTextBox_KeyPress);
            // 
            // lotNoTextBox
            // 
            this.lotNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotNoTextBox.Location = new System.Drawing.Point(208, 309);
            this.lotNoTextBox.Name = "lotNoTextBox";
            this.lotNoTextBox.Size = new System.Drawing.Size(223, 26);
            this.lotNoTextBox.TabIndex = 8;
            // 
            // lotQuantityTextBox
            // 
            this.lotQuantityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotQuantityTextBox.Location = new System.Drawing.Point(208, 347);
            this.lotQuantityTextBox.Name = "lotQuantityTextBox";
            this.lotQuantityTextBox.Size = new System.Drawing.Size(223, 26);
            this.lotQuantityTextBox.TabIndex = 9;
            this.lotQuantityTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lotQuantityTextBox_KeyPress);
            // 
            // boxNoTextBox
            // 
            this.boxNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxNoTextBox.Location = new System.Drawing.Point(208, 382);
            this.boxNoTextBox.Name = "boxNoTextBox";
            this.boxNoTextBox.Size = new System.Drawing.Size(223, 26);
            this.boxNoTextBox.TabIndex = 10;
            this.boxNoTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.boxNoTextBox_KeyPress);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "ID";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "INVOICE NO";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "PART NO";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "PART NAME";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(180, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "TOTAL QUANTITY";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 312);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "LOT NO";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(22, 350);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(180, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "LOT QUANTITY";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 385);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(180, 20);
            this.label10.TabIndex = 19;
            this.label10.Text = "BOX NUMBER";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // totalGoodTextBox
            // 
            this.totalGoodTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalGoodTextBox.Location = new System.Drawing.Point(632, 378);
            this.totalGoodTextBox.Name = "totalGoodTextBox";
            this.totalGoodTextBox.Size = new System.Drawing.Size(223, 26);
            this.totalGoodTextBox.TabIndex = 28;
            this.totalGoodTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.totalGoodTextBox_KeyPress);
            // 
            // diffTextBox
            // 
            this.diffTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diffTextBox.Location = new System.Drawing.Point(632, 343);
            this.diffTextBox.Name = "diffTextBox";
            this.diffTextBox.Size = new System.Drawing.Size(223, 26);
            this.diffTextBox.TabIndex = 27;
            this.diffTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.diffTextBox_KeyPress);
            // 
            // kenIDTextBox
            // 
            this.kenIDTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kenIDTextBox.Location = new System.Drawing.Point(632, 305);
            this.kenIDTextBox.Name = "kenIDTextBox";
            this.kenIDTextBox.Size = new System.Drawing.Size(223, 26);
            this.kenIDTextBox.TabIndex = 26;
            this.kenIDTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.kenIDTextBox_KeyPress);
            // 
            // remarksTextBox
            // 
            this.remarksTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarksTextBox.Location = new System.Drawing.Point(632, 229);
            this.remarksTextBox.Name = "remarksTextBox";
            this.remarksTextBox.Size = new System.Drawing.Size(223, 26);
            this.remarksTextBox.TabIndex = 24;
            // 
            // materialBoxNoTextBox
            // 
            this.materialBoxNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialBoxNoTextBox.Location = new System.Drawing.Point(632, 191);
            this.materialBoxNoTextBox.Name = "materialBoxNoTextBox";
            this.materialBoxNoTextBox.ReadOnly = true;
            this.materialBoxNoTextBox.Size = new System.Drawing.Size(223, 26);
            this.materialBoxNoTextBox.TabIndex = 23;
            this.materialBoxNoTextBox.TabStop = false;
            // 
            // goodsCodeTextBox
            // 
            this.goodsCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goodsCodeTextBox.Location = new System.Drawing.Point(632, 153);
            this.goodsCodeTextBox.Name = "goodsCodeTextBox";
            this.goodsCodeTextBox.ReadOnly = true;
            this.goodsCodeTextBox.Size = new System.Drawing.Size(223, 26);
            this.goodsCodeTextBox.TabIndex = 22;
            this.goodsCodeTextBox.TabStop = false;
            // 
            // sampleSizeTextBox
            // 
            this.sampleSizeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sampleSizeTextBox.Location = new System.Drawing.Point(632, 115);
            this.sampleSizeTextBox.Name = "sampleSizeTextBox";
            this.sampleSizeTextBox.Size = new System.Drawing.Size(223, 26);
            this.sampleSizeTextBox.TabIndex = 21;
            this.sampleSizeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sampleSizeTextBox_KeyPress);
            // 
            // rejectTextBox
            // 
            this.rejectTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rejectTextBox.Location = new System.Drawing.Point(632, 77);
            this.rejectTextBox.Name = "rejectTextBox";
            this.rejectTextBox.Size = new System.Drawing.Size(223, 26);
            this.rejectTextBox.TabIndex = 20;
            this.rejectTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rejectTextBox_KeyPress);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(747, 9);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(106, 40);
            this.cancelButton.TabIndex = 39;
            this.cancelButton.Text = "&CANCEL";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(172)))), ((int)(((byte)(60)))));
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSeaGreen;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(630, 9);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(106, 40);
            this.saveButton.TabIndex = 40;
            this.saveButton.Text = "&SAVE";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(432, 381);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 0);
            this.label21.TabIndex = 49;
            this.label21.Text = "BOX NUMBER";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(432, 346);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(0, 0);
            this.label22.TabIndex = 48;
            this.label22.Text = "LOT QUANTITY";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(432, 308);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 0);
            this.label23.TabIndex = 47;
            this.label23.Text = "LOT NO";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(432, 270);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(0, 0);
            this.label24.TabIndex = 46;
            this.label24.Text = "QUANTITY RECEIVED";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(432, 232);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(0, 0);
            this.label25.TabIndex = 45;
            this.label25.Text = "TOTAL QUANTITY";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(432, 194);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(0, 0);
            this.label26.TabIndex = 44;
            this.label26.Text = "PART NAME";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(432, 156);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 0);
            this.label27.TabIndex = 43;
            this.label27.Text = "PART NO";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(432, 118);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(0, 0);
            this.label28.TabIndex = 42;
            this.label28.Text = "INVOICE NO";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(439, 61);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 0);
            this.label29.TabIndex = 41;
            this.label29.Text = "ID";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(446, 381);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(180, 20);
            this.label11.TabIndex = 58;
            this.label11.Text = "TOTAL GOOD";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(446, 346);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(180, 20);
            this.label12.TabIndex = 57;
            this.label12.Text = "DIFF";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(446, 308);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(180, 20);
            this.label13.TabIndex = 56;
            this.label13.Text = "KEN ID";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(446, 270);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(180, 20);
            this.label14.TabIndex = 55;
            this.label14.Text = "DATE";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(446, 232);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(180, 20);
            this.label15.TabIndex = 54;
            this.label15.Text = "REMARKS";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(446, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(180, 20);
            this.label16.TabIndex = 53;
            this.label16.Text = "MATERIAL BOX NO";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(446, 156);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(180, 20);
            this.label17.TabIndex = 52;
            this.label17.Text = "GOODS CODE";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(446, 118);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(180, 20);
            this.label18.TabIndex = 51;
            this.label18.Text = "SAMPLE SIZE";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(444, 82);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(180, 20);
            this.label19.TabIndex = 50;
            this.label19.Text = "REJECT";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lotInfoFormLabel30
            // 
            this.lotInfoFormLabel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.lotInfoFormLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotInfoFormLabel30.ForeColor = System.Drawing.Color.White;
            this.lotInfoFormLabel30.Location = new System.Drawing.Point(0, 0);
            this.lotInfoFormLabel30.Name = "lotInfoFormLabel30";
            this.lotInfoFormLabel30.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.lotInfoFormLabel30.Size = new System.Drawing.Size(291, 57);
            this.lotInfoFormLabel30.TabIndex = 59;
            this.lotInfoFormLabel30.Text = "LOT INFORMATION UPDATE";
            this.lotInfoFormLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(199, 23);
            this.label7.TabIndex = 60;
            this.label7.Text = "QUANTITY RECEIVED";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.closeLabel);
            this.panel1.Controls.Add(this.lotInfoFormLabel30);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(899, 57);
            this.panel1.TabIndex = 61;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(205)))), ((int)(((byte)(207)))));
            this.panel2.Controls.Add(this.saveButton);
            this.panel2.Controls.Add(this.cancelButton);
            this.panel2.Controls.Add(this.addButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 429);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(899, 57);
            this.panel2.TabIndex = 62;
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(172)))), ((int)(((byte)(60)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.ForeColor = System.Drawing.Color.White;
            this.addButton.Location = new System.Drawing.Point(630, 9);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(106, 40);
            this.addButton.TabIndex = 41;
            this.addButton.Text = "&ADD";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Visible = false;
            // 
            // lotNumberDateTimePicker
            // 
            this.lotNumberDateTimePicker.CustomFormat = "yyyy-MM-dd hh:mm:ss";
            this.lotNumberDateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotNumberDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.lotNumberDateTimePicker.Location = new System.Drawing.Point(632, 267);
            this.lotNumberDateTimePicker.Name = "lotNumberDateTimePicker";
            this.lotNumberDateTimePicker.Size = new System.Drawing.Size(223, 26);
            this.lotNumberDateTimePicker.TabIndex = 25;
            // 
            // lotInformationUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(899, 486);
            this.ControlBox = false;
            this.Controls.Add(this.lotNumberDateTimePicker);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.totalGoodTextBox);
            this.Controls.Add(this.diffTextBox);
            this.Controls.Add(this.kenIDTextBox);
            this.Controls.Add(this.remarksTextBox);
            this.Controls.Add(this.materialBoxNoTextBox);
            this.Controls.Add(this.goodsCodeTextBox);
            this.Controls.Add(this.sampleSizeTextBox);
            this.Controls.Add(this.rejectTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.boxNoTextBox);
            this.Controls.Add(this.lotQuantityTextBox);
            this.Controls.Add(this.lotNoTextBox);
            this.Controls.Add(this.quantityReceivedTextBox);
            this.Controls.Add(this.totalQTYTextBox);
            this.Controls.Add(this.partNameTextBox);
            this.Controls.Add(this.partNoTextBox);
            this.Controls.Add(this.invoiceNoTextBox);
            this.Controls.Add(this.lotInfoIdTextBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "lotInformationUpdateForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UPDATE FORM";
            this.Load += new System.EventHandler(this.updateForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label closeLabel;
        private System.Windows.Forms.TextBox lotInfoIdTextBox;
        private System.Windows.Forms.TextBox invoiceNoTextBox;
        private System.Windows.Forms.TextBox partNoTextBox;
        private System.Windows.Forms.TextBox partNameTextBox;
        private System.Windows.Forms.TextBox totalQTYTextBox;
        private System.Windows.Forms.TextBox quantityReceivedTextBox;
        private System.Windows.Forms.TextBox lotNoTextBox;
        private System.Windows.Forms.TextBox lotQuantityTextBox;
        private System.Windows.Forms.TextBox boxNoTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox totalGoodTextBox;
        private System.Windows.Forms.TextBox diffTextBox;
        private System.Windows.Forms.TextBox kenIDTextBox;
        private System.Windows.Forms.TextBox remarksTextBox;
        private System.Windows.Forms.TextBox materialBoxNoTextBox;
        private System.Windows.Forms.TextBox goodsCodeTextBox;
        private System.Windows.Forms.TextBox sampleSizeTextBox;
        private System.Windows.Forms.TextBox rejectTextBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lotInfoFormLabel30;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker lotNumberDateTimePicker;
        private System.Windows.Forms.Button addButton;
    }
}