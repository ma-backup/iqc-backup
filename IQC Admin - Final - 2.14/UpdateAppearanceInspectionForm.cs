﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class UpdateAppearanceInspectionForm : Form
    {
        public string id, invoice_no, ig_checkpoints, instrument_used, result, judgement, 
            remarks, goodsCode, defectQTY, defect_enc, MaterialCodeBoxSeqID;

        public string appearanceInspectionFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;

        List<string> appearanceInspectionFields = new List<string>(); //List of the fields
        List<string> appearanceInspectionValues = new List<string>(); //List of textboxes values before clicking save
        List<string> appearanceInspectionNewValues = new List<string>(); //List of textboxes values after clicking save

        private string action; //Any changes of the values will be added in this string

        private void defectQtyTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialCodeBoxTextBox.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                { 
                    query = "UPDATE Appearance_Inspection " + 
                        "SET ig_checkpoints = '" + igCheckPointsTextBox.Text +
                        "', instrument_used = '" + instrumentUsedTextBox.Text +
                        "', result = '" + resultTextBox.Text +
                        "', judgement = '" + judgementTextBox.Text +
                        "', remarks = '" + remarksTextBox.Text +
                        "', goodsCode = '" + goodsCodeTextBox.Text +
                        "', defectqty = '" + defectQtyTextBox.Text +
                        "', defect_enc = '" + defectEncTextBox.Text +
                        "' WHERE id = '" + idTextBox1.Text + "'";
                    SqlCommand command = new SqlCommand(query, DBHelper.connection);
                    command.ExecuteNonQuery();
                    frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                    obj.comboBox2_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    try
                    {
                        //Add values of the textboxes to the list of new values after clicking the save button                     
                        appearanceInspectionNewValues.Clear();
                        appearanceInspectionNewValues.Add(igCheckPointsTextBox.Text);
                        appearanceInspectionNewValues.Add(instrumentUsedTextBox.Text);
                        appearanceInspectionNewValues.Add(resultTextBox.Text);
                        appearanceInspectionNewValues.Add(judgementTextBox.Text);
                        appearanceInspectionNewValues.Add(remarksTextBox.Text);
                        appearanceInspectionNewValues.Add(defectQtyTextBox.Text);
                        appearanceInspectionNewValues.Add(defectEncTextBox.Text);

                        var defaultAction = "EDITED APPEARANCE INSPECTION \x0AID :  " + idTextBox1.Text + "\x0A" +
                            "INVOICE NO :  " + invoiceTextBox2.Text + "\x0APART NO :  " + materialCodeBoxTextBox.Text + "\x0A";
                        action = defaultAction;

                        //loop through the Lists and check if there are unequal values from the appearanceInspectionValues and appearanceInspectionNewValues
                        //if there are unequal values, add it to the action string
                        for (int i = 0; i < appearanceInspectionFields.Count(); i++)
                        {
                            if(appearanceInspectionValues[i] != appearanceInspectionNewValues[i])
                            {
                                action += "\x0A" + appearanceInspectionFields[i] +
                                              "   from   ' " + appearanceInspectionValues[i] +
                                              " '   to   ' " + appearanceInspectionNewValues[i] + " '.";
                            }
                        }
                        //Check DBHelper.cs
                        DBHelper.saveActivityToDatabase(action, defaultAction);
                    }
                    catch (Exception)
                    {
                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateAppearanceInspectionForm_Load(object sender, EventArgs e)
        {
            if(appearanceInspectionFormLabel == "update")
            {
                appearanceInspectionlabel30.Text = "APPEARANCE INSPECTION UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                idTextBox1.Text = id;
                invoiceTextBox2.Text = invoice_no;
                igCheckPointsTextBox.Text = ig_checkpoints;
                instrumentUsedTextBox.Text = instrument_used;
                resultTextBox.Text = result;
                judgementTextBox.Text = judgement;
                remarksTextBox.Text = remarks;
                goodsCodeTextBox.Text = goodsCode;
                defectQtyTextBox.Text = defectQTY;
                defectEncTextBox.Text = defect_enc;
                materialCodeBoxTextBox.Text = MaterialCodeBoxSeqID;

                //Add the fields to list of fields
                appearanceInspectionFields.Clear();
                appearanceInspectionFields.Add("IG Checkpoints");
                appearanceInspectionFields.Add("Instrument");
                appearanceInspectionFields.Add("Result");
                appearanceInspectionFields.Add("Judgement");
                appearanceInspectionFields.Add("Remarks");
                appearanceInspectionFields.Add("Defect Quantity");
                appearanceInspectionFields.Add("Defect Encountered");

                //Add values to the list before any edit
                appearanceInspectionValues.Clear();
                appearanceInspectionValues.Add(igCheckPointsTextBox.Text);
                appearanceInspectionValues.Add(instrumentUsedTextBox.Text);
                appearanceInspectionValues.Add(resultTextBox.Text);
                appearanceInspectionValues.Add(judgementTextBox.Text);
                appearanceInspectionValues.Add(remarksTextBox.Text);
                appearanceInspectionValues.Add(defectQtyTextBox.Text);
                appearanceInspectionValues.Add(defectEncTextBox.Text);

            }
            else
            {
                appearanceInspectionlabel30.Text = appearanceInspectionFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceTextBox2.Text = comboBox1InvoiceNo;
                materialCodeBoxTextBox.Text = comboBox2PartNo;
            }           
        }

        string query;
        bool mouseDown;
        private Point offset;
        public UpdateAppearanceInspectionForm()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
    }
}
