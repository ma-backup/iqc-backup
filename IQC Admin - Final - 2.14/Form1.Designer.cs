﻿
namespace IQCAdmin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelMain1 = new System.Windows.Forms.Panel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.adminCredentialsGroup = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.xLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.enterButton = new System.Windows.Forms.Button();
            this.empNumber_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.password_textBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.btn_normal = new System.Windows.Forms.PictureBox();
            this.btn_minimize = new System.Windows.Forms.PictureBox();
            this.btn_maximize = new System.Windows.Forms.PictureBox();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.activityLogButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bgPanel = new System.Windows.Forms.Panel();
            this.powerBi = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.first_button = new System.Windows.Forms.Button();
            this.second_button = new System.Windows.Forms.Button();
            this.third_button = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.panelMain1.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.adminCredentialsGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_normal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMain1
            // 
            this.panelMain1.AutoSize = true;
            this.panelMain1.BackColor = System.Drawing.Color.Gray;
            this.panelMain1.Controls.Add(this.panelMain);
            this.panelMain1.Controls.Add(this.panelTitle);
            this.panelMain1.Location = new System.Drawing.Point(215, 0);
            this.panelMain1.Margin = new System.Windows.Forms.Padding(2);
            this.panelMain1.Name = "panelMain1";
            this.panelMain1.Size = new System.Drawing.Size(1288, 1000);
            this.panelMain1.TabIndex = 0;
            this.panelMain1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMain_Paint);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelMain.Controls.Add(this.adminCredentialsGroup);
            this.panelMain.Controls.Add(this.pictureBox1);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 59);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1288, 941);
            this.panelMain.TabIndex = 13;
            this.panelMain.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMain_Paint_1);
            // 
            // adminCredentialsGroup
            // 
            this.adminCredentialsGroup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.adminCredentialsGroup.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.adminCredentialsGroup.Controls.Add(this.pictureBox3);
            this.adminCredentialsGroup.Controls.Add(this.cancelButton);
            this.adminCredentialsGroup.Controls.Add(this.xLabel);
            this.adminCredentialsGroup.Controls.Add(this.label4);
            this.adminCredentialsGroup.Controls.Add(this.enterButton);
            this.adminCredentialsGroup.Controls.Add(this.empNumber_textBox);
            this.adminCredentialsGroup.Controls.Add(this.label3);
            this.adminCredentialsGroup.Controls.Add(this.label2);
            this.adminCredentialsGroup.Controls.Add(this.password_textBox);
            this.adminCredentialsGroup.Controls.Add(this.label5);
            this.adminCredentialsGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.adminCredentialsGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adminCredentialsGroup.ForeColor = System.Drawing.Color.Transparent;
            this.adminCredentialsGroup.Location = new System.Drawing.Point(460, 309);
            this.adminCredentialsGroup.Margin = new System.Windows.Forms.Padding(2);
            this.adminCredentialsGroup.Name = "adminCredentialsGroup";
            this.adminCredentialsGroup.Padding = new System.Windows.Forms.Padding(2);
            this.adminCredentialsGroup.Size = new System.Drawing.Size(382, 292);
            this.adminCredentialsGroup.TabIndex = 13;
            this.adminCredentialsGroup.TabStop = false;
            this.adminCredentialsGroup.Text = "ENTER ADMIN CREDENTIALS";
            this.adminCredentialsGroup.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.pictureBox3.Image = global::IQCAdmin.Properties.Resources._lock;
            this.pictureBox3.Location = new System.Drawing.Point(6, 7);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 36);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cancelButton.Location = new System.Drawing.Point(251, 236);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(99, 39);
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "CANCEL";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // xLabel
            // 
            this.xLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.xLabel.Location = new System.Drawing.Point(336, 0);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(46, 50);
            this.xLabel.TabIndex = 7;
            this.xLabel.Text = "X";
            this.xLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.xLabel.Click += new System.EventHandler(this.xLabel_Click);
            this.xLabel.MouseLeave += new System.EventHandler(this.xLabel_MouseLeave);
            this.xLabel.MouseHover += new System.EventHandler(this.xLabel_MouseHover);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(382, 50);
            this.label4.TabIndex = 6;
            this.label4.Text = "          ENTER ADMIN CREDENTIALS";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // enterButton
            // 
            this.enterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.enterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enterButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.enterButton.Location = new System.Drawing.Point(146, 236);
            this.enterButton.Name = "enterButton";
            this.enterButton.Size = new System.Drawing.Size(99, 39);
            this.enterButton.TabIndex = 5;
            this.enterButton.Text = "ENTER";
            this.enterButton.UseVisualStyleBackColor = false;
            this.enterButton.Click += new System.EventHandler(this.enterButton_Click);
            // 
            // empNumber_textBox
            // 
            this.empNumber_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.empNumber_textBox.Location = new System.Drawing.Point(34, 101);
            this.empNumber_textBox.Name = "empNumber_textBox";
            this.empNumber_textBox.Size = new System.Drawing.Size(316, 29);
            this.empNumber_textBox.TabIndex = 0;
            this.empNumber_textBox.TextChanged += new System.EventHandler(this.empNumber_textBox_TextChanged);
            this.empNumber_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.empNumber_textBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(30, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(30, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Employee No";
            // 
            // password_textBox
            // 
            this.password_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_textBox.Location = new System.Drawing.Point(34, 166);
            this.password_textBox.Margin = new System.Windows.Forms.Padding(2);
            this.password_textBox.Name = "password_textBox";
            this.password_textBox.Size = new System.Drawing.Size(316, 29);
            this.password_textBox.TabIndex = 4;
            this.password_textBox.UseSystemPasswordChar = true;
            this.password_textBox.TextChanged += new System.EventHandler(this.password_textBox_TextChanged);
            this.password_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.password_textBox_KeyPress);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(205)))), ((int)(((byte)(207)))));
            this.label5.Location = new System.Drawing.Point(0, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(382, 72);
            this.label5.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1288, 941);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panelTitle
            // 
            this.panelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panelTitle.Controls.Add(this.btn_normal);
            this.panelTitle.Controls.Add(this.btn_minimize);
            this.panelTitle.Controls.Add(this.btn_maximize);
            this.panelTitle.Controls.Add(this.btn_close);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Margin = new System.Windows.Forms.Padding(2);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(1288, 59);
            this.panelTitle.TabIndex = 0;
            this.panelTitle.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTitle_Paint);
            this.panelTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTitle_MouseMove);
            // 
            // btn_normal
            // 
            this.btn_normal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_normal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_normal.Image = ((System.Drawing.Image)(resources.GetObject("btn_normal.Image")));
            this.btn_normal.Location = new System.Drawing.Point(1218, 19);
            this.btn_normal.Margin = new System.Windows.Forms.Padding(2);
            this.btn_normal.Name = "btn_normal";
            this.btn_normal.Size = new System.Drawing.Size(22, 23);
            this.btn_normal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_normal.TabIndex = 3;
            this.btn_normal.TabStop = false;
            this.btn_normal.Visible = false;
            this.btn_normal.Click += new System.EventHandler(this.btn_normal_Click);
            // 
            // btn_minimize
            // 
            this.btn_minimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_minimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_minimize.Image = ((System.Drawing.Image)(resources.GetObject("btn_minimize.Image")));
            this.btn_minimize.Location = new System.Drawing.Point(1186, 19);
            this.btn_minimize.Margin = new System.Windows.Forms.Padding(2);
            this.btn_minimize.Name = "btn_minimize";
            this.btn_minimize.Size = new System.Drawing.Size(22, 23);
            this.btn_minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_minimize.TabIndex = 2;
            this.btn_minimize.TabStop = false;
            this.btn_minimize.Click += new System.EventHandler(this.btn_minimize_Click);
            // 
            // btn_maximize
            // 
            this.btn_maximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_maximize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_maximize.Image = ((System.Drawing.Image)(resources.GetObject("btn_maximize.Image")));
            this.btn_maximize.Location = new System.Drawing.Point(1218, 19);
            this.btn_maximize.Margin = new System.Windows.Forms.Padding(2);
            this.btn_maximize.Name = "btn_maximize";
            this.btn_maximize.Size = new System.Drawing.Size(22, 23);
            this.btn_maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_maximize.TabIndex = 1;
            this.btn_maximize.TabStop = false;
            this.btn_maximize.Click += new System.EventHandler(this.btn_maximize_Click);
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(1251, 19);
            this.btn_close.Margin = new System.Windows.Forms.Padding(2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(22, 23);
            this.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_close.TabIndex = 0;
            this.btn_close.TabStop = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.AutoScroll = true;
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(205)))), ((int)(((byte)(207)))));
            this.panelMenu.Controls.Add(this.activityLogButton);
            this.panelMenu.Controls.Add(this.panel1);
            this.panelMenu.Controls.Add(this.bgPanel);
            this.panelMenu.Controls.Add(this.powerBi);
            this.panelMenu.Controls.Add(this.editButton);
            this.panelMenu.Controls.Add(this.label1);
            this.panelMenu.Controls.Add(this.first_button);
            this.panelMenu.Controls.Add(this.second_button);
            this.panelMenu.Controls.Add(this.third_button);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(211, 894);
            this.panelMenu.TabIndex = 1;
            this.panelMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMenu_Paint);
            // 
            // activityLogButton
            // 
            this.activityLogButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.activityLogButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.activityLogButton.FlatAppearance.BorderSize = 0;
            this.activityLogButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.activityLogButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.activityLogButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.activityLogButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.activityLogButton.ForeColor = System.Drawing.Color.Black;
            this.activityLogButton.Image = ((System.Drawing.Image)(resources.GetObject("activityLogButton.Image")));
            this.activityLogButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.activityLogButton.Location = new System.Drawing.Point(2, 507);
            this.activityLogButton.Margin = new System.Windows.Forms.Padding(2);
            this.activityLogButton.Name = "activityLogButton";
            this.activityLogButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.activityLogButton.Size = new System.Drawing.Size(207, 60);
            this.activityLogButton.TabIndex = 9;
            this.activityLogButton.Text = "&Activity Log";
            this.activityLogButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.activityLogButton.UseVisualStyleBackColor = true;
            this.activityLogButton.Click += new System.EventHandler(this.activityLogButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(211, 148);
            this.panel1.TabIndex = 8;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(5, 8);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(10);
            this.pictureBox2.Size = new System.Drawing.Size(200, 132);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // bgPanel
            // 
            this.bgPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bgPanel.Location = new System.Drawing.Point(25, 778);
            this.bgPanel.Margin = new System.Windows.Forms.Padding(2);
            this.bgPanel.Name = "bgPanel";
            this.bgPanel.Size = new System.Drawing.Size(155, 26);
            this.bgPanel.TabIndex = 0;
            this.bgPanel.Visible = false;
            this.bgPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.bgPanel_Paint);
            // 
            // powerBi
            // 
            this.powerBi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.powerBi.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.powerBi.FlatAppearance.BorderSize = 0;
            this.powerBi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.powerBi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.powerBi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.powerBi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.powerBi.ForeColor = System.Drawing.Color.Black;
            this.powerBi.Image = ((System.Drawing.Image)(resources.GetObject("powerBi.Image")));
            this.powerBi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.powerBi.Location = new System.Drawing.Point(2, 387);
            this.powerBi.Margin = new System.Windows.Forms.Padding(2);
            this.powerBi.Name = "powerBi";
            this.powerBi.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.powerBi.Size = new System.Drawing.Size(207, 60);
            this.powerBi.TabIndex = 7;
            this.powerBi.Text = "&PowerBI Report";
            this.powerBi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.powerBi.UseVisualStyleBackColor = true;
            this.powerBi.Click += new System.EventHandler(this.powerBi_Click);
            // 
            // editButton
            // 
            this.editButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.editButton.FlatAppearance.BorderSize = 0;
            this.editButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.editButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.editButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.editButton.ForeColor = System.Drawing.Color.Black;
            this.editButton.Image = ((System.Drawing.Image)(resources.GetObject("editButton.Image")));
            this.editButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.editButton.Location = new System.Drawing.Point(2, 447);
            this.editButton.Margin = new System.Windows.Forms.Padding(2);
            this.editButton.Name = "editButton";
            this.editButton.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.editButton.Size = new System.Drawing.Size(207, 60);
            this.editButton.TabIndex = 6;
            this.editButton.Text = "&Edit Records";
            this.editButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 806);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10);
            this.label1.Size = new System.Drawing.Size(198, 88);
            this.label1.TabIndex = 5;
            this.label1.Text = "Developer Information:\r\nM.A. Technology Inc.\r\nTechincal - C.I.S Department\r\nLocal" +
    " #: 536";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // first_button
            // 
            this.first_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.first_button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.first_button.FlatAppearance.BorderSize = 0;
            this.first_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.first_button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.first_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.first_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.first_button.ForeColor = System.Drawing.Color.Black;
            this.first_button.Image = ((System.Drawing.Image)(resources.GetObject("first_button.Image")));
            this.first_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.first_button.Location = new System.Drawing.Point(2, 207);
            this.first_button.Margin = new System.Windows.Forms.Padding(2);
            this.first_button.Name = "first_button";
            this.first_button.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.first_button.Size = new System.Drawing.Size(207, 60);
            this.first_button.TabIndex = 0;
            this.first_button.Text = "&Summary Report";
            this.first_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.first_button.UseVisualStyleBackColor = true;
            this.first_button.Click += new System.EventHandler(this.first_button_Click);
            // 
            // second_button
            // 
            this.second_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.second_button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.second_button.FlatAppearance.BorderSize = 0;
            this.second_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.second_button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.second_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.second_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.second_button.ForeColor = System.Drawing.Color.Black;
            this.second_button.Image = ((System.Drawing.Image)(resources.GetObject("second_button.Image")));
            this.second_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.second_button.Location = new System.Drawing.Point(2, 267);
            this.second_button.Margin = new System.Windows.Forms.Padding(2);
            this.second_button.Name = "second_button";
            this.second_button.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.second_button.Size = new System.Drawing.Size(207, 60);
            this.second_button.TabIndex = 1;
            this.second_button.Text = "&Lot Number";
            this.second_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.second_button.UseVisualStyleBackColor = true;
            this.second_button.Click += new System.EventHandler(this.second_button_Click);
            // 
            // third_button
            // 
            this.third_button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.third_button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.third_button.FlatAppearance.BorderSize = 0;
            this.third_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(50)))), ((int)(((byte)(70)))));
            this.third_button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.third_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.third_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.third_button.ForeColor = System.Drawing.Color.Black;
            this.third_button.Image = ((System.Drawing.Image)(resources.GetObject("third_button.Image")));
            this.third_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.third_button.Location = new System.Drawing.Point(2, 327);
            this.third_button.Margin = new System.Windows.Forms.Padding(2);
            this.third_button.Name = "third_button";
            this.third_button.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.third_button.Size = new System.Drawing.Size(207, 60);
            this.third_button.TabIndex = 2;
            this.third_button.Text = "&Daily Report Form";
            this.third_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.third_button.UseVisualStyleBackColor = true;
            this.third_button.Click += new System.EventHandler(this.third_button_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.ClientSize = new System.Drawing.Size(1501, 894);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelMain1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IQC Admin";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelMain1.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.adminCredentialsGroup.ResumeLayout(false);
            this.adminCredentialsGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_normal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_maximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelMain1;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button third_button;
        private System.Windows.Forms.Button second_button;
        private System.Windows.Forms.Button first_button;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button powerBi;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel bgPanel;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.PictureBox btn_maximize;
        private System.Windows.Forms.PictureBox btn_minimize;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.GroupBox adminCredentialsGroup;
        private System.Windows.Forms.TextBox password_textBox;
        private System.Windows.Forms.PictureBox btn_normal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox empNumber_textBox;
        private System.Windows.Forms.Button enterButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button activityLogButton;
    }
}

