﻿
namespace IQCAdmin
{
    partial class FirstForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirstForm));
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource11 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource12 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource13 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource14 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource15 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.SP_SummaryReport_FunctionalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.IQCDatabaseDataSet = new IQCAdmin.IQCDatabaseDataSet1();
            this.SP_SummaryReport_DimensionalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SP_SummaryReport_AppearanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SP_SummaryReport_InspectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SP_SummaryReport_SampleSizeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SP_SummaryReport_FunctionalTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_FunctionalTableAdapter();
            this.SP_SummaryReport_DimensionalTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_DimensionalTableAdapter();
            this.SP_SummaryReport_AppearanceTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_AppearanceTableAdapter();
            this.SP_SummaryReport_InspectionTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_InspectionTableAdapter();
            this.SP_SummaryReport_SampleSizeTableAdapter = new IQCAdmin.IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_SampleSizeTableAdapter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_FunctionalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IQCDatabaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_DimensionalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_AppearanceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_InspectionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_SampleSizeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // SP_SummaryReport_FunctionalBindingSource
            // 
            this.SP_SummaryReport_FunctionalBindingSource.DataMember = "SP_SummaryReport_Functional";
            this.SP_SummaryReport_FunctionalBindingSource.DataSource = this.IQCDatabaseDataSet;
            // 
            // IQCDatabaseDataSet
            // 
            this.IQCDatabaseDataSet.DataSetName = "IQCDatabaseDataSet";
            this.IQCDatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SP_SummaryReport_DimensionalBindingSource
            // 
            this.SP_SummaryReport_DimensionalBindingSource.DataMember = "SP_SummaryReport_Dimensional";
            this.SP_SummaryReport_DimensionalBindingSource.DataSource = this.IQCDatabaseDataSet;
            // 
            // SP_SummaryReport_AppearanceBindingSource
            // 
            this.SP_SummaryReport_AppearanceBindingSource.DataMember = "SP_SummaryReport_Appearance";
            this.SP_SummaryReport_AppearanceBindingSource.DataSource = this.IQCDatabaseDataSet;
            // 
            // SP_SummaryReport_InspectionBindingSource
            // 
            this.SP_SummaryReport_InspectionBindingSource.DataMember = "SP_SummaryReport_Inspection";
            this.SP_SummaryReport_InspectionBindingSource.DataSource = this.IQCDatabaseDataSet;
            // 
            // SP_SummaryReport_SampleSizeBindingSource
            // 
            this.SP_SummaryReport_SampleSizeBindingSource.DataMember = "SP_SummaryReport_SampleSize";
            this.SP_SummaryReport_SampleSizeBindingSource.DataSource = this.IQCDatabaseDataSet;
            // 
            // comboBox2
            // 
            this.comboBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(751, 10);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(379, 32);
            this.comboBox2.TabIndex = 8;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            this.comboBox2.TextChanged += new System.EventHandler(this.comboBox2_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(668, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Part No.:";
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(265, 10);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(379, 32);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gainsboro;
            this.label2.Location = new System.Drawing.Point(125, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Invoice Number:";
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(1251, 3);
            this.btn_close.Margin = new System.Windows.Forms.Padding(2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 26);
            this.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_close.TabIndex = 7;
            this.btn_close.TabStop = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(337, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(665, 44);
            this.label1.TabIndex = 10;
            this.label1.Text = "[ GENERATE REPORT SUMMARY ]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Gainsboro;
            this.label9.Location = new System.Drawing.Point(1182, 54);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 26);
            this.label9.TabIndex = 14;
            this.label9.Text = "Hotkeys:\r\nJudgement - F1";
            // 
            // SP_SummaryReport_FunctionalTableAdapter
            // 
            this.SP_SummaryReport_FunctionalTableAdapter.ClearBeforeFill = true;
            // 
            // SP_SummaryReport_DimensionalTableAdapter
            // 
            this.SP_SummaryReport_DimensionalTableAdapter.ClearBeforeFill = true;
            // 
            // SP_SummaryReport_AppearanceTableAdapter
            // 
            this.SP_SummaryReport_AppearanceTableAdapter.ClearBeforeFill = true;
            // 
            // SP_SummaryReport_InspectionTableAdapter
            // 
            this.SP_SummaryReport_InspectionTableAdapter.ClearBeforeFill = true;
            // 
            // SP_SummaryReport_SampleSizeTableAdapter
            // 
            this.SP_SummaryReport_SampleSizeTableAdapter.ClearBeforeFill = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Location = new System.Drawing.Point(0, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 91);
            this.panel2.TabIndex = 7;
            // 
            // reportViewer1
            // 
            this.reportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource11.Name = "FunctionalDS";
            reportDataSource11.Value = this.SP_SummaryReport_FunctionalBindingSource;
            reportDataSource12.Name = "DimensionalDS";
            reportDataSource12.Value = this.SP_SummaryReport_DimensionalBindingSource;
            reportDataSource13.Name = "AppearanceDS";
            reportDataSource13.Value = this.SP_SummaryReport_AppearanceBindingSource;
            reportDataSource14.Name = "InspectionDS";
            reportDataSource14.Value = this.SP_SummaryReport_InspectionBindingSource;
            reportDataSource15.Name = "SampleSizeDS";
            reportDataSource15.Value = this.SP_SummaryReport_SampleSizeBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource11);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource12);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource13);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource14);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource15);
            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "IQCAdmin.SummaryReportFinal.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(2);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.PageCountMode = Microsoft.Reporting.WinForms.PageCountMode.Actual;
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(1278, 793);
            this.reportViewer1.TabIndex = 10;
            this.reportViewer1.ZoomPercent = 150;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 86);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1280, 850);
            this.panel1.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.reportViewer1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 55);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1278, 793);
            this.panel4.TabIndex = 10;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel3.Controls.Add(this.comboBox2);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1278, 55);
            this.panel3.TabIndex = 4;
            // 
            // FirstForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1280, 950);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FirstForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FirstForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FirstForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FirstForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_FunctionalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IQCDatabaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_DimensionalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_AppearanceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_InspectionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SP_SummaryReport_SampleSizeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource SP_SummaryReport_FunctionalBindingSource;
        private IQCDatabaseDataSet1 IQCDatabaseDataSet;
        private System.Windows.Forms.BindingSource SP_SummaryReport_DimensionalBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_AppearanceBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_InspectionBindingSource;
        private System.Windows.Forms.BindingSource SP_SummaryReport_SampleSizeBindingSource;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_FunctionalTableAdapter SP_SummaryReport_FunctionalTableAdapter;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_DimensionalTableAdapter SP_SummaryReport_DimensionalTableAdapter;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_AppearanceTableAdapter SP_SummaryReport_AppearanceTableAdapter;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_InspectionTableAdapter SP_SummaryReport_InspectionTableAdapter;
        private IQCDatabaseDataSet1TableAdapters.SP_SummaryReport_SampleSizeTableAdapter SP_SummaryReport_SampleSizeTableAdapter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
    }
}