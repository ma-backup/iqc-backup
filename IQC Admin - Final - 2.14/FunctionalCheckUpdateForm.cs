﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class FunctionalCheckUpdateForm : Form
    {
        public string id, invoiceNO, checkpoints, instrumentUsed, sampleUnit, sample1, sample2,
           sample3, sample4, sample5, sample6, sample7, sample8, sample9, sample10, minimum,
           average, maximum, lowerSpecLimit, upperSpecLimit, judgement, remarks, goodsCode, defectQty,
           defectEnc, materialCodeBox, date1, remarksCheckPoint;

        List<string> functionalCheckFields = new List<string>();
        List<string> functionalCheckValues = new List<string>();
        List<string> functionalCheckNewValues = new List<string>();

        private string action;

        public string functionalCheckFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialBoxTextBox25.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                {
                    query = "UPDATE FunctionalCheck " +
                        "SET checkpoints = '" + checkPointTextBox1.Text +
                        "', instrument_used = '" + instrumentUsedTextBox2.Text +
                        "', sample_unit = '" + sampleUnitTextBox3.Text +
                        "', sample1 = '" + sample1TextBox4.Text +
                        "', sample2 = '" + sample2TextBox5.Text +
                        "', sample3 = '" + sample3TextBox6.Text +
                        "', sample4 = '" + sample4TextBox7.Text +
                        "', sample5 = '" + sample5TextBox8.Text +
                        "', sample6 = '" + sample6TextBox9.Text +
                        "', sample7 = '" + sample7TextBox10.Text +
                        "', sample8 = '" + sample8TextBox11.Text +
                        "', sample9 = '" + sample9TextBox12.Text +
                        "', sample10 = '" + sample10TextBox13.Text +
                        "', minimum = '" + minimumTextBox14.Text +
                        "', average = '" + averageTextBox15.Text +
                        "', maximum = '" + maximumTextBox16.Text +
                        "', lower_spec_limit = '" + LowerSpecLimitTextBox17.Text +
                        "', upper_spec_limit = '" + upperSpecLimitTextBox18.Text +
                        "', judgement = '" + judgementTextBox20.Text +
                        "', remarks = '" + remarksTextBox21.Text +
                        "', goodsCode = '" + goodsCodeTextBox22.Text +
                        "', defectqty = '" + defectQtyTextBox23.Text +
                        "', defect_enc = '" + defectEncTextBox24.Text +
                        "', Date = '" + dateTimePicker1.Text +
                        "', [[Remarks_chckpoint] = '" + remarksCheckPointsTextBox1.Text +
                        "' WHERE id = '" + idTextBox.Text + "'";
                    SqlCommand command = new SqlCommand(query, DBHelper.connection);
                    command.ExecuteNonQuery();
                    frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                    obj.comboBox2_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    try
                    {
                        functionalCheckNewValues.Clear();
                        functionalCheckNewValues.Add(checkPointTextBox1.Text);
                        functionalCheckNewValues.Add(instrumentUsedTextBox2.Text);
                        functionalCheckNewValues.Add(sampleUnitTextBox3.Text);
                        functionalCheckNewValues.Add(sample1TextBox4.Text);
                        functionalCheckNewValues.Add(sample2TextBox5.Text);
                        functionalCheckNewValues.Add(sample3TextBox6.Text);
                        functionalCheckNewValues.Add(sample4TextBox7.Text);
                        functionalCheckNewValues.Add(sample5TextBox8.Text);
                        functionalCheckNewValues.Add(sample6TextBox9.Text);
                        functionalCheckNewValues.Add(sample7TextBox10.Text);
                        functionalCheckNewValues.Add(sample8TextBox11.Text);
                        functionalCheckNewValues.Add(sample9TextBox12.Text);
                        functionalCheckNewValues.Add(sample10TextBox13.Text);
                        functionalCheckNewValues.Add(minimumTextBox14.Text);
                        functionalCheckNewValues.Add(averageTextBox15.Text);
                        functionalCheckNewValues.Add(maximumTextBox16.Text);
                        functionalCheckNewValues.Add(LowerSpecLimitTextBox17.Text);
                        functionalCheckNewValues.Add(upperSpecLimitTextBox18.Text);
                        functionalCheckNewValues.Add(judgementTextBox20.Text);
                        functionalCheckNewValues.Add(remarksTextBox21.Text);
                        functionalCheckNewValues.Add(defectQtyTextBox23.Text);
                        functionalCheckNewValues.Add(defectEncTextBox24.Text);
                        functionalCheckNewValues.Add(dateTimePicker1.Text);
                        functionalCheckNewValues.Add(remarksCheckPointsTextBox1.Text);

                        var defaultAction = "EDITED FUNCTIONAL CHECK \x0AID :  " + idTextBox.Text + "\x0A" +
                           "INVOICE NO :  " + invoiceNotextBox.Text + "\x0APART NO :  " + materialBoxTextBox25.Text + "\x0A";
                        action = defaultAction;

                        for (int i = 0; i < functionalCheckFields.Count(); i++)
                        {
                            if (functionalCheckValues[i] != functionalCheckNewValues[i])
                            {
                                action += "\x0A" + functionalCheckFields[i] +
                                              "   from   ' " + functionalCheckValues[i] +
                                              " '   to   ' " + functionalCheckNewValues[i] + " '.";
                            }
                        }
                        DBHelper.saveActivityToDatabase(action, defaultAction);
                    }
                    catch (Exception)
                    {
                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FunctionalCheckUpdateForm_Load(object sender, EventArgs e)
        {
            //this.Location = new Point(425, 273);
            if(functionalCheckFormLabel == "update")
            {
                functionalCheckLabel30.Text = "FUNCTIONAL CHECK UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                idTextBox.Text = id;
                invoiceNotextBox.Text = invoiceNO;
                checkPointTextBox1.Text = checkpoints;
                instrumentUsedTextBox2.Text = instrumentUsed;
                sampleUnitTextBox3.Text = sampleUnit;
                sample1TextBox4.Text = sample1;
                sample2TextBox5.Text = sample2;
                sample3TextBox6.Text = sample3;
                sample4TextBox7.Text = sample4;
                sample5TextBox8.Text = sample5;
                sample6TextBox9.Text = sample6;
                sample7TextBox10.Text = sample7;
                sample8TextBox11.Text = sample8;
                sample9TextBox12.Text = sample9;
                sample10TextBox13.Text = sample10;
                minimumTextBox14.Text = minimum;
                averageTextBox15.Text = average;
                maximumTextBox16.Text = maximum;
                LowerSpecLimitTextBox17.Text = lowerSpecLimit;
                upperSpecLimitTextBox18.Text = upperSpecLimit;
                judgementTextBox20.Text = judgement;
                remarksTextBox21.Text = remarks;
                goodsCodeTextBox22.Text = goodsCode;
                defectQtyTextBox23.Text = defectQty;
                defectEncTextBox24.Text = defectEnc;
                materialBoxTextBox25.Text = materialCodeBox;
                dateTimePicker1.Text = date1;
                remarksCheckPointsTextBox1.Text = remarksCheckPoint;

                functionalCheckFields.Clear();
                functionalCheckFields.Add("Check Points");
                functionalCheckFields.Add("Instrument");
                functionalCheckFields.Add("Sample Unit");
                functionalCheckFields.Add("Sample 1");
                functionalCheckFields.Add("Sample 2");
                functionalCheckFields.Add("Sample 3");
                functionalCheckFields.Add("Sample 4");
                functionalCheckFields.Add("Sample 5");
                functionalCheckFields.Add("Sample 6");
                functionalCheckFields.Add("Sample 7");
                functionalCheckFields.Add("Sample 8");
                functionalCheckFields.Add("Sample 9");
                functionalCheckFields.Add("Sample 10");
                functionalCheckFields.Add("Minimum");
                functionalCheckFields.Add("Average");
                functionalCheckFields.Add("Maximum");
                functionalCheckFields.Add("Lower Spec Limit");
                functionalCheckFields.Add("Upper Spec Limit");
                functionalCheckFields.Add("Judgement");
                functionalCheckFields.Add("Remarks");
                functionalCheckFields.Add("Defect Quantity");
                functionalCheckFields.Add("Defect Encountered");
                functionalCheckFields.Add("Date");
                functionalCheckFields.Add("Remarks Check Points");

                functionalCheckValues.Clear();
                functionalCheckValues.Add(checkPointTextBox1.Text);
                functionalCheckValues.Add(instrumentUsedTextBox2.Text);
                functionalCheckValues.Add(sampleUnitTextBox3.Text);
                functionalCheckValues.Add(sample1TextBox4.Text);
                functionalCheckValues.Add(sample2TextBox5.Text);
                functionalCheckValues.Add(sample3TextBox6.Text);
                functionalCheckValues.Add(sample4TextBox7.Text);
                functionalCheckValues.Add(sample5TextBox8.Text);
                functionalCheckValues.Add(sample6TextBox9.Text);
                functionalCheckValues.Add(sample7TextBox10.Text);
                functionalCheckValues.Add(sample8TextBox11.Text);
                functionalCheckValues.Add(sample9TextBox12.Text);
                functionalCheckValues.Add(sample10TextBox13.Text);
                functionalCheckValues.Add(minimumTextBox14.Text);
                functionalCheckValues.Add(averageTextBox15.Text);
                functionalCheckValues.Add(maximumTextBox16.Text);
                functionalCheckValues.Add(LowerSpecLimitTextBox17.Text);
                functionalCheckValues.Add(upperSpecLimitTextBox18.Text);
                functionalCheckValues.Add(judgementTextBox20.Text);
                functionalCheckValues.Add(remarksTextBox21.Text);
                functionalCheckValues.Add(defectQtyTextBox23.Text);
                functionalCheckValues.Add(defectEncTextBox24.Text);
                functionalCheckValues.Add(dateTimePicker1.Text);
                functionalCheckValues.Add(remarksCheckPointsTextBox1.Text);
            }
            else
            {
                functionalCheckLabel30.Text = functionalCheckFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceNotextBox.Text = comboBox1InvoiceNo;
                materialBoxTextBox25.Text = comboBox2PartNo;
            }
        }

        string query;
        bool mouseDown;
        private Point offset;
        public FunctionalCheckUpdateForm()
        {
            InitializeComponent();
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void minimumTextBox14_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void averageTextBox15_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void maximumTextBox16_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void LowerSpecLimitTextBox17_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void upperSpecLimitTextBox18_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void defectQtyTextBox23_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void sample10TextBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample9TextBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample8TextBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample7TextBox10_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample6TextBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample5TextBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample4TextBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample3TextBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }

        }

        private void sample2TextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }

        private void sample1TextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8 && ch != '.')
            {
                e.Handled = true;
            }
        }
    }
}
