﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class lotInformationUpdateForm : Form
    {
        //LIST FOR ACTIVITY LOG
        List<string> lotInformationFields = new List<string>();
        List<string> lotInformationFieldsValues = new List<string>();
        List<string> lotInformationFieldsNewValues = new List<string>();

        private string action;

        public string lotInfoId, invoiceNo, partNo, partName, totalQty, 
            quantityReceived, lotNo, lotQty, boxNumber, reject, sampleSize, 
            goodsCode, materialCodeNo, remarks, date1, kenId, diff, totalGood;

        private void totalQTYTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void rejectTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void sampleSizeTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void kenIDTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void diffTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void quantityReceivedTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void boxNoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void lotQuantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void totalGoodTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsNumber(ch) && ch !=8) 
            {
                e.Handled = true;
            }
        }   

        public string lotInformationFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;

        string query;
        bool mouseDown;
        private Point offset;
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }    

        public lotInformationUpdateForm()
        {
            InitializeComponent();
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialBoxNoTextBox.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                {                   
                    try
                    { 
                       query = "UPDATE LotNumber " +
                       "SET part_no = '" + partNoTextBox.Text +
                       "', part_name = '" + partNameTextBox.Text +
                       "', total_quantity = '" + totalQTYTextBox.Text +
                       "', quantity_recieved = '" + quantityReceivedTextBox.Text +
                       "', lot_no = '" + lotNoTextBox.Text +
                       "', lot_quantity = '" + lotQuantityTextBox.Text +
                       "', box_number = '" + boxNoTextBox.Text +
                       "', reject = '" + rejectTextBox.Text +
                       "', sample_size = '" + sampleSizeTextBox.Text +
                       "', goodsCode = '" + goodsCodeTextBox.Text +
                       "', remarks = '" + remarksTextBox.Text +
                       "', Date = '" + lotNumberDateTimePicker.Text +
                       "', KEN_ID = '" + kenIDTextBox.Text +
                       "', DIFF = '" + diffTextBox.Text +
                       "', Total_good = '" + totalGoodTextBox.Text +
                       "' WHERE ID = '" + lotInfoIdTextBox.Text + "'";
                        SqlCommand command = new SqlCommand(query, DBHelper.connection);
                        command.ExecuteNonQuery();
                        frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                        obj.comboBox2_SelectedIndexChanged(sender, e);
                        MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //Save updates in activity log.
                        try
                        {
                            lotInformationFieldsNewValues.Clear();
                            //add data to list before an update
                            lotInformationFieldsNewValues.Add(partNoTextBox.Text);
                            lotInformationFieldsNewValues.Add(partNameTextBox.Text);
                            lotInformationFieldsNewValues.Add(totalQTYTextBox.Text);
                            lotInformationFieldsNewValues.Add(quantityReceivedTextBox.Text);
                            lotInformationFieldsNewValues.Add(lotNoTextBox.Text);
                            lotInformationFieldsNewValues.Add(lotQuantityTextBox.Text);
                            lotInformationFieldsNewValues.Add(boxNoTextBox.Text);
                            lotInformationFieldsNewValues.Add(rejectTextBox.Text);
                            lotInformationFieldsNewValues.Add(sampleSizeTextBox.Text);
                            lotInformationFieldsNewValues.Add(remarksTextBox.Text);
                            lotInformationFieldsNewValues.Add(lotNumberDateTimePicker.Text);
                            lotInformationFieldsNewValues.Add(kenIDTextBox.Text);
                            lotInformationFieldsNewValues.Add(diffTextBox.Text);
                            lotInformationFieldsNewValues.Add(totalGoodTextBox.Text);

                            var defaultAction = "EDITED LOT INFORMATION \x0AID :  " + lotInfoIdTextBox.Text + "\x0A" +
                            "INVOICE NO :  " + invoiceNoTextBox.Text + "\x0APART NO :  " + materialBoxNoTextBox.Text + "\x0A";
                            action = defaultAction;

                            //This compares the values/data before and after an update
                            for (int i = 0; i < lotInformationFields.Count(); i++)
                            {
                                if (lotInformationFieldsValues[i] != lotInformationFieldsNewValues[i])
                                {
                                    action += "\x0A" + lotInformationFields[i] +
                                              "   from   ' " + lotInformationFieldsValues[i] +
                                              " '   to   ' " + lotInformationFieldsNewValues[i] + " '.";
                                }
                            }

                            //This saves the actions to Activity_Log table in database
                            DBHelper.saveActivityToDatabase(action, defaultAction);
                        }
                        catch (Exception)
                        {                            
                        }

                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }     

        private void updateForm_Load(object sender, EventArgs e)
        {
            //this.Location = new Point(400, 255);

            if(lotInformationFormLabel == "update")
            {
                lotInfoFormLabel30.Text = "LOT INFORMATION UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                lotInfoIdTextBox.Text = lotInfoId;
                invoiceNoTextBox.Text = invoiceNo;
                partNoTextBox.Text = partNo;
                partNameTextBox.Text = partName;
                totalQTYTextBox.Text = totalQty;
                quantityReceivedTextBox.Text = quantityReceived;
                lotNoTextBox.Text = lotNo;
                lotQuantityTextBox.Text = lotQty;
                boxNoTextBox.Text = boxNumber;
                rejectTextBox.Text = reject;
                sampleSizeTextBox.Text = sampleSize;
                goodsCodeTextBox.Text = goodsCode;
                materialBoxNoTextBox.Text = materialCodeNo;
                remarksTextBox.Text = remarks;
                lotNumberDateTimePicker.Text = date1;
                kenIDTextBox.Text = kenId;
                diffTextBox.Text = diff;
                totalGoodTextBox.Text = totalGood;

                lotInformationFields.Clear();
                //add lot information fields to list for activity log
                lotInformationFields.Add("Part No");
                lotInformationFields.Add("Part Name");
                lotInformationFields.Add("Total Quantity");
                lotInformationFields.Add("Quantity Received");
                lotInformationFields.Add("Lot No");
                lotInformationFields.Add("Lot Quantity");
                lotInformationFields.Add("Box Number");
                lotInformationFields.Add("Reject");
                lotInformationFields.Add("Sample Size");
                lotInformationFields.Add("Remarks");
                lotInformationFields.Add("Date");
                lotInformationFields.Add("Ken ID");
                lotInformationFields.Add("Diff");
                lotInformationFields.Add("Total Good");

                lotInformationFieldsValues.Clear();
                //add data to list before an update
                lotInformationFieldsValues.Add(partNoTextBox.Text);
                lotInformationFieldsValues.Add(partNameTextBox.Text);
                lotInformationFieldsValues.Add(totalQTYTextBox.Text);
                lotInformationFieldsValues.Add(quantityReceivedTextBox.Text);
                lotInformationFieldsValues.Add(lotNoTextBox.Text);
                lotInformationFieldsValues.Add(lotQuantityTextBox.Text);
                lotInformationFieldsValues.Add(boxNoTextBox.Text);
                lotInformationFieldsValues.Add(rejectTextBox.Text);
                lotInformationFieldsValues.Add(sampleSizeTextBox.Text);
                lotInformationFieldsValues.Add(remarksTextBox.Text);
                lotInformationFieldsValues.Add(lotNumberDateTimePicker.Text);
                lotInformationFieldsValues.Add(kenIDTextBox.Text);
                lotInformationFieldsValues.Add(diffTextBox.Text);
                lotInformationFieldsValues.Add(totalGoodTextBox.Text);
            }
            else
            {
                lotInfoFormLabel30.Text = lotInformationFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceNoTextBox.Text = comboBox1InvoiceNo;
                materialBoxNoTextBox.Text = comboBox2PartNo;
            }
            
        }
        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
