﻿
namespace IQCAdmin
{
    partial class FunctionalCheckUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.remarksCheckPointsTextBox1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.materialBoxTextBox25 = new System.Windows.Forms.TextBox();
            this.defectEncTextBox24 = new System.Windows.Forms.TextBox();
            this.defectQtyTextBox23 = new System.Windows.Forms.TextBox();
            this.goodsCodeTextBox22 = new System.Windows.Forms.TextBox();
            this.functionalCheckLabel30 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.remarksTextBox21 = new System.Windows.Forms.TextBox();
            this.closeLabel = new System.Windows.Forms.Label();
            this.judgementTextBox20 = new System.Windows.Forms.TextBox();
            this.upperSpecLimitTextBox18 = new System.Windows.Forms.TextBox();
            this.LowerSpecLimitTextBox17 = new System.Windows.Forms.TextBox();
            this.maximumTextBox16 = new System.Windows.Forms.TextBox();
            this.averageTextBox15 = new System.Windows.Forms.TextBox();
            this.minimumTextBox14 = new System.Windows.Forms.TextBox();
            this.sample10TextBox13 = new System.Windows.Forms.TextBox();
            this.sample9TextBox12 = new System.Windows.Forms.TextBox();
            this.sample8TextBox11 = new System.Windows.Forms.TextBox();
            this.sample7TextBox10 = new System.Windows.Forms.TextBox();
            this.sample6TextBox9 = new System.Windows.Forms.TextBox();
            this.sample5TextBox8 = new System.Windows.Forms.TextBox();
            this.sample4TextBox7 = new System.Windows.Forms.TextBox();
            this.sample3TextBox6 = new System.Windows.Forms.TextBox();
            this.sample2TextBox5 = new System.Windows.Forms.TextBox();
            this.sample1TextBox4 = new System.Windows.Forms.TextBox();
            this.sampleUnitTextBox3 = new System.Windows.Forms.TextBox();
            this.instrumentUsedTextBox2 = new System.Windows.Forms.TextBox();
            this.checkPointTextBox1 = new System.Windows.Forms.TextBox();
            this.invoiceNotextBox = new System.Windows.Forms.TextBox();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.addButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd hh:mm:ss";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(850, 281);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker1.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(666, 313);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(211, 26);
            this.label21.TabIndex = 193;
            this.label21.Text = "REMARKS CHECK POINTS";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // remarksCheckPointsTextBox1
            // 
            this.remarksCheckPointsTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarksCheckPointsTextBox1.Location = new System.Drawing.Point(882, 313);
            this.remarksCheckPointsTextBox1.Name = "remarksCheckPointsTextBox1";
            this.remarksCheckPointsTextBox1.Size = new System.Drawing.Size(168, 26);
            this.remarksCheckPointsTextBox1.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(661, 283);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(184, 26);
            this.label28.TabIndex = 191;
            this.label28.Text = "DATE";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(666, 247);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(211, 26);
            this.label27.TabIndex = 190;
            this.label27.Text = "MATERIAL BOX";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(666, 213);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(211, 26);
            this.label26.TabIndex = 189;
            this.label26.Text = "DEFECT ENCOUNTERED";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(666, 179);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(211, 26);
            this.label25.TabIndex = 188;
            this.label25.Text = "DEFECT QUANTITY";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(666, 145);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(211, 26);
            this.label24.TabIndex = 187;
            this.label24.Text = "GOODS CODE";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(666, 111);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(211, 26);
            this.label23.TabIndex = 186;
            this.label23.Text = "REMARKS";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(666, 78);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(211, 26);
            this.label22.TabIndex = 185;
            this.label22.Text = "JUDGEMENT";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(312, 382);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(170, 26);
            this.label20.TabIndex = 184;
            this.label20.Text = "UPPER SPEC LIMIT";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(312, 347);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(170, 26);
            this.label19.TabIndex = 183;
            this.label19.Text = "LOWER SPEC LIMIT";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(312, 313);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(170, 26);
            this.label18.TabIndex = 182;
            this.label18.Text = "MAXIMUM";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(312, 279);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(170, 26);
            this.label17.TabIndex = 181;
            this.label17.Text = "AVERAGE";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(312, 245);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(170, 26);
            this.label16.TabIndex = 180;
            this.label16.Text = "MINIMUM";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(312, 211);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(170, 26);
            this.label15.TabIndex = 179;
            this.label15.Text = "SAMPLE 10";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(312, 177);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(170, 26);
            this.label14.TabIndex = 178;
            this.label14.Text = "SAMPLE 9";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(312, 143);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(170, 26);
            this.label13.TabIndex = 177;
            this.label13.Text = "SAMPLE 8";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(312, 110);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(170, 26);
            this.label12.TabIndex = 176;
            this.label12.Text = "SAMPLE 7";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(312, 80);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(170, 26);
            this.label11.TabIndex = 175;
            this.label11.Text = "SAMPLE 6";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 384);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 26);
            this.label10.TabIndex = 174;
            this.label10.Text = "SAMPLE 5";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 349);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 26);
            this.label9.TabIndex = 173;
            this.label9.Text = "SAMPLE 4";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 315);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 26);
            this.label8.TabIndex = 172;
            this.label8.Text = "SAMPLE 3";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 281);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 26);
            this.label7.TabIndex = 171;
            this.label7.Text = "SAMPLE 2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 247);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 26);
            this.label6.TabIndex = 170;
            this.label6.Text = "SAMPLE 1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 213);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 26);
            this.label5.TabIndex = 169;
            this.label5.Text = "SAMPLE UNIT";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 179);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 26);
            this.label4.TabIndex = 168;
            this.label4.Text = "INSTRUMENT";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 145);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 26);
            this.label3.TabIndex = 167;
            this.label3.Text = "CHECK POINTS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialBoxTextBox25
            // 
            this.materialBoxTextBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialBoxTextBox25.Location = new System.Drawing.Point(882, 247);
            this.materialBoxTextBox25.Name = "materialBoxTextBox25";
            this.materialBoxTextBox25.ReadOnly = true;
            this.materialBoxTextBox25.Size = new System.Drawing.Size(168, 26);
            this.materialBoxTextBox25.TabIndex = 166;
            this.materialBoxTextBox25.TabStop = false;
            // 
            // defectEncTextBox24
            // 
            this.defectEncTextBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defectEncTextBox24.Location = new System.Drawing.Point(882, 213);
            this.defectEncTextBox24.Name = "defectEncTextBox24";
            this.defectEncTextBox24.Size = new System.Drawing.Size(168, 26);
            this.defectEncTextBox24.TabIndex = 22;
            // 
            // defectQtyTextBox23
            // 
            this.defectQtyTextBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defectQtyTextBox23.Location = new System.Drawing.Point(882, 179);
            this.defectQtyTextBox23.Name = "defectQtyTextBox23";
            this.defectQtyTextBox23.Size = new System.Drawing.Size(168, 26);
            this.defectQtyTextBox23.TabIndex = 21;
            this.defectQtyTextBox23.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.defectQtyTextBox23_KeyPress);
            // 
            // goodsCodeTextBox22
            // 
            this.goodsCodeTextBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goodsCodeTextBox22.Location = new System.Drawing.Point(882, 145);
            this.goodsCodeTextBox22.Name = "goodsCodeTextBox22";
            this.goodsCodeTextBox22.ReadOnly = true;
            this.goodsCodeTextBox22.Size = new System.Drawing.Size(168, 26);
            this.goodsCodeTextBox22.TabIndex = 20;
            this.goodsCodeTextBox22.TabStop = false;
            // 
            // functionalCheckLabel30
            // 
            this.functionalCheckLabel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.functionalCheckLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.functionalCheckLabel30.ForeColor = System.Drawing.Color.White;
            this.functionalCheckLabel30.Location = new System.Drawing.Point(2, 0);
            this.functionalCheckLabel30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.functionalCheckLabel30.Name = "functionalCheckLabel30";
            this.functionalCheckLabel30.Padding = new System.Windows.Forms.Padding(11, 0, 0, 0);
            this.functionalCheckLabel30.Size = new System.Drawing.Size(378, 57);
            this.functionalCheckLabel30.TabIndex = 59;
            this.functionalCheckLabel30.Text = "FUNCTIONAL CHECK UPDATE";
            this.functionalCheckLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(172)))), ((int)(((byte)(60)))));
            this.saveButton.FlatAppearance.BorderSize = 0;
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSeaGreen;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(802, 12);
            this.saveButton.Margin = new System.Windows.Forms.Padding(2);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(120, 43);
            this.saveButton.TabIndex = 25;
            this.saveButton.Text = "&SAVE";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatAppearance.BorderSize = 0;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.White;
            this.cancelButton.Location = new System.Drawing.Point(930, 12);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(120, 43);
            this.cancelButton.TabIndex = 27;
            this.cancelButton.Text = "&CANCEL";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // remarksTextBox21
            // 
            this.remarksTextBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarksTextBox21.Location = new System.Drawing.Point(882, 111);
            this.remarksTextBox21.Name = "remarksTextBox21";
            this.remarksTextBox21.Size = new System.Drawing.Size(168, 26);
            this.remarksTextBox21.TabIndex = 19;
            // 
            // closeLabel
            // 
            this.closeLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.closeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeLabel.ForeColor = System.Drawing.Color.White;
            this.closeLabel.Location = new System.Drawing.Point(1019, 0);
            this.closeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.closeLabel.Name = "closeLabel";
            this.closeLabel.Size = new System.Drawing.Size(45, 57);
            this.closeLabel.TabIndex = 136;
            this.closeLabel.Text = "X";
            this.closeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.closeLabel.Click += new System.EventHandler(this.closeLabel_Click);
            // 
            // judgementTextBox20
            // 
            this.judgementTextBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.judgementTextBox20.Location = new System.Drawing.Point(882, 78);
            this.judgementTextBox20.Name = "judgementTextBox20";
            this.judgementTextBox20.Size = new System.Drawing.Size(168, 26);
            this.judgementTextBox20.TabIndex = 18;
            // 
            // upperSpecLimitTextBox18
            // 
            this.upperSpecLimitTextBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperSpecLimitTextBox18.Location = new System.Drawing.Point(487, 382);
            this.upperSpecLimitTextBox18.Name = "upperSpecLimitTextBox18";
            this.upperSpecLimitTextBox18.Size = new System.Drawing.Size(168, 26);
            this.upperSpecLimitTextBox18.TabIndex = 17;
            this.upperSpecLimitTextBox18.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.upperSpecLimitTextBox18_KeyPress);
            // 
            // LowerSpecLimitTextBox17
            // 
            this.LowerSpecLimitTextBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LowerSpecLimitTextBox17.Location = new System.Drawing.Point(487, 347);
            this.LowerSpecLimitTextBox17.Name = "LowerSpecLimitTextBox17";
            this.LowerSpecLimitTextBox17.Size = new System.Drawing.Size(168, 26);
            this.LowerSpecLimitTextBox17.TabIndex = 16;
            this.LowerSpecLimitTextBox17.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LowerSpecLimitTextBox17_KeyPress);
            // 
            // maximumTextBox16
            // 
            this.maximumTextBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maximumTextBox16.Location = new System.Drawing.Point(487, 313);
            this.maximumTextBox16.Name = "maximumTextBox16";
            this.maximumTextBox16.Size = new System.Drawing.Size(168, 26);
            this.maximumTextBox16.TabIndex = 15;
            this.maximumTextBox16.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maximumTextBox16_KeyPress);
            // 
            // averageTextBox15
            // 
            this.averageTextBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averageTextBox15.Location = new System.Drawing.Point(487, 279);
            this.averageTextBox15.Name = "averageTextBox15";
            this.averageTextBox15.Size = new System.Drawing.Size(168, 26);
            this.averageTextBox15.TabIndex = 14;
            this.averageTextBox15.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.averageTextBox15_KeyPress);
            // 
            // minimumTextBox14
            // 
            this.minimumTextBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minimumTextBox14.Location = new System.Drawing.Point(487, 245);
            this.minimumTextBox14.Name = "minimumTextBox14";
            this.minimumTextBox14.Size = new System.Drawing.Size(168, 26);
            this.minimumTextBox14.TabIndex = 13;
            this.minimumTextBox14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.minimumTextBox14_KeyPress);
            // 
            // sample10TextBox13
            // 
            this.sample10TextBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample10TextBox13.Location = new System.Drawing.Point(487, 211);
            this.sample10TextBox13.Name = "sample10TextBox13";
            this.sample10TextBox13.Size = new System.Drawing.Size(168, 26);
            this.sample10TextBox13.TabIndex = 12;
            this.sample10TextBox13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample10TextBox13_KeyPress);
            // 
            // sample9TextBox12
            // 
            this.sample9TextBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample9TextBox12.Location = new System.Drawing.Point(487, 177);
            this.sample9TextBox12.Name = "sample9TextBox12";
            this.sample9TextBox12.Size = new System.Drawing.Size(168, 26);
            this.sample9TextBox12.TabIndex = 11;
            this.sample9TextBox12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample9TextBox12_KeyPress);
            // 
            // sample8TextBox11
            // 
            this.sample8TextBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample8TextBox11.Location = new System.Drawing.Point(487, 143);
            this.sample8TextBox11.Name = "sample8TextBox11";
            this.sample8TextBox11.Size = new System.Drawing.Size(168, 26);
            this.sample8TextBox11.TabIndex = 10;
            this.sample8TextBox11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample8TextBox11_KeyPress);
            // 
            // sample7TextBox10
            // 
            this.sample7TextBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample7TextBox10.Location = new System.Drawing.Point(487, 110);
            this.sample7TextBox10.Name = "sample7TextBox10";
            this.sample7TextBox10.Size = new System.Drawing.Size(168, 26);
            this.sample7TextBox10.TabIndex = 9;
            this.sample7TextBox10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample7TextBox10_KeyPress);
            // 
            // sample6TextBox9
            // 
            this.sample6TextBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample6TextBox9.Location = new System.Drawing.Point(487, 78);
            this.sample6TextBox9.Name = "sample6TextBox9";
            this.sample6TextBox9.Size = new System.Drawing.Size(168, 26);
            this.sample6TextBox9.TabIndex = 8;
            this.sample6TextBox9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample6TextBox9_KeyPress);
            // 
            // sample5TextBox8
            // 
            this.sample5TextBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample5TextBox8.Location = new System.Drawing.Point(141, 384);
            this.sample5TextBox8.Name = "sample5TextBox8";
            this.sample5TextBox8.Size = new System.Drawing.Size(168, 26);
            this.sample5TextBox8.TabIndex = 7;
            this.sample5TextBox8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample5TextBox8_KeyPress);
            // 
            // sample4TextBox7
            // 
            this.sample4TextBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample4TextBox7.Location = new System.Drawing.Point(141, 349);
            this.sample4TextBox7.Name = "sample4TextBox7";
            this.sample4TextBox7.Size = new System.Drawing.Size(168, 26);
            this.sample4TextBox7.TabIndex = 6;
            this.sample4TextBox7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample4TextBox7_KeyPress);
            // 
            // sample3TextBox6
            // 
            this.sample3TextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample3TextBox6.Location = new System.Drawing.Point(141, 315);
            this.sample3TextBox6.Name = "sample3TextBox6";
            this.sample3TextBox6.Size = new System.Drawing.Size(168, 26);
            this.sample3TextBox6.TabIndex = 5;
            this.sample3TextBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample3TextBox6_KeyPress);
            // 
            // sample2TextBox5
            // 
            this.sample2TextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample2TextBox5.Location = new System.Drawing.Point(141, 281);
            this.sample2TextBox5.Name = "sample2TextBox5";
            this.sample2TextBox5.Size = new System.Drawing.Size(168, 26);
            this.sample2TextBox5.TabIndex = 4;
            this.sample2TextBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample2TextBox5_KeyPress);
            // 
            // sample1TextBox4
            // 
            this.sample1TextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sample1TextBox4.Location = new System.Drawing.Point(141, 247);
            this.sample1TextBox4.Name = "sample1TextBox4";
            this.sample1TextBox4.Size = new System.Drawing.Size(168, 26);
            this.sample1TextBox4.TabIndex = 3;
            this.sample1TextBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sample1TextBox4_KeyPress);
            // 
            // sampleUnitTextBox3
            // 
            this.sampleUnitTextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sampleUnitTextBox3.Location = new System.Drawing.Point(141, 213);
            this.sampleUnitTextBox3.Name = "sampleUnitTextBox3";
            this.sampleUnitTextBox3.Size = new System.Drawing.Size(168, 26);
            this.sampleUnitTextBox3.TabIndex = 2;
            // 
            // instrumentUsedTextBox2
            // 
            this.instrumentUsedTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instrumentUsedTextBox2.Location = new System.Drawing.Point(141, 179);
            this.instrumentUsedTextBox2.Name = "instrumentUsedTextBox2";
            this.instrumentUsedTextBox2.Size = new System.Drawing.Size(168, 26);
            this.instrumentUsedTextBox2.TabIndex = 1;
            // 
            // checkPointTextBox1
            // 
            this.checkPointTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkPointTextBox1.Location = new System.Drawing.Point(141, 145);
            this.checkPointTextBox1.Name = "checkPointTextBox1";
            this.checkPointTextBox1.Size = new System.Drawing.Size(168, 26);
            this.checkPointTextBox1.TabIndex = 0;
            // 
            // invoiceNotextBox
            // 
            this.invoiceNotextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceNotextBox.Location = new System.Drawing.Point(141, 112);
            this.invoiceNotextBox.Margin = new System.Windows.Forms.Padding(2);
            this.invoiceNotextBox.Name = "invoiceNotextBox";
            this.invoiceNotextBox.ReadOnly = true;
            this.invoiceNotextBox.Size = new System.Drawing.Size(168, 26);
            this.invoiceNotextBox.TabIndex = 142;
            this.invoiceNotextBox.TabStop = false;
            // 
            // idTextBox
            // 
            this.idTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idTextBox.Location = new System.Drawing.Point(141, 79);
            this.idTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.ReadOnly = true;
            this.idTextBox.Size = new System.Drawing.Size(168, 26);
            this.idTextBox.TabIndex = 141;
            this.idTextBox.TabStop = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 112);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 26);
            this.label2.TabIndex = 140;
            this.label2.Text = "INVOICE NO";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 24);
            this.label1.TabIndex = 139;
            this.label1.Text = "ID";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(205)))), ((int)(((byte)(207)))));
            this.panel1.Controls.Add(this.saveButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.addButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 435);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1064, 66);
            this.panel1.TabIndex = 138;
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.Blue;
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.ForeColor = System.Drawing.Color.White;
            this.addButton.Location = new System.Drawing.Point(802, 12);
            this.addButton.Margin = new System.Windows.Forms.Padding(2);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(120, 43);
            this.addButton.TabIndex = 26;
            this.addButton.Text = "&ADD";
            this.addButton.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.closeLabel);
            this.panel2.Controls.Add(this.functionalCheckLabel30);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1064, 57);
            this.panel2.TabIndex = 137;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // FunctionalCheckUpdateForm
            // 
            this.AcceptButton = this.saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(1064, 501);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.remarksCheckPointsTextBox1);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialBoxTextBox25);
            this.Controls.Add(this.defectEncTextBox24);
            this.Controls.Add(this.defectQtyTextBox23);
            this.Controls.Add(this.goodsCodeTextBox22);
            this.Controls.Add(this.remarksTextBox21);
            this.Controls.Add(this.judgementTextBox20);
            this.Controls.Add(this.upperSpecLimitTextBox18);
            this.Controls.Add(this.LowerSpecLimitTextBox17);
            this.Controls.Add(this.maximumTextBox16);
            this.Controls.Add(this.averageTextBox15);
            this.Controls.Add(this.minimumTextBox14);
            this.Controls.Add(this.sample10TextBox13);
            this.Controls.Add(this.sample9TextBox12);
            this.Controls.Add(this.sample8TextBox11);
            this.Controls.Add(this.sample7TextBox10);
            this.Controls.Add(this.sample6TextBox9);
            this.Controls.Add(this.sample5TextBox8);
            this.Controls.Add(this.sample4TextBox7);
            this.Controls.Add(this.sample3TextBox6);
            this.Controls.Add(this.sample2TextBox5);
            this.Controls.Add(this.sample1TextBox4);
            this.Controls.Add(this.sampleUnitTextBox3);
            this.Controls.Add(this.instrumentUsedTextBox2);
            this.Controls.Add(this.checkPointTextBox1);
            this.Controls.Add(this.invoiceNotextBox);
            this.Controls.Add(this.idTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FunctionalCheckUpdateForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FunctionalCheckUpdateForm";
            this.Load += new System.EventHandler(this.FunctionalCheckUpdateForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox remarksCheckPointsTextBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox materialBoxTextBox25;
        private System.Windows.Forms.TextBox defectEncTextBox24;
        private System.Windows.Forms.TextBox defectQtyTextBox23;
        private System.Windows.Forms.TextBox goodsCodeTextBox22;
        private System.Windows.Forms.Label functionalCheckLabel30;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox remarksTextBox21;
        private System.Windows.Forms.Label closeLabel;
        private System.Windows.Forms.TextBox judgementTextBox20;
        private System.Windows.Forms.TextBox upperSpecLimitTextBox18;
        private System.Windows.Forms.TextBox LowerSpecLimitTextBox17;
        private System.Windows.Forms.TextBox maximumTextBox16;
        private System.Windows.Forms.TextBox averageTextBox15;
        private System.Windows.Forms.TextBox minimumTextBox14;
        private System.Windows.Forms.TextBox sample10TextBox13;
        private System.Windows.Forms.TextBox sample9TextBox12;
        private System.Windows.Forms.TextBox sample8TextBox11;
        private System.Windows.Forms.TextBox sample7TextBox10;
        private System.Windows.Forms.TextBox sample6TextBox9;
        private System.Windows.Forms.TextBox sample5TextBox8;
        private System.Windows.Forms.TextBox sample4TextBox7;
        private System.Windows.Forms.TextBox sample3TextBox6;
        private System.Windows.Forms.TextBox sample2TextBox5;
        private System.Windows.Forms.TextBox sample1TextBox4;
        private System.Windows.Forms.TextBox sampleUnitTextBox3;
        private System.Windows.Forms.TextBox instrumentUsedTextBox2;
        private System.Windows.Forms.TextBox checkPointTextBox1;
        private System.Windows.Forms.TextBox invoiceNotextBox;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button addButton;
    }
}