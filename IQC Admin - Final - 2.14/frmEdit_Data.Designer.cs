﻿
namespace IQCAdmin
{
    partial class frmEdit_Data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEdit_Data));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.employeeNumberTextBox = new System.Windows.Forms.TextBox();
            this.employeeNameTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.lotInformationContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aCTIONSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addLotInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lotInformationDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lotNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lotQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boxNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sampleSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialCodeBoxSeqID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kenID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalGood = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.IncomingInspectioncontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addIncomingInspectionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editIncomingInspToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectionReportDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.approved = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isChecked = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prepared = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.approvedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preparedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.temperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assemblyLine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rohsCompliance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.humidity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inspectedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.receivedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maker = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inspector = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productionType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inspectionType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testReport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sampleSize1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ulMarking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.partName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoiceQuant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goodsCode1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialCodeBoxSeqId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.appearacneInspectionContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.addAppearanceInspectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editAppearanceInspectionToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.appearanceInspectionDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dimensionalCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.addDimensionalCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDimensionalCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.dimensionalCheckDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.functionalCheckContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.addFunctionalCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editFunctionalCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.functionalCheckDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            this.panel14.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.lotInformationContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lotInformationDataGridView)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.IncomingInspectioncontextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inspectionReportDataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.appearacneInspectionContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.appearanceInspectionDataGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.dimensionalCheckContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionalCheckDataGridView)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.functionalCheckContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.functionalCheckDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel2.Controls.Add(this.employeeNumberTextBox);
            this.panel2.Controls.Add(this.employeeNameTextBox);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Location = new System.Drawing.Point(0, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1279, 90);
            this.panel2.TabIndex = 12;
            // 
            // employeeNumberTextBox
            // 
            this.employeeNumberTextBox.Location = new System.Drawing.Point(43, 18);
            this.employeeNumberTextBox.Name = "employeeNumberTextBox";
            this.employeeNumberTextBox.Size = new System.Drawing.Size(168, 20);
            this.employeeNumberTextBox.TabIndex = 15;
            this.employeeNumberTextBox.Visible = false;
            // 
            // employeeNameTextBox
            // 
            this.employeeNameTextBox.Location = new System.Drawing.Point(43, 42);
            this.employeeNameTextBox.Name = "employeeNameTextBox";
            this.employeeNameTextBox.Size = new System.Drawing.Size(168, 20);
            this.employeeNameTextBox.TabIndex = 14;
            this.employeeNameTextBox.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Gainsboro;
            this.label9.Location = new System.Drawing.Point(960, 39);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(297, 39);
            this.label9.TabIndex = 13;
            this.label9.Text = "Hotkeys:\r\nDelete Entire Data - Ctrl+D (after generating data)\r\nEdit - Double Clic" +
    "k / Ctrl + E (after selecting a row)";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(35, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1211, 44);
            this.label1.TabIndex = 12;
            this.label1.Text = "[ EDIT DATA ]";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(1250, 3);
            this.btn_close.Margin = new System.Windows.Forms.Padding(2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(26, 26);
            this.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btn_close.TabIndex = 7;
            this.btn_close.TabStop = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            this.panel14.Controls.Add(this.label7);
            this.panel14.Controls.Add(this.label8);
            this.panel14.Controls.Add(this.comboBox2);
            this.panel14.Controls.Add(this.comboBox1);
            this.panel14.Location = new System.Drawing.Point(0, 88);
            this.panel14.Margin = new System.Windows.Forms.Padding(2);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1279, 52);
            this.panel14.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gainsboro;
            this.label7.Location = new System.Drawing.Point(709, 16);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Part No.:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gainsboro;
            this.label8.Location = new System.Drawing.Point(231, 16);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Invoice Number:";
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(786, 10);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(360, 32);
            this.comboBox2.TabIndex = 15;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            this.comboBox2.TextChanged += new System.EventHandler(this.comboBox2_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(360, 10);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(302, 32);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 145);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(10, 10);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1279, 649);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage7
            // 
            this.tabPage7.ContextMenuStrip = this.lotInformationContextMenuStrip;
            this.tabPage7.Controls.Add(this.lotInformationDataGridView);
            this.tabPage7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage7.Location = new System.Drawing.Point(4, 43);
            this.tabPage7.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage7.Size = new System.Drawing.Size(1271, 602);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "LOT INFORMATION";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // lotInformationContextMenuStrip
            // 
            this.lotInformationContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.lotInformationContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aCTIONSToolStripMenuItem,
            this.toolStripSeparator1,
            this.addLotInfoToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.lotInformationContextMenuStrip.Name = "lotInformationContextMenuStrip";
            this.lotInformationContextMenuStrip.Size = new System.Drawing.Size(218, 98);
            // 
            // aCTIONSToolStripMenuItem
            // 
            this.aCTIONSToolStripMenuItem.Name = "aCTIONSToolStripMenuItem";
            this.aCTIONSToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.aCTIONSToolStripMenuItem.Text = "ACTIONS";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(214, 6);
            // 
            // addLotInfoToolStripMenuItem
            // 
            this.addLotInfoToolStripMenuItem.Enabled = false;
            this.addLotInfoToolStripMenuItem.Name = "addLotInfoToolStripMenuItem";
            this.addLotInfoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.addLotInfoToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.addLotInfoToolStripMenuItem.Text = "&Add lot information";
            this.addLotInfoToolStripMenuItem.Visible = false;
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Enabled = false;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.editToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // lotInformationDataGridView
            // 
            this.lotInformationDataGridView.AllowUserToAddRows = false;
            this.lotInformationDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.lotInformationDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.lotInformationDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.lotInformationDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.lotInformationDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.lotInformationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lotInformationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.partNo,
            this.partName,
            this.totalQTY,
            this.quantityReceived,
            this.lotNo,
            this.lotQuantity,
            this.boxNumber,
            this.reject,
            this.sampleSize,
            this.goodsCode,
            this.materialCodeBoxSeqID,
            this.remarks,
            this.date1,
            this.kenID,
            this.diff,
            this.totalGood});
            this.lotInformationDataGridView.ContextMenuStrip = this.lotInformationContextMenuStrip;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.lotInformationDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.lotInformationDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lotInformationDataGridView.EnableHeadersVisualStyles = false;
            this.lotInformationDataGridView.GridColor = System.Drawing.Color.Gray;
            this.lotInformationDataGridView.Location = new System.Drawing.Point(2, 2);
            this.lotInformationDataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.lotInformationDataGridView.MultiSelect = false;
            this.lotInformationDataGridView.Name = "lotInformationDataGridView";
            this.lotInformationDataGridView.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.lotInformationDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.lotInformationDataGridView.RowHeadersWidth = 51;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            this.lotInformationDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.lotInformationDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotInformationDataGridView.RowTemplate.Height = 50;
            this.lotInformationDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.lotInformationDataGridView.Size = new System.Drawing.Size(1267, 598);
            this.lotInformationDataGridView.TabIndex = 1;
            this.lotInformationDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lotInformationDataGridView_CellClick);
            this.lotInformationDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lotInformationDataGridView_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "INVOICE NO";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // partNo
            // 
            this.partNo.HeaderText = "PART NO";
            this.partNo.MinimumWidth = 6;
            this.partNo.Name = "partNo";
            this.partNo.ReadOnly = true;
            this.partNo.Width = 200;
            // 
            // partName
            // 
            this.partName.HeaderText = "PART NAME";
            this.partName.MinimumWidth = 6;
            this.partName.Name = "partName";
            this.partName.ReadOnly = true;
            this.partName.Width = 200;
            // 
            // totalQTY
            // 
            this.totalQTY.HeaderText = "TOTAL QUANTITY";
            this.totalQTY.MinimumWidth = 6;
            this.totalQTY.Name = "totalQTY";
            this.totalQTY.ReadOnly = true;
            this.totalQTY.Width = 125;
            // 
            // quantityReceived
            // 
            this.quantityReceived.HeaderText = "QUANTITY RECEIVED";
            this.quantityReceived.MinimumWidth = 6;
            this.quantityReceived.Name = "quantityReceived";
            this.quantityReceived.ReadOnly = true;
            this.quantityReceived.Width = 125;
            // 
            // lotNo
            // 
            this.lotNo.HeaderText = "LOT NO";
            this.lotNo.MinimumWidth = 6;
            this.lotNo.Name = "lotNo";
            this.lotNo.ReadOnly = true;
            this.lotNo.Width = 175;
            // 
            // lotQuantity
            // 
            this.lotQuantity.HeaderText = "LOT QUANTITY";
            this.lotQuantity.MinimumWidth = 6;
            this.lotQuantity.Name = "lotQuantity";
            this.lotQuantity.ReadOnly = true;
            this.lotQuantity.Width = 125;
            // 
            // boxNumber
            // 
            this.boxNumber.HeaderText = "BOX NUMBER";
            this.boxNumber.MinimumWidth = 6;
            this.boxNumber.Name = "boxNumber";
            this.boxNumber.ReadOnly = true;
            this.boxNumber.Width = 175;
            // 
            // reject
            // 
            this.reject.HeaderText = "REJECT";
            this.reject.MinimumWidth = 6;
            this.reject.Name = "reject";
            this.reject.ReadOnly = true;
            this.reject.Width = 125;
            // 
            // sampleSize
            // 
            this.sampleSize.HeaderText = "SAMPLE SIZE";
            this.sampleSize.MinimumWidth = 6;
            this.sampleSize.Name = "sampleSize";
            this.sampleSize.ReadOnly = true;
            this.sampleSize.Width = 125;
            // 
            // goodsCode
            // 
            this.goodsCode.HeaderText = "GOODS CODE";
            this.goodsCode.MinimumWidth = 6;
            this.goodsCode.Name = "goodsCode";
            this.goodsCode.ReadOnly = true;
            this.goodsCode.Width = 125;
            // 
            // materialCodeBoxSeqID
            // 
            this.materialCodeBoxSeqID.HeaderText = "MATERIAL CODE BOX SEQ ID";
            this.materialCodeBoxSeqID.MinimumWidth = 6;
            this.materialCodeBoxSeqID.Name = "materialCodeBoxSeqID";
            this.materialCodeBoxSeqID.ReadOnly = true;
            this.materialCodeBoxSeqID.Width = 250;
            // 
            // remarks
            // 
            this.remarks.HeaderText = "REMARKS";
            this.remarks.MinimumWidth = 6;
            this.remarks.Name = "remarks";
            this.remarks.ReadOnly = true;
            this.remarks.Width = 125;
            // 
            // date1
            // 
            this.date1.HeaderText = "DATE";
            this.date1.MinimumWidth = 6;
            this.date1.Name = "date1";
            this.date1.ReadOnly = true;
            this.date1.Width = 200;
            // 
            // kenID
            // 
            this.kenID.HeaderText = "KEN ID";
            this.kenID.MinimumWidth = 6;
            this.kenID.Name = "kenID";
            this.kenID.ReadOnly = true;
            this.kenID.Width = 125;
            // 
            // diff
            // 
            this.diff.HeaderText = "DIFF";
            this.diff.MinimumWidth = 6;
            this.diff.Name = "diff";
            this.diff.ReadOnly = true;
            this.diff.Width = 125;
            // 
            // totalGood
            // 
            this.totalGood.HeaderText = "TOTAL GOOD";
            this.totalGood.MinimumWidth = 6;
            this.totalGood.Name = "totalGood";
            this.totalGood.ReadOnly = true;
            this.totalGood.Width = 125;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.ContextMenuStrip = this.IncomingInspectioncontextMenuStrip;
            this.tabPage1.Controls.Add(this.inspectionReportDataGridView);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 43);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1271, 602);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "INCOMING INSPECTION REPORT";
            // 
            // IncomingInspectioncontextMenuStrip
            // 
            this.IncomingInspectioncontextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.IncomingInspectioncontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator2,
            this.addIncomingInspectionReportToolStripMenuItem,
            this.editIncomingInspToolStripMenuItem2,
            this.deleteToolStripMenuItem1});
            this.IncomingInspectioncontextMenuStrip.Name = "lotInformationContextMenuStrip";
            this.IncomingInspectioncontextMenuStrip.Size = new System.Drawing.Size(285, 98);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(284, 22);
            this.toolStripMenuItem1.Text = "ACTIONS";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(281, 6);
            // 
            // addIncomingInspectionReportToolStripMenuItem
            // 
            this.addIncomingInspectionReportToolStripMenuItem.Enabled = false;
            this.addIncomingInspectionReportToolStripMenuItem.Name = "addIncomingInspectionReportToolStripMenuItem";
            this.addIncomingInspectionReportToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.addIncomingInspectionReportToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.addIncomingInspectionReportToolStripMenuItem.Text = "&Add Incoming Inspection Report";
            this.addIncomingInspectionReportToolStripMenuItem.Visible = false;
            // 
            // editIncomingInspToolStripMenuItem2
            // 
            this.editIncomingInspToolStripMenuItem2.Enabled = false;
            this.editIncomingInspToolStripMenuItem2.Name = "editIncomingInspToolStripMenuItem2";
            this.editIncomingInspToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.editIncomingInspToolStripMenuItem2.Size = new System.Drawing.Size(284, 22);
            this.editIncomingInspToolStripMenuItem2.Text = "&Edit";
            this.editIncomingInspToolStripMenuItem2.Click += new System.EventHandler(this.editIncomingInspToolStripMenuItem2_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Enabled = false;
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(284, 22);
            this.deleteToolStripMenuItem1.Text = "&Delete";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // inspectionReportDataGridView
            // 
            this.inspectionReportDataGridView.AllowUserToAddRows = false;
            this.inspectionReportDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.inspectionReportDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.inspectionReportDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.inspectionReportDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.inspectionReportDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.inspectionReportDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.inspectionReportDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.approved,
            this.isChecked,
            this.prepared,
            this.approvedDate,
            this.checkedDate,
            this.preparedDate,
            this.temperature,
            this.assemblyLine,
            this.partNumber,
            this.rohsCompliance,
            this.humidity,
            this.inspectedDate,
            this.receivedDate,
            this.supplier,
            this.maker,
            this.inspector,
            this.materialType,
            this.productionType,
            this.inspectionType,
            this.oir,
            this.testReport,
            this.sampleSize1,
            this.ulMarking,
            this.coc,
            this.partName1,
            this.invoiceQuant,
            this.goodsCode1,
            this.materialCodeBoxSeqId1,
            this.date2});
            this.inspectionReportDataGridView.ContextMenuStrip = this.IncomingInspectioncontextMenuStrip;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.inspectionReportDataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.inspectionReportDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inspectionReportDataGridView.EnableHeadersVisualStyles = false;
            this.inspectionReportDataGridView.GridColor = System.Drawing.Color.Gray;
            this.inspectionReportDataGridView.Location = new System.Drawing.Point(3, 3);
            this.inspectionReportDataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.inspectionReportDataGridView.MultiSelect = false;
            this.inspectionReportDataGridView.Name = "inspectionReportDataGridView";
            this.inspectionReportDataGridView.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.inspectionReportDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.inspectionReportDataGridView.RowHeadersWidth = 51;
            this.inspectionReportDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inspectionReportDataGridView.RowTemplate.Height = 50;
            this.inspectionReportDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.inspectionReportDataGridView.Size = new System.Drawing.Size(1265, 596);
            this.inspectionReportDataGridView.TabIndex = 2;
            this.inspectionReportDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.inspectionReportDataGridView_CellClick);
            this.inspectionReportDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.inspectionReportDataGridView_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ID";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "INVOICE NO";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 175;
            // 
            // approved
            // 
            this.approved.HeaderText = "APPROVED";
            this.approved.MinimumWidth = 6;
            this.approved.Name = "approved";
            this.approved.ReadOnly = true;
            this.approved.Width = 150;
            // 
            // isChecked
            // 
            this.isChecked.HeaderText = "CHECKED";
            this.isChecked.MinimumWidth = 6;
            this.isChecked.Name = "isChecked";
            this.isChecked.ReadOnly = true;
            this.isChecked.Width = 150;
            // 
            // prepared
            // 
            this.prepared.HeaderText = "PREPARED";
            this.prepared.MinimumWidth = 6;
            this.prepared.Name = "prepared";
            this.prepared.ReadOnly = true;
            this.prepared.Width = 150;
            // 
            // approvedDate
            // 
            this.approvedDate.HeaderText = "APPROVED DATE";
            this.approvedDate.MinimumWidth = 6;
            this.approvedDate.Name = "approvedDate";
            this.approvedDate.ReadOnly = true;
            this.approvedDate.Width = 200;
            // 
            // checkedDate
            // 
            this.checkedDate.HeaderText = "CHECKED DATE";
            this.checkedDate.MinimumWidth = 6;
            this.checkedDate.Name = "checkedDate";
            this.checkedDate.ReadOnly = true;
            this.checkedDate.Width = 200;
            // 
            // preparedDate
            // 
            this.preparedDate.HeaderText = "PREPARED DATE";
            this.preparedDate.MinimumWidth = 6;
            this.preparedDate.Name = "preparedDate";
            this.preparedDate.ReadOnly = true;
            this.preparedDate.Width = 200;
            // 
            // temperature
            // 
            this.temperature.HeaderText = "TEMPERATURE";
            this.temperature.MinimumWidth = 6;
            this.temperature.Name = "temperature";
            this.temperature.ReadOnly = true;
            this.temperature.Width = 150;
            // 
            // assemblyLine
            // 
            this.assemblyLine.HeaderText = "ASSEMBLY LINE";
            this.assemblyLine.MinimumWidth = 6;
            this.assemblyLine.Name = "assemblyLine";
            this.assemblyLine.ReadOnly = true;
            this.assemblyLine.Width = 150;
            // 
            // partNumber
            // 
            this.partNumber.HeaderText = "PART NUMBER";
            this.partNumber.MinimumWidth = 6;
            this.partNumber.Name = "partNumber";
            this.partNumber.ReadOnly = true;
            this.partNumber.Width = 250;
            // 
            // rohsCompliance
            // 
            this.rohsCompliance.HeaderText = "ROHS COMPLIANCE";
            this.rohsCompliance.MinimumWidth = 6;
            this.rohsCompliance.Name = "rohsCompliance";
            this.rohsCompliance.ReadOnly = true;
            this.rohsCompliance.Width = 150;
            // 
            // humidity
            // 
            this.humidity.HeaderText = "HUMIDITY";
            this.humidity.MinimumWidth = 6;
            this.humidity.Name = "humidity";
            this.humidity.ReadOnly = true;
            this.humidity.Width = 150;
            // 
            // inspectedDate
            // 
            this.inspectedDate.HeaderText = "INSPECTED DATE";
            this.inspectedDate.MinimumWidth = 6;
            this.inspectedDate.Name = "inspectedDate";
            this.inspectedDate.ReadOnly = true;
            this.inspectedDate.Width = 200;
            // 
            // receivedDate
            // 
            this.receivedDate.HeaderText = "RECEIVED DATE";
            this.receivedDate.MinimumWidth = 6;
            this.receivedDate.Name = "receivedDate";
            this.receivedDate.ReadOnly = true;
            this.receivedDate.Width = 200;
            // 
            // supplier
            // 
            this.supplier.HeaderText = "SUPPLIER";
            this.supplier.MinimumWidth = 6;
            this.supplier.Name = "supplier";
            this.supplier.ReadOnly = true;
            this.supplier.Width = 150;
            // 
            // maker
            // 
            this.maker.HeaderText = "MAKER";
            this.maker.MinimumWidth = 6;
            this.maker.Name = "maker";
            this.maker.ReadOnly = true;
            this.maker.Width = 200;
            // 
            // inspector
            // 
            this.inspector.HeaderText = "INSPECTOR";
            this.inspector.MinimumWidth = 6;
            this.inspector.Name = "inspector";
            this.inspector.ReadOnly = true;
            this.inspector.Width = 200;
            // 
            // materialType
            // 
            this.materialType.HeaderText = "MATERIAL TYPE";
            this.materialType.MinimumWidth = 6;
            this.materialType.Name = "materialType";
            this.materialType.ReadOnly = true;
            this.materialType.Width = 175;
            // 
            // productionType
            // 
            this.productionType.HeaderText = "PRODUCTION TYPE";
            this.productionType.MinimumWidth = 6;
            this.productionType.Name = "productionType";
            this.productionType.ReadOnly = true;
            this.productionType.Width = 175;
            // 
            // inspectionType
            // 
            this.inspectionType.HeaderText = "INSPECTION TYPE";
            this.inspectionType.MinimumWidth = 6;
            this.inspectionType.Name = "inspectionType";
            this.inspectionType.ReadOnly = true;
            this.inspectionType.Width = 175;
            // 
            // oir
            // 
            this.oir.HeaderText = "OIR";
            this.oir.MinimumWidth = 6;
            this.oir.Name = "oir";
            this.oir.ReadOnly = true;
            this.oir.Width = 125;
            // 
            // testReport
            // 
            this.testReport.HeaderText = "TEST REPORT";
            this.testReport.MinimumWidth = 6;
            this.testReport.Name = "testReport";
            this.testReport.ReadOnly = true;
            this.testReport.Width = 125;
            // 
            // sampleSize1
            // 
            this.sampleSize1.HeaderText = "SAMPLE SIZE";
            this.sampleSize1.MinimumWidth = 6;
            this.sampleSize1.Name = "sampleSize1";
            this.sampleSize1.ReadOnly = true;
            this.sampleSize1.Width = 125;
            // 
            // ulMarking
            // 
            this.ulMarking.HeaderText = "UL MARKING";
            this.ulMarking.MinimumWidth = 6;
            this.ulMarking.Name = "ulMarking";
            this.ulMarking.ReadOnly = true;
            this.ulMarking.Width = 125;
            // 
            // coc
            // 
            this.coc.HeaderText = "COC";
            this.coc.MinimumWidth = 6;
            this.coc.Name = "coc";
            this.coc.ReadOnly = true;
            this.coc.Width = 125;
            // 
            // partName1
            // 
            this.partName1.HeaderText = "PART NAME";
            this.partName1.MinimumWidth = 6;
            this.partName1.Name = "partName1";
            this.partName1.ReadOnly = true;
            this.partName1.Width = 200;
            // 
            // invoiceQuant
            // 
            this.invoiceQuant.HeaderText = "INVOICE QUANITY";
            this.invoiceQuant.MinimumWidth = 6;
            this.invoiceQuant.Name = "invoiceQuant";
            this.invoiceQuant.ReadOnly = true;
            this.invoiceQuant.Width = 150;
            // 
            // goodsCode1
            // 
            this.goodsCode1.HeaderText = "GOODS CODE";
            this.goodsCode1.MinimumWidth = 6;
            this.goodsCode1.Name = "goodsCode1";
            this.goodsCode1.ReadOnly = true;
            this.goodsCode1.Width = 150;
            // 
            // materialCodeBoxSeqId1
            // 
            this.materialCodeBoxSeqId1.HeaderText = "MATERIAL CODE BOX SEQ ID";
            this.materialCodeBoxSeqId1.MinimumWidth = 6;
            this.materialCodeBoxSeqId1.Name = "materialCodeBoxSeqId1";
            this.materialCodeBoxSeqId1.ReadOnly = true;
            this.materialCodeBoxSeqId1.Width = 250;
            // 
            // date2
            // 
            this.date2.HeaderText = "DATE";
            this.date2.MinimumWidth = 6;
            this.date2.Name = "date2";
            this.date2.ReadOnly = true;
            this.date2.Width = 200;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.ContextMenuStrip = this.appearacneInspectionContextMenuStrip;
            this.tabPage2.Controls.Add(this.appearanceInspectionDataGridView);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 43);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1271, 602);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "APPEARANCE INSPECTION";
            // 
            // appearacneInspectionContextMenuStrip
            // 
            this.appearacneInspectionContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.appearacneInspectionContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripSeparator3,
            this.addAppearanceInspectionToolStripMenuItem,
            this.editAppearanceInspectionToolStripMenuItem3,
            this.deleteToolStripMenuItem2});
            this.appearacneInspectionContextMenuStrip.Name = "lotInformationContextMenuStrip";
            this.appearacneInspectionContextMenuStrip.Size = new System.Drawing.Size(259, 98);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(258, 22);
            this.toolStripMenuItem2.Text = "ACTIONS";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(255, 6);
            // 
            // addAppearanceInspectionToolStripMenuItem
            // 
            this.addAppearanceInspectionToolStripMenuItem.Enabled = false;
            this.addAppearanceInspectionToolStripMenuItem.Name = "addAppearanceInspectionToolStripMenuItem";
            this.addAppearanceInspectionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.addAppearanceInspectionToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.addAppearanceInspectionToolStripMenuItem.Text = "&Add Appearance Inspection";
            this.addAppearanceInspectionToolStripMenuItem.Visible = false;
            // 
            // editAppearanceInspectionToolStripMenuItem3
            // 
            this.editAppearanceInspectionToolStripMenuItem3.Enabled = false;
            this.editAppearanceInspectionToolStripMenuItem3.Name = "editAppearanceInspectionToolStripMenuItem3";
            this.editAppearanceInspectionToolStripMenuItem3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.editAppearanceInspectionToolStripMenuItem3.Size = new System.Drawing.Size(258, 22);
            this.editAppearanceInspectionToolStripMenuItem3.Text = "&Edit";
            this.editAppearanceInspectionToolStripMenuItem3.Click += new System.EventHandler(this.editAppearanceInspectionToolStripMenuItem3_Click);
            // 
            // deleteToolStripMenuItem2
            // 
            this.deleteToolStripMenuItem2.Enabled = false;
            this.deleteToolStripMenuItem2.Name = "deleteToolStripMenuItem2";
            this.deleteToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.deleteToolStripMenuItem2.Size = new System.Drawing.Size(258, 22);
            this.deleteToolStripMenuItem2.Text = "&Delete";
            this.deleteToolStripMenuItem2.Click += new System.EventHandler(this.deleteToolStripMenuItem2_Click);
            // 
            // appearanceInspectionDataGridView
            // 
            this.appearanceInspectionDataGridView.AllowUserToAddRows = false;
            this.appearanceInspectionDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.appearanceInspectionDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.appearanceInspectionDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.appearanceInspectionDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.appearanceInspectionDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.appearanceInspectionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.appearanceInspectionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11});
            this.appearanceInspectionDataGridView.ContextMenuStrip = this.appearacneInspectionContextMenuStrip;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.appearanceInspectionDataGridView.DefaultCellStyle = dataGridViewCellStyle12;
            this.appearanceInspectionDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.appearanceInspectionDataGridView.EnableHeadersVisualStyles = false;
            this.appearanceInspectionDataGridView.GridColor = System.Drawing.Color.Gray;
            this.appearanceInspectionDataGridView.Location = new System.Drawing.Point(3, 3);
            this.appearanceInspectionDataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.appearanceInspectionDataGridView.MultiSelect = false;
            this.appearanceInspectionDataGridView.Name = "appearanceInspectionDataGridView";
            this.appearanceInspectionDataGridView.ReadOnly = true;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.appearanceInspectionDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.appearanceInspectionDataGridView.RowHeadersWidth = 51;
            this.appearanceInspectionDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appearanceInspectionDataGridView.RowTemplate.Height = 50;
            this.appearanceInspectionDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.appearanceInspectionDataGridView.Size = new System.Drawing.Size(1265, 596);
            this.appearanceInspectionDataGridView.TabIndex = 0;
            this.appearanceInspectionDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.appearanceInspectionDataGridView_CellClick);
            this.appearanceInspectionDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.appearanceInspectionDataGridView_CellDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 125;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "INVOICE NO";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 175;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "CHECK POINTS";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 150;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "INSTRUMENT";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 200;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "RESULT";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 125;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "JUDGEMENT";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 150;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "REMARKS";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 125;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "GOODS CODE";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 125;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "DEFECT QTY";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 125;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "DEFECT ECOUNTERED";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 150;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "MATERIAL CODE BOX SEQ ID";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Width = 250;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.ContextMenuStrip = this.dimensionalCheckContextMenuStrip;
            this.tabPage3.Controls.Add(this.dimensionalCheckDataGridView);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 43);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1271, 602);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "DIMENSIONAL CHECK";
            // 
            // dimensionalCheckContextMenuStrip
            // 
            this.dimensionalCheckContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.dimensionalCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripSeparator4,
            this.addDimensionalCheckToolStripMenuItem,
            this.editDimensionalCheckToolStripMenuItem,
            this.deleteToolStripMenuItem3});
            this.dimensionalCheckContextMenuStrip.Name = "lotInformationContextMenuStrip";
            this.dimensionalCheckContextMenuStrip.Size = new System.Drawing.Size(237, 98);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(236, 22);
            this.toolStripMenuItem3.Text = "ACTIONS";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(233, 6);
            // 
            // addDimensionalCheckToolStripMenuItem
            // 
            this.addDimensionalCheckToolStripMenuItem.Enabled = false;
            this.addDimensionalCheckToolStripMenuItem.Name = "addDimensionalCheckToolStripMenuItem";
            this.addDimensionalCheckToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.addDimensionalCheckToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.addDimensionalCheckToolStripMenuItem.Text = "&Add dimensional check";
            this.addDimensionalCheckToolStripMenuItem.Visible = false;
            // 
            // editDimensionalCheckToolStripMenuItem
            // 
            this.editDimensionalCheckToolStripMenuItem.Enabled = false;
            this.editDimensionalCheckToolStripMenuItem.Name = "editDimensionalCheckToolStripMenuItem";
            this.editDimensionalCheckToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.editDimensionalCheckToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.editDimensionalCheckToolStripMenuItem.Text = "&Edit";
            this.editDimensionalCheckToolStripMenuItem.Click += new System.EventHandler(this.editDimensionalCheckToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem3
            // 
            this.deleteToolStripMenuItem3.Enabled = false;
            this.deleteToolStripMenuItem3.Name = "deleteToolStripMenuItem3";
            this.deleteToolStripMenuItem3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.deleteToolStripMenuItem3.Size = new System.Drawing.Size(236, 22);
            this.deleteToolStripMenuItem3.Text = "&Delete";
            this.deleteToolStripMenuItem3.Click += new System.EventHandler(this.deleteToolStripMenuItem3_Click);
            // 
            // dimensionalCheckDataGridView
            // 
            this.dimensionalCheckDataGridView.AllowUserToAddRows = false;
            this.dimensionalCheckDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dimensionalCheckDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dimensionalCheckDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.dimensionalCheckDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dimensionalCheckDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dimensionalCheckDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dimensionalCheckDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60});
            this.dimensionalCheckDataGridView.ContextMenuStrip = this.dimensionalCheckContextMenuStrip;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dimensionalCheckDataGridView.DefaultCellStyle = dataGridViewCellStyle16;
            this.dimensionalCheckDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dimensionalCheckDataGridView.EnableHeadersVisualStyles = false;
            this.dimensionalCheckDataGridView.GridColor = System.Drawing.Color.Gray;
            this.dimensionalCheckDataGridView.Location = new System.Drawing.Point(0, 0);
            this.dimensionalCheckDataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.dimensionalCheckDataGridView.MultiSelect = false;
            this.dimensionalCheckDataGridView.Name = "dimensionalCheckDataGridView";
            this.dimensionalCheckDataGridView.ReadOnly = true;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dimensionalCheckDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dimensionalCheckDataGridView.RowHeadersWidth = 51;
            this.dimensionalCheckDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dimensionalCheckDataGridView.RowTemplate.Height = 50;
            this.dimensionalCheckDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dimensionalCheckDataGridView.Size = new System.Drawing.Size(1271, 602);
            this.dimensionalCheckDataGridView.TabIndex = 3;
            this.dimensionalCheckDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dimensionalCheckDataGridView_CellClick);
            this.dimensionalCheckDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dimensionalCheckDataGridView_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "ID";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 125;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.HeaderText = "INVOICE NO";
            this.dataGridViewTextBoxColumn34.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 150;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.HeaderText = "CHECK POINTS";
            this.dataGridViewTextBoxColumn35.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.ReadOnly = true;
            this.dataGridViewTextBoxColumn35.Width = 150;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.HeaderText = "INSTRUMENT";
            this.dataGridViewTextBoxColumn36.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 200;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.HeaderText = "SAMPLE UNIT";
            this.dataGridViewTextBoxColumn37.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 150;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.HeaderText = "SAMPLE 1";
            this.dataGridViewTextBoxColumn38.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 150;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.HeaderText = "SAMPLE 2";
            this.dataGridViewTextBoxColumn39.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 150;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.HeaderText = "SAMPLE 3";
            this.dataGridViewTextBoxColumn40.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 150;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.HeaderText = "SAMPLE 4";
            this.dataGridViewTextBoxColumn41.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.ReadOnly = true;
            this.dataGridViewTextBoxColumn41.Width = 150;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.HeaderText = "SAMPLE 5";
            this.dataGridViewTextBoxColumn42.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 150;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.HeaderText = "SAMPLE 6";
            this.dataGridViewTextBoxColumn43.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 150;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.HeaderText = "SAMPLE 7";
            this.dataGridViewTextBoxColumn44.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 150;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.HeaderText = "SAMPLE 8";
            this.dataGridViewTextBoxColumn45.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 150;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.HeaderText = "SAMPLE 9";
            this.dataGridViewTextBoxColumn46.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 150;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.HeaderText = "SAMPLE 10";
            this.dataGridViewTextBoxColumn47.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.ReadOnly = true;
            this.dataGridViewTextBoxColumn47.Width = 150;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.HeaderText = "MINIMUM";
            this.dataGridViewTextBoxColumn48.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Width = 150;
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.HeaderText = "AVERAGE";
            this.dataGridViewTextBoxColumn49.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Width = 150;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.HeaderText = "MAXIMUM";
            this.dataGridViewTextBoxColumn50.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            this.dataGridViewTextBoxColumn50.Width = 150;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "LOWER SPEC LIMIT";
            this.dataGridViewTextBoxColumn51.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Width = 150;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.HeaderText = "UPPER SPEC LIMIT";
            this.dataGridViewTextBoxColumn52.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Width = 150;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.HeaderText = "JUDGEMENT";
            this.dataGridViewTextBoxColumn53.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.ReadOnly = true;
            this.dataGridViewTextBoxColumn53.Width = 150;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.HeaderText = "REMARKS";
            this.dataGridViewTextBoxColumn54.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Width = 125;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.HeaderText = "GOODS CODE";
            this.dataGridViewTextBoxColumn55.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            this.dataGridViewTextBoxColumn55.Width = 150;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.HeaderText = "DEFECT QUANTITY";
            this.dataGridViewTextBoxColumn56.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            this.dataGridViewTextBoxColumn56.Width = 150;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.HeaderText = "DEFECT ENCOUNTER";
            this.dataGridViewTextBoxColumn57.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            this.dataGridViewTextBoxColumn57.Width = 150;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.HeaderText = "MATERIAL CODE BOX SEQ ID";
            this.dataGridViewTextBoxColumn58.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            this.dataGridViewTextBoxColumn58.Width = 250;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.HeaderText = "DATE";
            this.dataGridViewTextBoxColumn59.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.ReadOnly = true;
            this.dataGridViewTextBoxColumn59.Width = 200;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.HeaderText = "REMARKS CHECKPOINT";
            this.dataGridViewTextBoxColumn60.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Width = 150;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage4.ContextMenuStrip = this.functionalCheckContextMenuStrip;
            this.tabPage4.Controls.Add(this.functionalCheckDataGridView);
            this.tabPage4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage4.Location = new System.Drawing.Point(4, 43);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1271, 602);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "FUNCTIONAL CHECK";
            // 
            // functionalCheckContextMenuStrip
            // 
            this.functionalCheckContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.functionalCheckContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripSeparator5,
            this.addFunctionalCheckToolStripMenuItem,
            this.editFunctionalCheckToolStripMenuItem,
            this.deleteToolStripMenuItem4});
            this.functionalCheckContextMenuStrip.Name = "lotInformationContextMenuStrip";
            this.functionalCheckContextMenuStrip.Size = new System.Drawing.Size(230, 98);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(229, 22);
            this.toolStripMenuItem4.Text = "ACTIONS";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(226, 6);
            // 
            // addFunctionalCheckToolStripMenuItem
            // 
            this.addFunctionalCheckToolStripMenuItem.Enabled = false;
            this.addFunctionalCheckToolStripMenuItem.Name = "addFunctionalCheckToolStripMenuItem";
            this.addFunctionalCheckToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.addFunctionalCheckToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.addFunctionalCheckToolStripMenuItem.Text = "&Add Functional Check";
            this.addFunctionalCheckToolStripMenuItem.Visible = false;
            // 
            // editFunctionalCheckToolStripMenuItem
            // 
            this.editFunctionalCheckToolStripMenuItem.Enabled = false;
            this.editFunctionalCheckToolStripMenuItem.Name = "editFunctionalCheckToolStripMenuItem";
            this.editFunctionalCheckToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.editFunctionalCheckToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.editFunctionalCheckToolStripMenuItem.Text = "&Edit";
            this.editFunctionalCheckToolStripMenuItem.Click += new System.EventHandler(this.editFunctionalCheckToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem4
            // 
            this.deleteToolStripMenuItem4.Enabled = false;
            this.deleteToolStripMenuItem4.Name = "deleteToolStripMenuItem4";
            this.deleteToolStripMenuItem4.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.deleteToolStripMenuItem4.Size = new System.Drawing.Size(229, 22);
            this.deleteToolStripMenuItem4.Text = "&Delete";
            this.deleteToolStripMenuItem4.Click += new System.EventHandler(this.deleteToolStripMenuItem4_Click);
            // 
            // functionalCheckDataGridView
            // 
            this.functionalCheckDataGridView.AllowUserToAddRows = false;
            this.functionalCheckDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.functionalCheckDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.functionalCheckDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.functionalCheckDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(83)))), ((int)(((byte)(147)))));
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.functionalCheckDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.functionalCheckDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.functionalCheckDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33});
            this.functionalCheckDataGridView.ContextMenuStrip = this.functionalCheckContextMenuStrip;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.functionalCheckDataGridView.DefaultCellStyle = dataGridViewCellStyle20;
            this.functionalCheckDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionalCheckDataGridView.EnableHeadersVisualStyles = false;
            this.functionalCheckDataGridView.GridColor = System.Drawing.Color.Gray;
            this.functionalCheckDataGridView.Location = new System.Drawing.Point(0, 0);
            this.functionalCheckDataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.functionalCheckDataGridView.MultiSelect = false;
            this.functionalCheckDataGridView.Name = "functionalCheckDataGridView";
            this.functionalCheckDataGridView.ReadOnly = true;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.functionalCheckDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.functionalCheckDataGridView.RowHeadersWidth = 51;
            this.functionalCheckDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.functionalCheckDataGridView.RowTemplate.Height = 50;
            this.functionalCheckDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.functionalCheckDataGridView.Size = new System.Drawing.Size(1271, 602);
            this.functionalCheckDataGridView.TabIndex = 2;
            this.functionalCheckDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.functionalCheckDataGridView_CellClick);
            this.functionalCheckDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.functionalCheckDataGridView_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ID";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "INVOICE NO";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 150;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "CHECK POINTS";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "INSTRUMENT";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 200;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "SAMPLE UNIT";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "SAMPLE 1";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 150;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "SAMPLE 2";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 150;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "SAMPLE 3";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 150;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "SAMPLE 4";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 150;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "SAMPLE 5";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 150;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "SAMPLE 6";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "SAMPLE 7";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 150;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "SAMPLE 8";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 150;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "SAMPLE 9";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 150;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "SAMPLE 10";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 150;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "MINIMUM";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 150;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "AVERAGE";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 125;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "MAXIMUM";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 150;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "LOWER SPEC LIMIT";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 150;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "UPPER SPEC LIMIT";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 150;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "JUDGEMENT";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Width = 150;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "REMARKS";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 150;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "GOODS CODE";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 175;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "DEFECT QUANTITY";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Width = 150;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "DEFECT ENCOUNTER";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 150;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "MATERIAL CODE BOX SEQ ID";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 250;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.HeaderText = "DATE";
            this.dataGridViewTextBoxColumn32.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 200;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.HeaderText = "REMARKS CHECKPOINT";
            this.dataGridViewTextBoxColumn33.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 200;
            // 
            // frmEdit_Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1278, 793);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmEdit_Data";
            this.Text = "frmEdit_Data";
            this.Activated += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEdit_Data_FormClosing);
            this.Load += new System.EventHandler(this.frmEdit_Data_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEdit_Data_KeyDown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.lotInformationContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lotInformationDataGridView)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.IncomingInspectioncontextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.inspectionReportDataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.appearacneInspectionContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.appearanceInspectionDataGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.dimensionalCheckContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dimensionalCheckDataGridView)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.functionalCheckContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.functionalCheckDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_close;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView appearanceInspectionDataGridView;
        private System.Windows.Forms.DataGridView lotInformationDataGridView;
        private System.Windows.Forms.DataGridView functionalCheckDataGridView;
        private System.Windows.Forms.DataGridView inspectionReportDataGridView;
        private System.Windows.Forms.DataGridView dimensionalCheckDataGridView;
        private System.Windows.Forms.ContextMenuStrip lotInformationContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCTIONSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn approved;
        private System.Windows.Forms.DataGridViewTextBoxColumn isChecked;
        private System.Windows.Forms.DataGridViewTextBoxColumn prepared;
        private System.Windows.Forms.DataGridViewTextBoxColumn approvedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn checkedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn preparedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn temperature;
        private System.Windows.Forms.DataGridViewTextBoxColumn assemblyLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn partNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn rohsCompliance;
        private System.Windows.Forms.DataGridViewTextBoxColumn humidity;
        private System.Windows.Forms.DataGridViewTextBoxColumn inspectedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn receivedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn maker;
        private System.Windows.Forms.DataGridViewTextBoxColumn inspector;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialType;
        private System.Windows.Forms.DataGridViewTextBoxColumn productionType;
        private System.Windows.Forms.DataGridViewTextBoxColumn inspectionType;
        private System.Windows.Forms.DataGridViewTextBoxColumn oir;
        private System.Windows.Forms.DataGridViewTextBoxColumn testReport;
        private System.Windows.Forms.DataGridViewTextBoxColumn sampleSize1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ulMarking;
        private System.Windows.Forms.DataGridViewTextBoxColumn coc;
        private System.Windows.Forms.DataGridViewTextBoxColumn partName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoiceQuant;
        private System.Windows.Forms.DataGridViewTextBoxColumn goodsCode1;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialCodeBoxSeqId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn date2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.ContextMenuStrip IncomingInspectioncontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem editIncomingInspToolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip appearacneInspectionContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem editAppearanceInspectionToolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip dimensionalCheckContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem editDimensionalCheckToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip functionalCheckContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem editFunctionalCheckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addLotInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addIncomingInspectionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAppearanceInspectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDimensionalCheckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFunctionalCheckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn partNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn partName;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityReceived;
        private System.Windows.Forms.DataGridViewTextBoxColumn lotNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn lotQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn boxNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn reject;
        private System.Windows.Forms.DataGridViewTextBoxColumn sampleSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn goodsCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialCodeBoxSeqID;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarks;
        private System.Windows.Forms.DataGridViewTextBoxColumn date1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kenID;
        private System.Windows.Forms.DataGridViewTextBoxColumn diff;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalGood;
        public System.Windows.Forms.TextBox employeeNumberTextBox;
        public System.Windows.Forms.TextBox employeeNameTextBox;
    }
}